describe('HomePage', () => {
  it('Check VocaDB Top Songs', () => {
    cy.visit('/');

    cy.get('button', { timeout: 5000 }).then(() => {
      expect(cy.$$('.ant-list-item')).length(10);

      cy.get('button').click();

      cy.get('button').then(() => {
        expect(cy.$$('.ant-list-item')).length(20);
      });
    });
  });
});
