module.exports = {
  testMatch: [
    '**/__tests__/**/*.ts?(x)',
    '**/?(*.)+(spec|test).ts?(x)',
    '!**/cypress/**',
  ],
  moduleNameMapper: {
    '^@/(.*)': '<rootDir>/$1',
    '\\.(css|less|scss)$': 'identity-obj-proxy',
  },
  collectCoverage: true,
  collectCoverageFrom: [
    '**/{pages,components,utils}/**/*.ts?(x)',
    '!**/node_modules/**',
  ],
  coverageDirectory: '.nyc_output',
  coverageReporters: ['text', ['json', { file: 'out.json' }]],
};
