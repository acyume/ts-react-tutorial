import React from 'react';
import { Spin, Typography, Divider, List, Button, Space } from 'antd';
import { DownOutlined, FireOutlined, HeartOutlined } from '@ant-design/icons';
import { useRequest } from 'ahooks';
import dayjs from '@/utils/dayjs';
import {
  useSongApiApi,
  SongForApiContract,
  PartialFindResultSongListForApiContract,
} from '@/api';
import styles from './SongList.module.scss';

type IconTextProps = {
  icon: React.ReactElement;
  text: string;
};

const IconText: React.FC<IconTextProps> = ({ icon, text }) => (
  <Space>
    {icon}
    {text}
  </Space>
);

const SongList: React.FC = () => {
  const { songApiGetList } = useSongApiApi();
  const { data, loadMore, loadingMore } = useRequest<
    { list: SongForApiContract[] },
    PartialFindResultSongListForApiContract
  >(
    (d) =>
      songApiGetList({
        sort: 'RatingScore',
        start: d?.list.length ?? 0,
        maxResults: 10,
      }),
    {
      loadMore: true,
      formatResult: (res) => {
        return {
          list: res.items ?? [],
        };
      },
    },
  );
  if (!data || !data.list.length) {
    return (
      <div className={styles.loading}>
        <Spin size="large" />
      </div>
    );
  }
  return (
    <div className={styles.container}>
      <Typography.Title>VocaDB Top Songs</Typography.Title>
      <Divider />
      <List
        itemLayout="vertical"
        dataSource={data.list}
        renderItem={(song) => {
          return (
            <List.Item
              actions={[
                <IconText
                  key="list-vertical-fire"
                  icon={<FireOutlined />}
                  text={`${song.ratingScore}`}
                />,
                <IconText
                  key="list-vertical-heart"
                  icon={<HeartOutlined />}
                  text={`${song.favoritedTimes}`}
                />,
              ]}
              extra={
                <div style={{ color: 'rgba(0, 0, 0, .35)', marginTop: 20 }}>
                  <i>Created {dayjs(song.createDate).fromNow()}</i>
                </div>
              }
            >
              <List.Item.Meta
                title={song.name}
                description={song.artistString}
              />
            </List.Item>
          );
        }}
      />
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
          marginTop: 20,
        }}
      >
        {!loadingMore ? (
          <Button onClick={loadMore}>
            More Songs <DownOutlined />
          </Button>
        ) : (
          <Spin size="large" />
        )}
      </div>
    </div>
  );
};

export default SongList;
