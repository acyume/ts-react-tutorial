import React from 'react';
import SongList from '@/components/SongList';

const HomePage: React.FC = () => {
  return <SongList />;
};

export default HomePage;
