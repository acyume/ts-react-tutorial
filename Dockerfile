FROM node:lts-alpine

WORKDIR /app

COPY package.json .yarnclean yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build && yarn --production

ENV NODE_ENV production
CMD ["node_modules/.bin/next", "start"]
