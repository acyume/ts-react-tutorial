import App from 'next/app';
import 'antd/lib/style/index.less';
import '@/styles/app.global.scss';

export default App;
