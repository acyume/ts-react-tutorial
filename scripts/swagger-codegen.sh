#!/bin/bash

# SwaggerUI: https://vocadb.net/swagger/ui/index#

SWAGGER_URL=https://vocadb.net/swagger/docs/v1

cd $(dirname $0)/..

mkdir -p api && cd api

docker run --rm -v $(pwd):/api \
  swaggerapi/swagger-codegen-cli:2.4.14 generate \
  -i ${SWAGGER_URL} \
  -l typescript-fetch \
  -t /api/swagger-codegen/template \
  -o /api

SED=sed

if [[ `uname -a` =~ "Darwin" ]]; then
  SED=gsed
fi

$SED -i 's/__\([a-z]\)\(\S*Params\)/\u\1\2/g' api.ts

rm -rf .gitignore .swagger-codegen .swagger-codegen-ignore git_push.sh custom.d.ts *.bak

prettier . --write
