module.exports = {
  presets: ['next/babel'],
  plugins: [
    [
      'import',
      {
        libraryName: 'antd',
        style: true,
      },
    ],
    ...(process.env.WITH_ISTANBUL ? ['istanbul'] : []),
  ],
};
