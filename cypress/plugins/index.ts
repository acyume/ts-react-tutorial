/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */

module.exports = (on: any, config: any) => {
  require('@cypress/code-coverage/task')(on, config);

  return config;
};
