/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-namespace */
import url from 'url';
import isomorphicFetch from 'isomorphic-fetch';
import React from 'react';
import { Configuration } from './configuration';

const BASE_PATH = 'https://vocadb.net'.replace(/\/+$/, '');

/**
 *
 * @export
 */
export const COLLECTION_FORMATS = {
  csv: ',',
  ssv: ' ',
  tsv: '\t',
  pipes: '|',
};

/**
 *
 * @export
 * @interface FetchAPI
 */
export interface FetchAPI {
  (url: string, init?: any): Promise<Response>;
}

/**
 *
 * @export
 * @interface FetchArgs
 */
export interface FetchArgs {
  url: string;
  options: any;
}

/**
 *
 * @export
 * @class BaseAPI
 */
export class BaseAPI {
  protected configuration: Configuration;

  constructor(
    configuration?: Configuration,
    protected basePath: string = BASE_PATH,
    protected fetch: FetchAPI = isomorphicFetch,
  ) {
    if (configuration) {
      this.configuration = configuration;
      this.basePath = configuration.basePath || this.basePath;
    }
  }
}

/**
 *
 * @export
 * @class RequiredError
 * @extends {Error}
 */
export class RequiredError extends Error {
  name: 'RequiredError';
  constructor(public field: string, msg?: string) {
    super(msg);
  }
}

/**
 *
 * @export
 * @interface ActivityEntryForApiContract
 */
export interface ActivityEntryForApiContract {
  /**
   *
   * @type {ArchivedObjectVersionForApiContract}
   * @memberof ActivityEntryForApiContract
   */
  archivedVersion?: ArchivedObjectVersionForApiContract;
  /**
   *
   * @type {UserForApiContract}
   * @memberof ActivityEntryForApiContract
   */
  author?: UserForApiContract;
  /**
   *
   * @type {Date}
   * @memberof ActivityEntryForApiContract
   */
  createDate?: Date;
  /**
   *
   * @type {string}
   * @memberof ActivityEntryForApiContract
   */
  editEvent?: ActivityEntryForApiContract.EditEventEnum;
  /**
   *
   * @type {EntryForApiContract}
   * @memberof ActivityEntryForApiContract
   */
  entry?: EntryForApiContract;
}

/**
 * @export
 * @namespace ActivityEntryForApiContract
 */
export namespace ActivityEntryForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum EditEventEnum {
    Created = <any>'Created',
    Updated = <any>'Updated',
    Deleted = <any>'Deleted',
    Restored = <any>'Restored',
  }
}

/**
 *
 * @export
 * @interface AdvancedSearchFilter
 */
export interface AdvancedSearchFilter {
  /**
   *
   * @type {string}
   * @memberof AdvancedSearchFilter
   */
  filterType?: AdvancedSearchFilter.FilterTypeEnum;
  /**
   *
   * @type {boolean}
   * @memberof AdvancedSearchFilter
   */
  negate?: boolean;
  /**
   *
   * @type {string}
   * @memberof AdvancedSearchFilter
   */
  param?: string;
}

/**
 * @export
 * @namespace AdvancedSearchFilter
 */
export namespace AdvancedSearchFilter {
  /**
   * @export
   * @enum {string}
   */
  export enum FilterTypeEnum {
    Nothing = <any>'Nothing',
    ArtistType = <any>'ArtistType',
    WebLink = <any>'WebLink',
    HasUserAccount = <any>'HasUserAccount',
    RootVoicebank = <any>'RootVoicebank',
    VoiceProvider = <any>'VoiceProvider',
    HasStoreLink = <any>'HasStoreLink',
    HasTracks = <any>'HasTracks',
    NoCoverPicture = <any>'NoCoverPicture',
    HasAlbum = <any>'HasAlbum',
    HasOriginalMedia = <any>'HasOriginalMedia',
    HasMedia = <any>'HasMedia',
    HasMultipleVoicebanks = <any>'HasMultipleVoicebanks',
    HasPublishDate = <any>'HasPublishDate',
    Lyrics = <any>'Lyrics',
    LyricsContent = <any>'LyricsContent',
  }
}

/**
 *
 * @export
 * @interface AlbumContract
 */
export interface AlbumContract {
  /**
   *
   * @type {string}
   * @memberof AlbumContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof AlbumContract
   */
  artistString?: string;
  /**
   *
   * @type {string}
   * @memberof AlbumContract
   */
  coverPictureMime?: string;
  /**
   *
   * @type {Date}
   * @memberof AlbumContract
   */
  createDate?: Date;
  /**
   *
   * @type {boolean}
   * @memberof AlbumContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof AlbumContract
   */
  discType?: AlbumContract.DiscTypeEnum;
  /**
   *
   * @type {number}
   * @memberof AlbumContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof AlbumContract
   */
  name?: string;
  /**
   *
   * @type {number}
   * @memberof AlbumContract
   */
  ratingAverage?: number;
  /**
   *
   * @type {number}
   * @memberof AlbumContract
   */
  ratingCount?: number;
  /**
   *
   * @type {OptionalDateTimeContract}
   * @memberof AlbumContract
   */
  releaseDate?: OptionalDateTimeContract;
  /**
   *
   * @type {ReleaseEventForApiContract}
   * @memberof AlbumContract
   */
  releaseEvent?: ReleaseEventForApiContract;
  /**
   *
   * @type {string}
   * @memberof AlbumContract
   */
  status?: AlbumContract.StatusEnum;
  /**
   *
   * @type {number}
   * @memberof AlbumContract
   */
  version?: number;
}

/**
 * @export
 * @namespace AlbumContract
 */
export namespace AlbumContract {
  /**
   * @export
   * @enum {string}
   */
  export enum DiscTypeEnum {
    Unknown = <any>'Unknown',
    Album = <any>'Album',
    Single = <any>'Single',
    EP = <any>'EP',
    SplitAlbum = <any>'SplitAlbum',
    Compilation = <any>'Compilation',
    Video = <any>'Video',
    Artbook = <any>'Artbook',
    Game = <any>'Game',
    Fanmade = <any>'Fanmade',
    Instrumental = <any>'Instrumental',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface AlbumDiscPropertiesContract
 */
export interface AlbumDiscPropertiesContract {
  /**
   *
   * @type {number}
   * @memberof AlbumDiscPropertiesContract
   */
  discNumber?: number;
  /**
   *
   * @type {number}
   * @memberof AlbumDiscPropertiesContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof AlbumDiscPropertiesContract
   */
  mediaType?: AlbumDiscPropertiesContract.MediaTypeEnum;
  /**
   *
   * @type {string}
   * @memberof AlbumDiscPropertiesContract
   */
  name?: string;
}

/**
 * @export
 * @namespace AlbumDiscPropertiesContract
 */
export namespace AlbumDiscPropertiesContract {
  /**
   * @export
   * @enum {string}
   */
  export enum MediaTypeEnum {
    Audio = <any>'Audio',
    Video = <any>'Video',
  }
}

/**
 *
 * @export
 * @interface AlbumForApiContract
 */
export interface AlbumForApiContract {
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {Array<ArtistForAlbumForApiContract>}
   * @memberof AlbumForApiContract
   */
  artists?: Array<ArtistForAlbumForApiContract>;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  artistString?: string;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  barcode?: string;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  catalogNumber?: string;
  /**
   *
   * @type {Date}
   * @memberof AlbumForApiContract
   */
  createDate?: Date;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  defaultName?: string;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  defaultNameLanguage?: AlbumForApiContract.DefaultNameLanguageEnum;
  /**
   *
   * @type {boolean}
   * @memberof AlbumForApiContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  description?: string;
  /**
   *
   * @type {Array<AlbumDiscPropertiesContract>}
   * @memberof AlbumForApiContract
   */
  discs?: Array<AlbumDiscPropertiesContract>;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  discType?: AlbumForApiContract.DiscTypeEnum;
  /**
   *
   * @type {number}
   * @memberof AlbumForApiContract
   */
  id?: number;
  /**
   *
   * @type {Array<AlbumIdentifierContract>}
   * @memberof AlbumForApiContract
   */
  identifiers?: Array<AlbumIdentifierContract>;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof AlbumForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {number}
   * @memberof AlbumForApiContract
   */
  mergedTo?: number;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringContract>}
   * @memberof AlbumForApiContract
   */
  names?: Array<LocalizedStringContract>;
  /**
   *
   * @type {Array<PVContract>}
   * @memberof AlbumForApiContract
   */
  pvs?: Array<PVContract>;
  /**
   *
   * @type {number}
   * @memberof AlbumForApiContract
   */
  ratingAverage?: number;
  /**
   *
   * @type {number}
   * @memberof AlbumForApiContract
   */
  ratingCount?: number;
  /**
   *
   * @type {OptionalDateTimeContract}
   * @memberof AlbumForApiContract
   */
  releaseDate?: OptionalDateTimeContract;
  /**
   *
   * @type {ReleaseEventForApiContract}
   * @memberof AlbumForApiContract
   */
  releaseEvent?: ReleaseEventForApiContract;
  /**
   *
   * @type {string}
   * @memberof AlbumForApiContract
   */
  status?: AlbumForApiContract.StatusEnum;
  /**
   *
   * @type {Array<TagUsageForApiContract>}
   * @memberof AlbumForApiContract
   */
  tags?: Array<TagUsageForApiContract>;
  /**
   *
   * @type {Array<SongInAlbumForApiContract>}
   * @memberof AlbumForApiContract
   */
  tracks?: Array<SongInAlbumForApiContract>;
  /**
   *
   * @type {number}
   * @memberof AlbumForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkForApiContract>}
   * @memberof AlbumForApiContract
   */
  webLinks?: Array<WebLinkForApiContract>;
}

/**
 * @export
 * @namespace AlbumForApiContract
 */
export namespace AlbumForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum DefaultNameLanguageEnum {
    Unspecified = <any>'Unspecified',
    Japanese = <any>'Japanese',
    Romaji = <any>'Romaji',
    English = <any>'English',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum DiscTypeEnum {
    Unknown = <any>'Unknown',
    Album = <any>'Album',
    Single = <any>'Single',
    EP = <any>'EP',
    SplitAlbum = <any>'SplitAlbum',
    Compilation = <any>'Compilation',
    Video = <any>'Video',
    Artbook = <any>'Artbook',
    Game = <any>'Game',
    Fanmade = <any>'Fanmade',
    Instrumental = <any>'Instrumental',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface AlbumForUserForApiContract
 */
export interface AlbumForUserForApiContract {
  /**
   *
   * @type {AlbumForApiContract}
   * @memberof AlbumForUserForApiContract
   */
  album?: AlbumForApiContract;
  /**
   *
   * @type {string}
   * @memberof AlbumForUserForApiContract
   */
  mediaType?: AlbumForUserForApiContract.MediaTypeEnum;
  /**
   *
   * @type {string}
   * @memberof AlbumForUserForApiContract
   */
  purchaseStatus?: AlbumForUserForApiContract.PurchaseStatusEnum;
  /**
   *
   * @type {number}
   * @memberof AlbumForUserForApiContract
   */
  rating?: number;
  /**
   *
   * @type {UserForApiContract}
   * @memberof AlbumForUserForApiContract
   */
  user?: UserForApiContract;
}

/**
 * @export
 * @namespace AlbumForUserForApiContract
 */
export namespace AlbumForUserForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum MediaTypeEnum {
    PhysicalDisc = <any>'PhysicalDisc',
    DigitalDownload = <any>'DigitalDownload',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum PurchaseStatusEnum {
    Nothing = <any>'Nothing',
    Wishlisted = <any>'Wishlisted',
    Ordered = <any>'Ordered',
    Owned = <any>'Owned',
  }
}

/**
 *
 * @export
 * @interface AlbumIdentifierContract
 */
export interface AlbumIdentifierContract {
  /**
   *
   * @type {string}
   * @memberof AlbumIdentifierContract
   */
  value?: string;
}

/**
 *
 * @export
 * @interface AlbumReviewContract
 */
export interface AlbumReviewContract {
  /**
   *
   * @type {number}
   * @memberof AlbumReviewContract
   */
  id?: number;
  /**
   *
   * @type {number}
   * @memberof AlbumReviewContract
   */
  albumId?: number;
  /**
   *
   * @type {Date}
   * @memberof AlbumReviewContract
   */
  date?: Date;
  /**
   *
   * @type {string}
   * @memberof AlbumReviewContract
   */
  languageCode?: string;
  /**
   *
   * @type {string}
   * @memberof AlbumReviewContract
   */
  text?: string;
  /**
   *
   * @type {string}
   * @memberof AlbumReviewContract
   */
  title?: string;
  /**
   *
   * @type {UserForApiContract}
   * @memberof AlbumReviewContract
   */
  user?: UserForApiContract;
}

/**
 *
 * @export
 * @interface ArchivedObjectVersionForApiContract
 */
export interface ArchivedObjectVersionForApiContract {
  /**
   *
   * @type {Array<string>}
   * @memberof ArchivedObjectVersionForApiContract
   */
  changedFields?: Array<string>;
  /**
   *
   * @type {number}
   * @memberof ArchivedObjectVersionForApiContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof ArchivedObjectVersionForApiContract
   */
  notes?: string;
  /**
   *
   * @type {number}
   * @memberof ArchivedObjectVersionForApiContract
   */
  version?: number;
}

/**
 *
 * @export
 * @interface ArchivedWebLinkContract
 */
export interface ArchivedWebLinkContract {
  /**
   *
   * @type {string}
   * @memberof ArchivedWebLinkContract
   */
  category?: ArchivedWebLinkContract.CategoryEnum;
  /**
   *
   * @type {string}
   * @memberof ArchivedWebLinkContract
   */
  description?: string;
  /**
   *
   * @type {string}
   * @memberof ArchivedWebLinkContract
   */
  url?: string;
}

/**
 * @export
 * @namespace ArchivedWebLinkContract
 */
export namespace ArchivedWebLinkContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoryEnum {
    Official = <any>'Official',
    Commercial = <any>'Commercial',
    Reference = <any>'Reference',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface ArtistContract
 */
export interface ArtistContract {
  /**
   *
   * @type {string}
   * @memberof ArtistContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof ArtistContract
   */
  artistType?: ArtistContract.ArtistTypeEnum;
  /**
   *
   * @type {boolean}
   * @memberof ArtistContract
   */
  deleted?: boolean;
  /**
   *
   * @type {number}
   * @memberof ArtistContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof ArtistContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof ArtistContract
   */
  pictureMime?: string;
  /**
   *
   * @type {Date}
   * @memberof ArtistContract
   */
  releaseDate?: Date;
  /**
   *
   * @type {string}
   * @memberof ArtistContract
   */
  status?: ArtistContract.StatusEnum;
  /**
   *
   * @type {number}
   * @memberof ArtistContract
   */
  version?: number;
}

/**
 * @export
 * @namespace ArtistContract
 */
export namespace ArtistContract {
  /**
   * @export
   * @enum {string}
   */
  export enum ArtistTypeEnum {
    Unknown = <any>'Unknown',
    Circle = <any>'Circle',
    Label = <any>'Label',
    Producer = <any>'Producer',
    Animator = <any>'Animator',
    Illustrator = <any>'Illustrator',
    Lyricist = <any>'Lyricist',
    Vocaloid = <any>'Vocaloid',
    UTAU = <any>'UTAU',
    CeVIO = <any>'CeVIO',
    OtherVoiceSynthesizer = <any>'OtherVoiceSynthesizer',
    OtherVocalist = <any>'OtherVocalist',
    OtherGroup = <any>'OtherGroup',
    OtherIndividual = <any>'OtherIndividual',
    Utaite = <any>'Utaite',
    Band = <any>'Band',
    Vocalist = <any>'Vocalist',
    Character = <any>'Character',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface ArtistForAlbumForApiContract
 */
export interface ArtistForAlbumForApiContract {
  /**
   *
   * @type {ArtistContract}
   * @memberof ArtistForAlbumForApiContract
   */
  artist?: ArtistContract;
  /**
   *
   * @type {string}
   * @memberof ArtistForAlbumForApiContract
   */
  categories?: ArtistForAlbumForApiContract.CategoriesEnum;
  /**
   *
   * @type {string}
   * @memberof ArtistForAlbumForApiContract
   */
  effectiveRoles?: ArtistForAlbumForApiContract.EffectiveRolesEnum;
  /**
   *
   * @type {boolean}
   * @memberof ArtistForAlbumForApiContract
   */
  isSupport?: boolean;
  /**
   *
   * @type {string}
   * @memberof ArtistForAlbumForApiContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof ArtistForAlbumForApiContract
   */
  roles?: ArtistForAlbumForApiContract.RolesEnum;
}

/**
 * @export
 * @namespace ArtistForAlbumForApiContract
 */
export namespace ArtistForAlbumForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoriesEnum {
    Nothing = <any>'Nothing',
    Vocalist = <any>'Vocalist',
    Producer = <any>'Producer',
    Animator = <any>'Animator',
    Label = <any>'Label',
    Circle = <any>'Circle',
    Other = <any>'Other',
    Band = <any>'Band',
    Illustrator = <any>'Illustrator',
    Subject = <any>'Subject',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum EffectiveRolesEnum {
    Default = <any>'Default',
    Animator = <any>'Animator',
    Arranger = <any>'Arranger',
    Composer = <any>'Composer',
    Distributor = <any>'Distributor',
    Illustrator = <any>'Illustrator',
    Instrumentalist = <any>'Instrumentalist',
    Lyricist = <any>'Lyricist',
    Mastering = <any>'Mastering',
    Publisher = <any>'Publisher',
    Vocalist = <any>'Vocalist',
    VoiceManipulator = <any>'VoiceManipulator',
    Other = <any>'Other',
    Mixer = <any>'Mixer',
    Chorus = <any>'Chorus',
    Encoder = <any>'Encoder',
    VocalDataProvider = <any>'VocalDataProvider',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum RolesEnum {
    Default = <any>'Default',
    Animator = <any>'Animator',
    Arranger = <any>'Arranger',
    Composer = <any>'Composer',
    Distributor = <any>'Distributor',
    Illustrator = <any>'Illustrator',
    Instrumentalist = <any>'Instrumentalist',
    Lyricist = <any>'Lyricist',
    Mastering = <any>'Mastering',
    Publisher = <any>'Publisher',
    Vocalist = <any>'Vocalist',
    VoiceManipulator = <any>'VoiceManipulator',
    Other = <any>'Other',
    Mixer = <any>'Mixer',
    Chorus = <any>'Chorus',
    Encoder = <any>'Encoder',
    VocalDataProvider = <any>'VocalDataProvider',
  }
}

/**
 *
 * @export
 * @interface ArtistForApiContract
 */
export interface ArtistForApiContract {
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {Array<ArtistForArtistForApiContract>}
   * @memberof ArtistForApiContract
   */
  artistLinks?: Array<ArtistForArtistForApiContract>;
  /**
   *
   * @type {Array<ArtistForArtistForApiContract>}
   * @memberof ArtistForApiContract
   */
  artistLinksReverse?: Array<ArtistForArtistForApiContract>;
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  artistType?: ArtistForApiContract.ArtistTypeEnum;
  /**
   *
   * @type {ArtistContract}
   * @memberof ArtistForApiContract
   */
  baseVoicebank?: ArtistContract;
  /**
   *
   * @type {Date}
   * @memberof ArtistForApiContract
   */
  createDate?: Date;
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  defaultName?: string;
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  defaultNameLanguage?: ArtistForApiContract.DefaultNameLanguageEnum;
  /**
   *
   * @type {boolean}
   * @memberof ArtistForApiContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  description?: string;
  /**
   *
   * @type {number}
   * @memberof ArtistForApiContract
   */
  id?: number;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof ArtistForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {number}
   * @memberof ArtistForApiContract
   */
  mergedTo?: number;
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringContract>}
   * @memberof ArtistForApiContract
   */
  names?: Array<LocalizedStringContract>;
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  pictureMime?: string;
  /**
   *
   * @type {ArtistRelationsForApi}
   * @memberof ArtistForApiContract
   */
  relations?: ArtistRelationsForApi;
  /**
   *
   * @type {Date}
   * @memberof ArtistForApiContract
   */
  releaseDate?: Date;
  /**
   *
   * @type {string}
   * @memberof ArtistForApiContract
   */
  status?: ArtistForApiContract.StatusEnum;
  /**
   *
   * @type {Array<TagUsageForApiContract>}
   * @memberof ArtistForApiContract
   */
  tags?: Array<TagUsageForApiContract>;
  /**
   *
   * @type {number}
   * @memberof ArtistForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkForApiContract>}
   * @memberof ArtistForApiContract
   */
  webLinks?: Array<WebLinkForApiContract>;
}

/**
 * @export
 * @namespace ArtistForApiContract
 */
export namespace ArtistForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum ArtistTypeEnum {
    Unknown = <any>'Unknown',
    Circle = <any>'Circle',
    Label = <any>'Label',
    Producer = <any>'Producer',
    Animator = <any>'Animator',
    Illustrator = <any>'Illustrator',
    Lyricist = <any>'Lyricist',
    Vocaloid = <any>'Vocaloid',
    UTAU = <any>'UTAU',
    CeVIO = <any>'CeVIO',
    OtherVoiceSynthesizer = <any>'OtherVoiceSynthesizer',
    OtherVocalist = <any>'OtherVocalist',
    OtherGroup = <any>'OtherGroup',
    OtherIndividual = <any>'OtherIndividual',
    Utaite = <any>'Utaite',
    Band = <any>'Band',
    Vocalist = <any>'Vocalist',
    Character = <any>'Character',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum DefaultNameLanguageEnum {
    Unspecified = <any>'Unspecified',
    Japanese = <any>'Japanese',
    Romaji = <any>'Romaji',
    English = <any>'English',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface ArtistForArtistForApiContract
 */
export interface ArtistForArtistForApiContract {
  /**
   *
   * @type {ArtistContract}
   * @memberof ArtistForArtistForApiContract
   */
  artist?: ArtistContract;
  /**
   *
   * @type {string}
   * @memberof ArtistForArtistForApiContract
   */
  linkType?: ArtistForArtistForApiContract.LinkTypeEnum;
}

/**
 * @export
 * @namespace ArtistForArtistForApiContract
 */
export namespace ArtistForArtistForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum LinkTypeEnum {
    CharacterDesigner = <any>'CharacterDesigner',
    Group = <any>'Group',
    Illustrator = <any>'Illustrator',
    Manager = <any>'Manager',
    VoiceProvider = <any>'VoiceProvider',
  }
}

/**
 *
 * @export
 * @interface ArtistForEventContract
 */
export interface ArtistForEventContract {
  /**
   *
   * @type {ArtistContract}
   * @memberof ArtistForEventContract
   */
  artist?: ArtistContract;
  /**
   *
   * @type {string}
   * @memberof ArtistForEventContract
   */
  effectiveRoles?: ArtistForEventContract.EffectiveRolesEnum;
  /**
   *
   * @type {number}
   * @memberof ArtistForEventContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof ArtistForEventContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof ArtistForEventContract
   */
  roles?: ArtistForEventContract.RolesEnum;
}

/**
 * @export
 * @namespace ArtistForEventContract
 */
export namespace ArtistForEventContract {
  /**
   * @export
   * @enum {string}
   */
  export enum EffectiveRolesEnum {
    Default = <any>'Default',
    Dancer = <any>'Dancer',
    DJ = <any>'DJ',
    Instrumentalist = <any>'Instrumentalist',
    Organizer = <any>'Organizer',
    Promoter = <any>'Promoter',
    VJ = <any>'VJ',
    Vocalist = <any>'Vocalist',
    VoiceManipulator = <any>'VoiceManipulator',
    OtherPerformer = <any>'OtherPerformer',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum RolesEnum {
    Default = <any>'Default',
    Dancer = <any>'Dancer',
    DJ = <any>'DJ',
    Instrumentalist = <any>'Instrumentalist',
    Organizer = <any>'Organizer',
    Promoter = <any>'Promoter',
    VJ = <any>'VJ',
    Vocalist = <any>'Vocalist',
    VoiceManipulator = <any>'VoiceManipulator',
    OtherPerformer = <any>'OtherPerformer',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface ArtistForSongContract
 */
export interface ArtistForSongContract {
  /**
   *
   * @type {ArtistContract}
   * @memberof ArtistForSongContract
   */
  artist?: ArtistContract;
  /**
   *
   * @type {string}
   * @memberof ArtistForSongContract
   */
  categories?: ArtistForSongContract.CategoriesEnum;
  /**
   *
   * @type {string}
   * @memberof ArtistForSongContract
   */
  effectiveRoles?: ArtistForSongContract.EffectiveRolesEnum;
  /**
   *
   * @type {number}
   * @memberof ArtistForSongContract
   */
  id?: number;
  /**
   *
   * @type {boolean}
   * @memberof ArtistForSongContract
   */
  isCustomName?: boolean;
  /**
   *
   * @type {boolean}
   * @memberof ArtistForSongContract
   */
  isSupport?: boolean;
  /**
   *
   * @type {string}
   * @memberof ArtistForSongContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof ArtistForSongContract
   */
  roles?: ArtistForSongContract.RolesEnum;
}

/**
 * @export
 * @namespace ArtistForSongContract
 */
export namespace ArtistForSongContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoriesEnum {
    Nothing = <any>'Nothing',
    Vocalist = <any>'Vocalist',
    Producer = <any>'Producer',
    Animator = <any>'Animator',
    Label = <any>'Label',
    Circle = <any>'Circle',
    Other = <any>'Other',
    Band = <any>'Band',
    Illustrator = <any>'Illustrator',
    Subject = <any>'Subject',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum EffectiveRolesEnum {
    Default = <any>'Default',
    Animator = <any>'Animator',
    Arranger = <any>'Arranger',
    Composer = <any>'Composer',
    Distributor = <any>'Distributor',
    Illustrator = <any>'Illustrator',
    Instrumentalist = <any>'Instrumentalist',
    Lyricist = <any>'Lyricist',
    Mastering = <any>'Mastering',
    Publisher = <any>'Publisher',
    Vocalist = <any>'Vocalist',
    VoiceManipulator = <any>'VoiceManipulator',
    Other = <any>'Other',
    Mixer = <any>'Mixer',
    Chorus = <any>'Chorus',
    Encoder = <any>'Encoder',
    VocalDataProvider = <any>'VocalDataProvider',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum RolesEnum {
    Default = <any>'Default',
    Animator = <any>'Animator',
    Arranger = <any>'Arranger',
    Composer = <any>'Composer',
    Distributor = <any>'Distributor',
    Illustrator = <any>'Illustrator',
    Instrumentalist = <any>'Instrumentalist',
    Lyricist = <any>'Lyricist',
    Mastering = <any>'Mastering',
    Publisher = <any>'Publisher',
    Vocalist = <any>'Vocalist',
    VoiceManipulator = <any>'VoiceManipulator',
    Other = <any>'Other',
    Mixer = <any>'Mixer',
    Chorus = <any>'Chorus',
    Encoder = <any>'Encoder',
    VocalDataProvider = <any>'VocalDataProvider',
  }
}

/**
 *
 * @export
 * @interface ArtistForUserForApiContract
 */
export interface ArtistForUserForApiContract {
  /**
   *
   * @type {ArtistForApiContract}
   * @memberof ArtistForUserForApiContract
   */
  artist?: ArtistForApiContract;
}

/**
 *
 * @export
 * @interface ArtistRelationsForApi
 */
export interface ArtistRelationsForApi {
  /**
   *
   * @type {Array<AlbumForApiContract>}
   * @memberof ArtistRelationsForApi
   */
  latestAlbums?: Array<AlbumForApiContract>;
  /**
   *
   * @type {Array<ReleaseEventForApiContract>}
   * @memberof ArtistRelationsForApi
   */
  latestEvents?: Array<ReleaseEventForApiContract>;
  /**
   *
   * @type {Array<SongForApiContract>}
   * @memberof ArtistRelationsForApi
   */
  latestSongs?: Array<SongForApiContract>;
  /**
   *
   * @type {Array<AlbumForApiContract>}
   * @memberof ArtistRelationsForApi
   */
  popularAlbums?: Array<AlbumForApiContract>;
  /**
   *
   * @type {Array<SongForApiContract>}
   * @memberof ArtistRelationsForApi
   */
  popularSongs?: Array<SongForApiContract>;
}

/**
 *
 * @export
 * @interface CommentForApiContract
 */
export interface CommentForApiContract {
  /**
   *
   * @type {UserForApiContract}
   * @memberof CommentForApiContract
   */
  author?: UserForApiContract;
  /**
   *
   * @type {string}
   * @memberof CommentForApiContract
   */
  authorName?: string;
  /**
   *
   * @type {Date}
   * @memberof CommentForApiContract
   */
  created?: Date;
  /**
   *
   * @type {EntryRefContract}
   * @memberof CommentForApiContract
   */
  entry?: EntryRefContract;
  /**
   *
   * @type {number}
   * @memberof CommentForApiContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof CommentForApiContract
   */
  message?: string;
}

/**
 *
 * @export
 * @interface CreateReportModel
 */
export interface CreateReportModel {
  /**
   *
   * @type {string}
   * @memberof CreateReportModel
   */
  reportType?: CreateReportModel.ReportTypeEnum;
  /**
   *
   * @type {string}
   * @memberof CreateReportModel
   */
  reason?: string;
}

/**
 * @export
 * @namespace CreateReportModel
 */
export namespace CreateReportModel {
  /**
   * @export
   * @enum {string}
   */
  export enum ReportTypeEnum {
    MaliciousIP = <any>'MaliciousIP',
    Spamming = <any>'Spamming',
    RemovePermissions = <any>'RemovePermissions',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface DiscussionFolderContract
 */
export interface DiscussionFolderContract {
  /**
   *
   * @type {string}
   * @memberof DiscussionFolderContract
   */
  description?: string;
  /**
   *
   * @type {number}
   * @memberof DiscussionFolderContract
   */
  id?: number;
  /**
   *
   * @type {UserForApiContract}
   * @memberof DiscussionFolderContract
   */
  lastTopicAuthor?: UserForApiContract;
  /**
   *
   * @type {Date}
   * @memberof DiscussionFolderContract
   */
  lastTopicDate?: Date;
  /**
   *
   * @type {string}
   * @memberof DiscussionFolderContract
   */
  name?: string;
  /**
   *
   * @type {number}
   * @memberof DiscussionFolderContract
   */
  topicCount?: number;
}

/**
 *
 * @export
 * @interface DiscussionTopicContract
 */
export interface DiscussionTopicContract {
  /**
   *
   * @type {UserForApiContract}
   * @memberof DiscussionTopicContract
   */
  author?: UserForApiContract;
  /**
   *
   * @type {number}
   * @memberof DiscussionTopicContract
   */
  commentCount?: number;
  /**
   *
   * @type {Array<CommentForApiContract>}
   * @memberof DiscussionTopicContract
   */
  comments?: Array<CommentForApiContract>;
  /**
   *
   * @type {string}
   * @memberof DiscussionTopicContract
   */
  content?: string;
  /**
   *
   * @type {Date}
   * @memberof DiscussionTopicContract
   */
  created?: Date;
  /**
   *
   * @type {number}
   * @memberof DiscussionTopicContract
   */
  folderId?: number;
  /**
   *
   * @type {number}
   * @memberof DiscussionTopicContract
   */
  id?: number;
  /**
   *
   * @type {CommentForApiContract}
   * @memberof DiscussionTopicContract
   */
  lastComment?: CommentForApiContract;
  /**
   *
   * @type {boolean}
   * @memberof DiscussionTopicContract
   */
  locked?: boolean;
  /**
   *
   * @type {string}
   * @memberof DiscussionTopicContract
   */
  name?: string;
}

/**
 *
 * @export
 * @interface EnglishTranslatedStringContract
 */
export interface EnglishTranslatedStringContract {
  /**
   *
   * @type {string}
   * @memberof EnglishTranslatedStringContract
   */
  english?: string;
  /**
   *
   * @type {string}
   * @memberof EnglishTranslatedStringContract
   */
  original?: string;
}

/**
 *
 * @export
 * @interface EntryForApiContract
 */
export interface EntryForApiContract {
  /**
   *
   * @type {Date}
   * @memberof EntryForApiContract
   */
  activityDate?: Date;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  artistString?: string;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  artistType?: EntryForApiContract.ArtistTypeEnum;
  /**
   *
   * @type {Date}
   * @memberof EntryForApiContract
   */
  createDate?: Date;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  defaultName?: string;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  defaultNameLanguage?: EntryForApiContract.DefaultNameLanguageEnum;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  description?: string;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  discType?: EntryForApiContract.DiscTypeEnum;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  entryType?: EntryForApiContract.EntryTypeEnum;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  eventCategory?: EntryForApiContract.EventCategoryEnum;
  /**
   *
   * @type {number}
   * @memberof EntryForApiContract
   */
  id?: number;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof EntryForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringContract>}
   * @memberof EntryForApiContract
   */
  names?: Array<LocalizedStringContract>;
  /**
   *
   * @type {Array<PVContract>}
   * @memberof EntryForApiContract
   */
  pVs?: Array<PVContract>;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  songListFeaturedCategory?: EntryForApiContract.SongListFeaturedCategoryEnum;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  songType?: EntryForApiContract.SongTypeEnum;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  status?: EntryForApiContract.StatusEnum;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  releaseEventSeriesName?: string;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  tagCategoryName?: string;
  /**
   *
   * @type {Array<TagUsageForApiContract>}
   * @memberof EntryForApiContract
   */
  tags?: Array<TagUsageForApiContract>;
  /**
   *
   * @type {string}
   * @memberof EntryForApiContract
   */
  urlSlug?: string;
  /**
   *
   * @type {number}
   * @memberof EntryForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<ArchivedWebLinkContract>}
   * @memberof EntryForApiContract
   */
  webLinks?: Array<ArchivedWebLinkContract>;
}

/**
 * @export
 * @namespace EntryForApiContract
 */
export namespace EntryForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum ArtistTypeEnum {
    Unknown = <any>'Unknown',
    Circle = <any>'Circle',
    Label = <any>'Label',
    Producer = <any>'Producer',
    Animator = <any>'Animator',
    Illustrator = <any>'Illustrator',
    Lyricist = <any>'Lyricist',
    Vocaloid = <any>'Vocaloid',
    UTAU = <any>'UTAU',
    CeVIO = <any>'CeVIO',
    OtherVoiceSynthesizer = <any>'OtherVoiceSynthesizer',
    OtherVocalist = <any>'OtherVocalist',
    OtherGroup = <any>'OtherGroup',
    OtherIndividual = <any>'OtherIndividual',
    Utaite = <any>'Utaite',
    Band = <any>'Band',
    Vocalist = <any>'Vocalist',
    Character = <any>'Character',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum DefaultNameLanguageEnum {
    Unspecified = <any>'Unspecified',
    Japanese = <any>'Japanese',
    Romaji = <any>'Romaji',
    English = <any>'English',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum DiscTypeEnum {
    Unknown = <any>'Unknown',
    Album = <any>'Album',
    Single = <any>'Single',
    EP = <any>'EP',
    SplitAlbum = <any>'SplitAlbum',
    Compilation = <any>'Compilation',
    Video = <any>'Video',
    Artbook = <any>'Artbook',
    Game = <any>'Game',
    Fanmade = <any>'Fanmade',
    Instrumental = <any>'Instrumental',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum EntryTypeEnum {
    Undefined = <any>'Undefined',
    Album = <any>'Album',
    Artist = <any>'Artist',
    DiscussionTopic = <any>'DiscussionTopic',
    PV = <any>'PV',
    ReleaseEvent = <any>'ReleaseEvent',
    ReleaseEventSeries = <any>'ReleaseEventSeries',
    Song = <any>'Song',
    SongList = <any>'SongList',
    Tag = <any>'Tag',
    User = <any>'User',
    Venue = <any>'Venue',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum EventCategoryEnum {
    Unspecified = <any>'Unspecified',
    AlbumRelease = <any>'AlbumRelease',
    Anniversary = <any>'Anniversary',
    Club = <any>'Club',
    Concert = <any>'Concert',
    Contest = <any>'Contest',
    Convention = <any>'Convention',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum SongListFeaturedCategoryEnum {
    Nothing = <any>'Nothing',
    Concerts = <any>'Concerts',
    VocaloidRanking = <any>'VocaloidRanking',
    Pools = <any>'Pools',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum SongTypeEnum {
    Unspecified = <any>'Unspecified',
    Original = <any>'Original',
    Remaster = <any>'Remaster',
    Remix = <any>'Remix',
    Cover = <any>'Cover',
    Arrangement = <any>'Arrangement',
    Instrumental = <any>'Instrumental',
    Mashup = <any>'Mashup',
    MusicPV = <any>'MusicPV',
    DramaPV = <any>'DramaPV',
    Live = <any>'Live',
    Illustration = <any>'Illustration',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface EntryRefContract
 */
export interface EntryRefContract {
  /**
   *
   * @type {string}
   * @memberof EntryRefContract
   */
  entryType?: EntryRefContract.EntryTypeEnum;
  /**
   *
   * @type {number}
   * @memberof EntryRefContract
   */
  id?: number;
}

/**
 * @export
 * @namespace EntryRefContract
 */
export namespace EntryRefContract {
  /**
   * @export
   * @enum {string}
   */
  export enum EntryTypeEnum {
    Undefined = <any>'Undefined',
    Album = <any>'Album',
    Artist = <any>'Artist',
    DiscussionTopic = <any>'DiscussionTopic',
    PV = <any>'PV',
    ReleaseEvent = <any>'ReleaseEvent',
    ReleaseEventSeries = <any>'ReleaseEventSeries',
    Song = <any>'Song',
    SongList = <any>'SongList',
    Tag = <any>'Tag',
    User = <any>'User',
    Venue = <any>'Venue',
  }
}

/**
 *
 * @export
 * @interface EntryThumbContract
 */
export interface EntryThumbContract {
  /**
   *
   * @type {string}
   * @memberof EntryThumbContract
   */
  entryType?: EntryThumbContract.EntryTypeEnum;
  /**
   *
   * @type {number}
   * @memberof EntryThumbContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof EntryThumbContract
   */
  mime?: string;
  /**
   *
   * @type {number}
   * @memberof EntryThumbContract
   */
  version?: number;
}

/**
 * @export
 * @namespace EntryThumbContract
 */
export namespace EntryThumbContract {
  /**
   * @export
   * @enum {string}
   */
  export enum EntryTypeEnum {
    Undefined = <any>'Undefined',
    Album = <any>'Album',
    Artist = <any>'Artist',
    DiscussionTopic = <any>'DiscussionTopic',
    PV = <any>'PV',
    ReleaseEvent = <any>'ReleaseEvent',
    ReleaseEventSeries = <any>'ReleaseEventSeries',
    Song = <any>'Song',
    SongList = <any>'SongList',
    Tag = <any>'Tag',
    User = <any>'User',
    Venue = <any>'Venue',
  }
}

/**
 *
 * @export
 * @interface EntryThumbForApiContract
 */
export interface EntryThumbForApiContract {
  /**
   *
   * @type {string}
   * @memberof EntryThumbForApiContract
   */
  mime?: string;
  /**
   *
   * @type {string}
   * @memberof EntryThumbForApiContract
   */
  urlOriginal?: string;
  /**
   *
   * @type {string}
   * @memberof EntryThumbForApiContract
   */
  urlSmallThumb?: string;
  /**
   *
   * @type {string}
   * @memberof EntryThumbForApiContract
   */
  urlThumb?: string;
  /**
   *
   * @type {string}
   * @memberof EntryThumbForApiContract
   */
  urlTinyThumb?: string;
}

/**
 *
 * @export
 * @interface LocalizedStringContract
 */
export interface LocalizedStringContract {
  /**
   *
   * @type {string}
   * @memberof LocalizedStringContract
   */
  language?: LocalizedStringContract.LanguageEnum;
  /**
   *
   * @type {string}
   * @memberof LocalizedStringContract
   */
  value?: string;
}

/**
 * @export
 * @namespace LocalizedStringContract
 */
export namespace LocalizedStringContract {
  /**
   * @export
   * @enum {string}
   */
  export enum LanguageEnum {
    Unspecified = <any>'Unspecified',
    Japanese = <any>'Japanese',
    Romaji = <any>'Romaji',
    English = <any>'English',
  }
}

/**
 *
 * @export
 * @interface LocalizedStringWithIdContract
 */
export interface LocalizedStringWithIdContract {
  /**
   *
   * @type {number}
   * @memberof LocalizedStringWithIdContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof LocalizedStringWithIdContract
   */
  language?: LocalizedStringWithIdContract.LanguageEnum;
  /**
   *
   * @type {string}
   * @memberof LocalizedStringWithIdContract
   */
  value?: string;
}

/**
 * @export
 * @namespace LocalizedStringWithIdContract
 */
export namespace LocalizedStringWithIdContract {
  /**
   * @export
   * @enum {string}
   */
  export enum LanguageEnum {
    Unspecified = <any>'Unspecified',
    Japanese = <any>'Japanese',
    Romaji = <any>'Romaji',
    English = <any>'English',
  }
}

/**
 *
 * @export
 * @interface LyricsForSongContract
 */
export interface LyricsForSongContract {
  /**
   *
   * @type {string}
   * @memberof LyricsForSongContract
   */
  cultureCode?: string;
  /**
   *
   * @type {number}
   * @memberof LyricsForSongContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof LyricsForSongContract
   */
  source?: string;
  /**
   *
   * @type {string}
   * @memberof LyricsForSongContract
   */
  translationType?: LyricsForSongContract.TranslationTypeEnum;
  /**
   *
   * @type {string}
   * @memberof LyricsForSongContract
   */
  url?: string;
  /**
   *
   * @type {string}
   * @memberof LyricsForSongContract
   */
  value?: string;
}

/**
 * @export
 * @namespace LyricsForSongContract
 */
export namespace LyricsForSongContract {
  /**
   * @export
   * @enum {string}
   */
  export enum TranslationTypeEnum {
    Original = <any>'Original',
    Romanized = <any>'Romanized',
    Translation = <any>'Translation',
  }
}

/**
 *
 * @export
 * @interface OldUsernameContract
 */
export interface OldUsernameContract {
  /**
   *
   * @type {Date}
   * @memberof OldUsernameContract
   */
  date?: Date;
  /**
   *
   * @type {string}
   * @memberof OldUsernameContract
   */
  oldName?: string;
}

/**
 *
 * @export
 * @interface OptionalDateTimeContract
 */
export interface OptionalDateTimeContract {
  /**
   *
   * @type {number}
   * @memberof OptionalDateTimeContract
   */
  day?: number;
  /**
   *
   * @type {string}
   * @memberof OptionalDateTimeContract
   */
  formatted?: string;
  /**
   *
   * @type {boolean}
   * @memberof OptionalDateTimeContract
   */
  isEmpty?: boolean;
  /**
   *
   * @type {number}
   * @memberof OptionalDateTimeContract
   */
  month?: number;
  /**
   *
   * @type {number}
   * @memberof OptionalDateTimeContract
   */
  year?: number;
}

/**
 *
 * @export
 * @interface OptionalGeoPointContract
 */
export interface OptionalGeoPointContract {
  /**
   *
   * @type {string}
   * @memberof OptionalGeoPointContract
   */
  formatted?: string;
  /**
   *
   * @type {boolean}
   * @memberof OptionalGeoPointContract
   */
  hasValue?: boolean;
  /**
   *
   * @type {number}
   * @memberof OptionalGeoPointContract
   */
  latitude?: number;
  /**
   *
   * @type {number}
   * @memberof OptionalGeoPointContract
   */
  longitude?: number;
}

/**
 *
 * @export
 * @interface PVContract
 */
export interface PVContract {
  /**
   *
   * @type {string}
   * @memberof PVContract
   */
  author?: string;
  /**
   *
   * @type {number}
   * @memberof PVContract
   */
  createdBy?: number;
  /**
   *
   * @type {boolean}
   * @memberof PVContract
   */
  disabled?: boolean;
  /**
   *
   * @type {PVExtendedMetadata}
   * @memberof PVContract
   */
  extendedMetadata?: PVExtendedMetadata;
  /**
   *
   * @type {number}
   * @memberof PVContract
   */
  id?: number;
  /**
   *
   * @type {number}
   * @memberof PVContract
   */
  length?: number;
  /**
   *
   * @type {string}
   * @memberof PVContract
   */
  name?: string;
  /**
   *
   * @type {Date}
   * @memberof PVContract
   */
  publishDate?: Date;
  /**
   *
   * @type {string}
   * @memberof PVContract
   */
  pvId?: string;
  /**
   *
   * @type {string}
   * @memberof PVContract
   */
  service?: PVContract.ServiceEnum;
  /**
   *
   * @type {string}
   * @memberof PVContract
   */
  pvType?: PVContract.PvTypeEnum;
  /**
   *
   * @type {string}
   * @memberof PVContract
   */
  thumbUrl?: string;
  /**
   *
   * @type {string}
   * @memberof PVContract
   */
  url?: string;
}

/**
 * @export
 * @namespace PVContract
 */
export namespace PVContract {
  /**
   * @export
   * @enum {string}
   */
  export enum ServiceEnum {
    NicoNicoDouga = <any>'NicoNicoDouga',
    Youtube = <any>'Youtube',
    SoundCloud = <any>'SoundCloud',
    Vimeo = <any>'Vimeo',
    Piapro = <any>'Piapro',
    Bilibili = <any>'Bilibili',
    File = <any>'File',
    LocalFile = <any>'LocalFile',
    Creofuga = <any>'Creofuga',
    Bandcamp = <any>'Bandcamp',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum PvTypeEnum {
    Original = <any>'Original',
    Reprint = <any>'Reprint',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface PVExtendedMetadata
 */
export interface PVExtendedMetadata {
  /**
   *
   * @type {string}
   * @memberof PVExtendedMetadata
   */
  json?: string;
}

/**
 *
 * @export
 * @interface PVForSongContract
 */
export interface PVForSongContract {
  /**
   *
   * @type {SongContract}
   * @memberof PVForSongContract
   */
  song?: SongContract;
  /**
   *
   * @type {string}
   * @memberof PVForSongContract
   */
  author?: string;
  /**
   *
   * @type {number}
   * @memberof PVForSongContract
   */
  createdBy?: number;
  /**
   *
   * @type {boolean}
   * @memberof PVForSongContract
   */
  disabled?: boolean;
  /**
   *
   * @type {PVExtendedMetadata}
   * @memberof PVForSongContract
   */
  extendedMetadata?: PVExtendedMetadata;
  /**
   *
   * @type {number}
   * @memberof PVForSongContract
   */
  id?: number;
  /**
   *
   * @type {number}
   * @memberof PVForSongContract
   */
  length?: number;
  /**
   *
   * @type {string}
   * @memberof PVForSongContract
   */
  name?: string;
  /**
   *
   * @type {Date}
   * @memberof PVForSongContract
   */
  publishDate?: Date;
  /**
   *
   * @type {string}
   * @memberof PVForSongContract
   */
  pvId?: string;
  /**
   *
   * @type {string}
   * @memberof PVForSongContract
   */
  service?: PVForSongContract.ServiceEnum;
  /**
   *
   * @type {string}
   * @memberof PVForSongContract
   */
  pvType?: PVForSongContract.PvTypeEnum;
  /**
   *
   * @type {string}
   * @memberof PVForSongContract
   */
  thumbUrl?: string;
  /**
   *
   * @type {string}
   * @memberof PVForSongContract
   */
  url?: string;
}

/**
 * @export
 * @namespace PVForSongContract
 */
export namespace PVForSongContract {
  /**
   * @export
   * @enum {string}
   */
  export enum ServiceEnum {
    NicoNicoDouga = <any>'NicoNicoDouga',
    Youtube = <any>'Youtube',
    SoundCloud = <any>'SoundCloud',
    Vimeo = <any>'Vimeo',
    Piapro = <any>'Piapro',
    Bilibili = <any>'Bilibili',
    File = <any>'File',
    LocalFile = <any>'LocalFile',
    Creofuga = <any>'Creofuga',
    Bandcamp = <any>'Bandcamp',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum PvTypeEnum {
    Original = <any>'Original',
    Reprint = <any>'Reprint',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface PartialFindResultActivityEntryForApiContract
 */
export interface PartialFindResultActivityEntryForApiContract {
  /**
   *
   * @type {Array<ActivityEntryForApiContract>}
   * @memberof PartialFindResultActivityEntryForApiContract
   */
  items?: Array<ActivityEntryForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultActivityEntryForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultActivityEntryForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultAlbumForApiContract
 */
export interface PartialFindResultAlbumForApiContract {
  /**
   *
   * @type {Array<AlbumForApiContract>}
   * @memberof PartialFindResultAlbumForApiContract
   */
  items?: Array<AlbumForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultAlbumForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultAlbumForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultAlbumForUserForApiContract
 */
export interface PartialFindResultAlbumForUserForApiContract {
  /**
   *
   * @type {Array<AlbumForUserForApiContract>}
   * @memberof PartialFindResultAlbumForUserForApiContract
   */
  items?: Array<AlbumForUserForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultAlbumForUserForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultAlbumForUserForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultArtistForApiContract
 */
export interface PartialFindResultArtistForApiContract {
  /**
   *
   * @type {Array<ArtistForApiContract>}
   * @memberof PartialFindResultArtistForApiContract
   */
  items?: Array<ArtistForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultArtistForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultArtistForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultArtistForUserForApiContract
 */
export interface PartialFindResultArtistForUserForApiContract {
  /**
   *
   * @type {Array<ArtistForUserForApiContract>}
   * @memberof PartialFindResultArtistForUserForApiContract
   */
  items?: Array<ArtistForUserForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultArtistForUserForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultArtistForUserForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultCommentForApiContract
 */
export interface PartialFindResultCommentForApiContract {
  /**
   *
   * @type {Array<CommentForApiContract>}
   * @memberof PartialFindResultCommentForApiContract
   */
  items?: Array<CommentForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultCommentForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultCommentForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultDiscussionTopicContract
 */
export interface PartialFindResultDiscussionTopicContract {
  /**
   *
   * @type {Array<DiscussionTopicContract>}
   * @memberof PartialFindResultDiscussionTopicContract
   */
  items?: Array<DiscussionTopicContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultDiscussionTopicContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultDiscussionTopicContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultEntryForApiContract
 */
export interface PartialFindResultEntryForApiContract {
  /**
   *
   * @type {Array<EntryForApiContract>}
   * @memberof PartialFindResultEntryForApiContract
   */
  items?: Array<EntryForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultEntryForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultEntryForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultPVForSongContract
 */
export interface PartialFindResultPVForSongContract {
  /**
   *
   * @type {Array<PVForSongContract>}
   * @memberof PartialFindResultPVForSongContract
   */
  items?: Array<PVForSongContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultPVForSongContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultPVForSongContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultRatedSongForUserForApiContract
 */
export interface PartialFindResultRatedSongForUserForApiContract {
  /**
   *
   * @type {Array<RatedSongForUserForApiContract>}
   * @memberof PartialFindResultRatedSongForUserForApiContract
   */
  items?: Array<RatedSongForUserForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultRatedSongForUserForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultRatedSongForUserForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultReleaseEventForApiContract
 */
export interface PartialFindResultReleaseEventForApiContract {
  /**
   *
   * @type {Array<ReleaseEventForApiContract>}
   * @memberof PartialFindResultReleaseEventForApiContract
   */
  items?: Array<ReleaseEventForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultReleaseEventForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultReleaseEventForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultReleaseEventSeriesForApiContract
 */
export interface PartialFindResultReleaseEventSeriesForApiContract {
  /**
   *
   * @type {Array<ReleaseEventSeriesForApiContract>}
   * @memberof PartialFindResultReleaseEventSeriesForApiContract
   */
  items?: Array<ReleaseEventSeriesForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultReleaseEventSeriesForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultReleaseEventSeriesForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultSongForApiContract
 */
export interface PartialFindResultSongForApiContract {
  /**
   *
   * @type {Array<SongForApiContract>}
   * @memberof PartialFindResultSongForApiContract
   */
  items?: Array<SongForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultSongForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultSongForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultSongInListForApiContract
 */
export interface PartialFindResultSongInListForApiContract {
  /**
   *
   * @type {Array<SongInListForApiContract>}
   * @memberof PartialFindResultSongInListForApiContract
   */
  items?: Array<SongInListForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultSongInListForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultSongInListForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultSongListForApiContract
 */
export interface PartialFindResultSongListForApiContract {
  /**
   *
   * @type {Array<SongListForApiContract>}
   * @memberof PartialFindResultSongListForApiContract
   */
  items?: Array<SongListForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultSongListForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultSongListForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultTagForApiContract
 */
export interface PartialFindResultTagForApiContract {
  /**
   *
   * @type {Array<TagForApiContract>}
   * @memberof PartialFindResultTagForApiContract
   */
  items?: Array<TagForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultTagForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultTagForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultUserForApiContract
 */
export interface PartialFindResultUserForApiContract {
  /**
   *
   * @type {Array<UserForApiContract>}
   * @memberof PartialFindResultUserForApiContract
   */
  items?: Array<UserForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultUserForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultUserForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultUserMessageContract
 */
export interface PartialFindResultUserMessageContract {
  /**
   *
   * @type {Array<UserMessageContract>}
   * @memberof PartialFindResultUserMessageContract
   */
  items?: Array<UserMessageContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultUserMessageContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultUserMessageContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface PartialFindResultVenueForApiContract
 */
export interface PartialFindResultVenueForApiContract {
  /**
   *
   * @type {Array<VenueForApiContract>}
   * @memberof PartialFindResultVenueForApiContract
   */
  items?: Array<VenueForApiContract>;
  /**
   *
   * @type {string}
   * @memberof PartialFindResultVenueForApiContract
   */
  term?: string;
  /**
   *
   * @type {number}
   * @memberof PartialFindResultVenueForApiContract
   */
  totalCount?: number;
}

/**
 *
 * @export
 * @interface RatedSongForUserForApiContract
 */
export interface RatedSongForUserForApiContract {
  /**
   *
   * @type {Date}
   * @memberof RatedSongForUserForApiContract
   */
  date?: Date;
  /**
   *
   * @type {SongForApiContract}
   * @memberof RatedSongForUserForApiContract
   */
  song?: SongForApiContract;
  /**
   *
   * @type {UserForApiContract}
   * @memberof RatedSongForUserForApiContract
   */
  user?: UserForApiContract;
  /**
   *
   * @type {string}
   * @memberof RatedSongForUserForApiContract
   */
  rating?: RatedSongForUserForApiContract.RatingEnum;
}

/**
 * @export
 * @namespace RatedSongForUserForApiContract
 */
export namespace RatedSongForUserForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum RatingEnum {
    Nothing = <any>'Nothing',
    Dislike = <any>'Dislike',
    Like = <any>'Like',
    Favorite = <any>'Favorite',
  }
}

/**
 *
 * @export
 * @interface RelatedSongsContract
 */
export interface RelatedSongsContract {
  /**
   *
   * @type {Array<SongForApiContract>}
   * @memberof RelatedSongsContract
   */
  artistMatches?: Array<SongForApiContract>;
  /**
   *
   * @type {Array<SongForApiContract>}
   * @memberof RelatedSongsContract
   */
  likeMatches?: Array<SongForApiContract>;
  /**
   *
   * @type {Array<SongForApiContract>}
   * @memberof RelatedSongsContract
   */
  tagMatches?: Array<SongForApiContract>;
}

/**
 *
 * @export
 * @interface ReleaseEventContract
 */
export interface ReleaseEventContract {
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  category?: ReleaseEventContract.CategoryEnum;
  /**
   *
   * @type {boolean}
   * @memberof ReleaseEventContract
   */
  customName?: boolean;
  /**
   *
   * @type {Date}
   * @memberof ReleaseEventContract
   */
  date?: Date;
  /**
   *
   * @type {boolean}
   * @memberof ReleaseEventContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  description?: string;
  /**
   *
   * @type {Date}
   * @memberof ReleaseEventContract
   */
  endDate?: Date;
  /**
   *
   * @type {boolean}
   * @memberof ReleaseEventContract
   */
  hasVenueOrVenueName?: boolean;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  inheritedCategory?: ReleaseEventContract.InheritedCategoryEnum;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  pictureMime?: string;
  /**
   *
   * @type {ReleaseEventSeriesContract}
   * @memberof ReleaseEventContract
   */
  series?: ReleaseEventSeriesContract;
  /**
   *
   * @type {SongListBaseContract}
   * @memberof ReleaseEventContract
   */
  songList?: SongListBaseContract;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  status?: ReleaseEventContract.StatusEnum;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  urlSlug?: string;
  /**
   *
   * @type {VenueContract}
   * @memberof ReleaseEventContract
   */
  venue?: VenueContract;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventContract
   */
  venueName?: string;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventContract
   */
  version?: number;
}

/**
 * @export
 * @namespace ReleaseEventContract
 */
export namespace ReleaseEventContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoryEnum {
    Unspecified = <any>'Unspecified',
    AlbumRelease = <any>'AlbumRelease',
    Anniversary = <any>'Anniversary',
    Club = <any>'Club',
    Concert = <any>'Concert',
    Contest = <any>'Contest',
    Convention = <any>'Convention',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum InheritedCategoryEnum {
    Unspecified = <any>'Unspecified',
    AlbumRelease = <any>'AlbumRelease',
    Anniversary = <any>'Anniversary',
    Club = <any>'Club',
    Concert = <any>'Concert',
    Contest = <any>'Contest',
    Convention = <any>'Convention',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface ReleaseEventForApiContract
 */
export interface ReleaseEventForApiContract {
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {Array<ArtistForEventContract>}
   * @memberof ReleaseEventForApiContract
   */
  artists?: Array<ArtistForEventContract>;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  category?: ReleaseEventForApiContract.CategoryEnum;
  /**
   *
   * @type {Date}
   * @memberof ReleaseEventForApiContract
   */
  date?: Date;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  description?: string;
  /**
   *
   * @type {Date}
   * @memberof ReleaseEventForApiContract
   */
  endDate?: Date;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventForApiContract
   */
  id?: number;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof ReleaseEventForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringContract>}
   * @memberof ReleaseEventForApiContract
   */
  names?: Array<LocalizedStringContract>;
  /**
   *
   * @type {ReleaseEventSeriesContract}
   * @memberof ReleaseEventForApiContract
   */
  series?: ReleaseEventSeriesContract;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventForApiContract
   */
  seriesId?: number;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventForApiContract
   */
  seriesNumber?: number;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  seriesSuffix?: string;
  /**
   *
   * @type {SongListBaseContract}
   * @memberof ReleaseEventForApiContract
   */
  songList?: SongListBaseContract;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  status?: ReleaseEventForApiContract.StatusEnum;
  /**
   *
   * @type {Array<TagUsageForApiContract>}
   * @memberof ReleaseEventForApiContract
   */
  tags?: Array<TagUsageForApiContract>;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  urlSlug?: string;
  /**
   *
   * @type {VenueForApiContract}
   * @memberof ReleaseEventForApiContract
   */
  venue?: VenueForApiContract;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventForApiContract
   */
  venueName?: string;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkForApiContract>}
   * @memberof ReleaseEventForApiContract
   */
  webLinks?: Array<WebLinkForApiContract>;
}

/**
 * @export
 * @namespace ReleaseEventForApiContract
 */
export namespace ReleaseEventForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoryEnum {
    Unspecified = <any>'Unspecified',
    AlbumRelease = <any>'AlbumRelease',
    Anniversary = <any>'Anniversary',
    Club = <any>'Club',
    Concert = <any>'Concert',
    Contest = <any>'Contest',
    Convention = <any>'Convention',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface ReleaseEventSeriesContract
 */
export interface ReleaseEventSeriesContract {
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesContract
   */
  category?: ReleaseEventSeriesContract.CategoryEnum;
  /**
   *
   * @type {boolean}
   * @memberof ReleaseEventSeriesContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesContract
   */
  description?: string;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventSeriesContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesContract
   */
  pictureMime?: string;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesContract
   */
  status?: ReleaseEventSeriesContract.StatusEnum;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesContract
   */
  urlSlug?: string;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventSeriesContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkContract>}
   * @memberof ReleaseEventSeriesContract
   */
  webLinks?: Array<WebLinkContract>;
}

/**
 * @export
 * @namespace ReleaseEventSeriesContract
 */
export namespace ReleaseEventSeriesContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoryEnum {
    Unspecified = <any>'Unspecified',
    AlbumRelease = <any>'AlbumRelease',
    Anniversary = <any>'Anniversary',
    Club = <any>'Club',
    Concert = <any>'Concert',
    Contest = <any>'Contest',
    Convention = <any>'Convention',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface ReleaseEventSeriesForApiContract
 */
export interface ReleaseEventSeriesForApiContract {
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesForApiContract
   */
  category?: ReleaseEventSeriesForApiContract.CategoryEnum;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesForApiContract
   */
  description?: string;
  /**
   *
   * @type {Array<ReleaseEventForApiContract>}
   * @memberof ReleaseEventSeriesForApiContract
   */
  events?: Array<ReleaseEventForApiContract>;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventSeriesForApiContract
   */
  id?: number;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof ReleaseEventSeriesForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringContract>}
   * @memberof ReleaseEventSeriesForApiContract
   */
  names?: Array<LocalizedStringContract>;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesForApiContract
   */
  status?: ReleaseEventSeriesForApiContract.StatusEnum;
  /**
   *
   * @type {string}
   * @memberof ReleaseEventSeriesForApiContract
   */
  urlSlug?: string;
  /**
   *
   * @type {number}
   * @memberof ReleaseEventSeriesForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkForApiContract>}
   * @memberof ReleaseEventSeriesForApiContract
   */
  webLinks?: Array<WebLinkForApiContract>;
}

/**
 * @export
 * @namespace ReleaseEventSeriesForApiContract
 */
export namespace ReleaseEventSeriesForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoryEnum {
    Unspecified = <any>'Unspecified',
    AlbumRelease = <any>'AlbumRelease',
    Anniversary = <any>'Anniversary',
    Club = <any>'Club',
    Concert = <any>'Concert',
    Contest = <any>'Contest',
    Convention = <any>'Convention',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface SongContract
 */
export interface SongContract {
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  artistString?: string;
  /**
   *
   * @type {Date}
   * @memberof SongContract
   */
  createDate?: Date;
  /**
   *
   * @type {boolean}
   * @memberof SongContract
   */
  deleted?: boolean;
  /**
   *
   * @type {number}
   * @memberof SongContract
   */
  favoritedTimes?: number;
  /**
   *
   * @type {number}
   * @memberof SongContract
   */
  id?: number;
  /**
   *
   * @type {number}
   * @memberof SongContract
   */
  lengthSeconds?: number;
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  nicoId?: string;
  /**
   *
   * @type {Date}
   * @memberof SongContract
   */
  publishDate?: Date;
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  pvServices?: SongContract.PvServicesEnum;
  /**
   *
   * @type {number}
   * @memberof SongContract
   */
  ratingScore?: number;
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  songType?: SongContract.SongTypeEnum;
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  status?: SongContract.StatusEnum;
  /**
   *
   * @type {string}
   * @memberof SongContract
   */
  thumbUrl?: string;
  /**
   *
   * @type {number}
   * @memberof SongContract
   */
  version?: number;
}

/**
 * @export
 * @namespace SongContract
 */
export namespace SongContract {
  /**
   * @export
   * @enum {string}
   */
  export enum PvServicesEnum {
    Nothing = <any>'Nothing',
    NicoNicoDouga = <any>'NicoNicoDouga',
    Youtube = <any>'Youtube',
    SoundCloud = <any>'SoundCloud',
    Vimeo = <any>'Vimeo',
    Piapro = <any>'Piapro',
    Bilibili = <any>'Bilibili',
    File = <any>'File',
    LocalFile = <any>'LocalFile',
    Creofuga = <any>'Creofuga',
    Bandcamp = <any>'Bandcamp',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum SongTypeEnum {
    Unspecified = <any>'Unspecified',
    Original = <any>'Original',
    Remaster = <any>'Remaster',
    Remix = <any>'Remix',
    Cover = <any>'Cover',
    Arrangement = <any>'Arrangement',
    Instrumental = <any>'Instrumental',
    Mashup = <any>'Mashup',
    MusicPV = <any>'MusicPV',
    DramaPV = <any>'DramaPV',
    Live = <any>'Live',
    Illustration = <any>'Illustration',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface SongForApiContract
 */
export interface SongForApiContract {
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {Array<AlbumContract>}
   * @memberof SongForApiContract
   */
  albums?: Array<AlbumContract>;
  /**
   *
   * @type {Array<ArtistForSongContract>}
   * @memberof SongForApiContract
   */
  artists?: Array<ArtistForSongContract>;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  artistString?: string;
  /**
   *
   * @type {Date}
   * @memberof SongForApiContract
   */
  createDate?: Date;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  defaultName?: string;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  defaultNameLanguage?: SongForApiContract.DefaultNameLanguageEnum;
  /**
   *
   * @type {boolean}
   * @memberof SongForApiContract
   */
  deleted?: boolean;
  /**
   *
   * @type {number}
   * @memberof SongForApiContract
   */
  favoritedTimes?: number;
  /**
   *
   * @type {number}
   * @memberof SongForApiContract
   */
  id?: number;
  /**
   *
   * @type {number}
   * @memberof SongForApiContract
   */
  lengthSeconds?: number;
  /**
   *
   * @type {Array<LyricsForSongContract>}
   * @memberof SongForApiContract
   */
  lyrics?: Array<LyricsForSongContract>;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof SongForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {number}
   * @memberof SongForApiContract
   */
  mergedTo?: number;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringContract>}
   * @memberof SongForApiContract
   */
  names?: Array<LocalizedStringContract>;
  /**
   *
   * @type {number}
   * @memberof SongForApiContract
   */
  originalVersionId?: number;
  /**
   *
   * @type {Date}
   * @memberof SongForApiContract
   */
  publishDate?: Date;
  /**
   *
   * @type {Array<PVContract>}
   * @memberof SongForApiContract
   */
  pvs?: Array<PVContract>;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  pvServices?: SongForApiContract.PvServicesEnum;
  /**
   *
   * @type {number}
   * @memberof SongForApiContract
   */
  ratingScore?: number;
  /**
   *
   * @type {ReleaseEventForApiContract}
   * @memberof SongForApiContract
   */
  releaseEvent?: ReleaseEventForApiContract;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  songType?: SongForApiContract.SongTypeEnum;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  status?: SongForApiContract.StatusEnum;
  /**
   *
   * @type {Array<TagUsageForApiContract>}
   * @memberof SongForApiContract
   */
  tags?: Array<TagUsageForApiContract>;
  /**
   *
   * @type {string}
   * @memberof SongForApiContract
   */
  thumbUrl?: string;
  /**
   *
   * @type {number}
   * @memberof SongForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkForApiContract>}
   * @memberof SongForApiContract
   */
  webLinks?: Array<WebLinkForApiContract>;
}

/**
 * @export
 * @namespace SongForApiContract
 */
export namespace SongForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum DefaultNameLanguageEnum {
    Unspecified = <any>'Unspecified',
    Japanese = <any>'Japanese',
    Romaji = <any>'Romaji',
    English = <any>'English',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum PvServicesEnum {
    Nothing = <any>'Nothing',
    NicoNicoDouga = <any>'NicoNicoDouga',
    Youtube = <any>'Youtube',
    SoundCloud = <any>'SoundCloud',
    Vimeo = <any>'Vimeo',
    Piapro = <any>'Piapro',
    Bilibili = <any>'Bilibili',
    File = <any>'File',
    LocalFile = <any>'LocalFile',
    Creofuga = <any>'Creofuga',
    Bandcamp = <any>'Bandcamp',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum SongTypeEnum {
    Unspecified = <any>'Unspecified',
    Original = <any>'Original',
    Remaster = <any>'Remaster',
    Remix = <any>'Remix',
    Cover = <any>'Cover',
    Arrangement = <any>'Arrangement',
    Instrumental = <any>'Instrumental',
    Mashup = <any>'Mashup',
    MusicPV = <any>'MusicPV',
    DramaPV = <any>'DramaPV',
    Live = <any>'Live',
    Illustration = <any>'Illustration',
    Other = <any>'Other',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface SongInAlbumForApiContract
 */
export interface SongInAlbumForApiContract {
  /**
   *
   * @type {number}
   * @memberof SongInAlbumForApiContract
   */
  discNumber?: number;
  /**
   *
   * @type {number}
   * @memberof SongInAlbumForApiContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof SongInAlbumForApiContract
   */
  name?: string;
  /**
   *
   * @type {SongForApiContract}
   * @memberof SongInAlbumForApiContract
   */
  song?: SongForApiContract;
  /**
   *
   * @type {number}
   * @memberof SongInAlbumForApiContract
   */
  trackNumber?: number;
}

/**
 *
 * @export
 * @interface SongInListEditContract
 */
export interface SongInListEditContract {
  /**
   *
   * @type {number}
   * @memberof SongInListEditContract
   */
  songInListId?: number;
  /**
   *
   * @type {string}
   * @memberof SongInListEditContract
   */
  notes?: string;
  /**
   *
   * @type {number}
   * @memberof SongInListEditContract
   */
  order?: number;
  /**
   *
   * @type {SongForApiContract}
   * @memberof SongInListEditContract
   */
  song?: SongForApiContract;
}

/**
 *
 * @export
 * @interface SongInListForApiContract
 */
export interface SongInListForApiContract {
  /**
   *
   * @type {string}
   * @memberof SongInListForApiContract
   */
  notes?: string;
  /**
   *
   * @type {number}
   * @memberof SongInListForApiContract
   */
  order?: number;
  /**
   *
   * @type {SongForApiContract}
   * @memberof SongInListForApiContract
   */
  song?: SongForApiContract;
}

/**
 *
 * @export
 * @interface SongListBaseContract
 */
export interface SongListBaseContract {
  /**
   *
   * @type {string}
   * @memberof SongListBaseContract
   */
  featuredCategory?: SongListBaseContract.FeaturedCategoryEnum;
  /**
   *
   * @type {number}
   * @memberof SongListBaseContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof SongListBaseContract
   */
  name?: string;
}

/**
 * @export
 * @namespace SongListBaseContract
 */
export namespace SongListBaseContract {
  /**
   * @export
   * @enum {string}
   */
  export enum FeaturedCategoryEnum {
    Nothing = <any>'Nothing',
    Concerts = <any>'Concerts',
    VocaloidRanking = <any>'VocaloidRanking',
    Pools = <any>'Pools',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface SongListForApiContract
 */
export interface SongListForApiContract {
  /**
   *
   * @type {UserForApiContract}
   * @memberof SongListForApiContract
   */
  author?: UserForApiContract;
  /**
   *
   * @type {boolean}
   * @memberof SongListForApiContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof SongListForApiContract
   */
  description?: string;
  /**
   *
   * @type {Date}
   * @memberof SongListForApiContract
   */
  eventDate?: Date;
  /**
   *
   * @type {Array<ReleaseEventForApiContract>}
   * @memberof SongListForApiContract
   */
  events?: Array<ReleaseEventForApiContract>;
  /**
   *
   * @type {Array<CommentForApiContract>}
   * @memberof SongListForApiContract
   */
  latestComments?: Array<CommentForApiContract>;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof SongListForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {string}
   * @memberof SongListForApiContract
   */
  status?: SongListForApiContract.StatusEnum;
  /**
   *
   * @type {Array<TagUsageForApiContract>}
   * @memberof SongListForApiContract
   */
  tags?: Array<TagUsageForApiContract>;
  /**
   *
   * @type {string}
   * @memberof SongListForApiContract
   */
  featuredCategory?: SongListForApiContract.FeaturedCategoryEnum;
  /**
   *
   * @type {number}
   * @memberof SongListForApiContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof SongListForApiContract
   */
  name?: string;
}

/**
 * @export
 * @namespace SongListForApiContract
 */
export namespace SongListForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum FeaturedCategoryEnum {
    Nothing = <any>'Nothing',
    Concerts = <any>'Concerts',
    VocaloidRanking = <any>'VocaloidRanking',
    Pools = <any>'Pools',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface SongListForEditContract
 */
export interface SongListForEditContract {
  /**
   *
   * @type {Array<SongInListEditContract>}
   * @memberof SongListForEditContract
   */
  songLinks?: Array<SongInListEditContract>;
  /**
   *
   * @type {string}
   * @memberof SongListForEditContract
   */
  updateNotes?: string;
  /**
   *
   * @type {UserWithEmailContract}
   * @memberof SongListForEditContract
   */
  author?: UserWithEmailContract;
  /**
   *
   * @type {boolean}
   * @memberof SongListForEditContract
   */
  canEdit?: boolean;
  /**
   *
   * @type {boolean}
   * @memberof SongListForEditContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof SongListForEditContract
   */
  description?: string;
  /**
   *
   * @type {Date}
   * @memberof SongListForEditContract
   */
  eventDate?: Date;
  /**
   *
   * @type {string}
   * @memberof SongListForEditContract
   */
  status?: SongListForEditContract.StatusEnum;
  /**
   *
   * @type {EntryThumbContract}
   * @memberof SongListForEditContract
   */
  thumb?: EntryThumbContract;
  /**
   *
   * @type {number}
   * @memberof SongListForEditContract
   */
  version?: number;
  /**
   *
   * @type {string}
   * @memberof SongListForEditContract
   */
  featuredCategory?: SongListForEditContract.FeaturedCategoryEnum;
  /**
   *
   * @type {number}
   * @memberof SongListForEditContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof SongListForEditContract
   */
  name?: string;
}

/**
 * @export
 * @namespace SongListForEditContract
 */
export namespace SongListForEditContract {
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum FeaturedCategoryEnum {
    Nothing = <any>'Nothing',
    Concerts = <any>'Concerts',
    VocaloidRanking = <any>'VocaloidRanking',
    Pools = <any>'Pools',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface SongRatingContract
 */
export interface SongRatingContract {
  /**
   *
   * @type {string}
   * @memberof SongRatingContract
   */
  rating?: SongRatingContract.RatingEnum;
}

/**
 * @export
 * @namespace SongRatingContract
 */
export namespace SongRatingContract {
  /**
   * @export
   * @enum {string}
   */
  export enum RatingEnum {
    Nothing = <any>'Nothing',
    Dislike = <any>'Dislike',
    Like = <any>'Like',
    Favorite = <any>'Favorite',
  }
}

/**
 *
 * @export
 * @interface TagBaseContract
 */
export interface TagBaseContract {
  /**
   *
   * @type {string}
   * @memberof TagBaseContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof TagBaseContract
   */
  categoryName?: string;
  /**
   *
   * @type {number}
   * @memberof TagBaseContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof TagBaseContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof TagBaseContract
   */
  urlSlug?: string;
}

/**
 *
 * @export
 * @interface TagForApiContract
 */
export interface TagForApiContract {
  /**
   *
   * @type {string}
   * @memberof TagForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {TagBaseContract}
   * @memberof TagForApiContract
   */
  aliasedTo?: TagBaseContract;
  /**
   *
   * @type {string}
   * @memberof TagForApiContract
   */
  categoryName?: string;
  /**
   *
   * @type {Date}
   * @memberof TagForApiContract
   */
  createDate?: Date;
  /**
   *
   * @type {string}
   * @memberof TagForApiContract
   */
  defaultNameLanguage?: TagForApiContract.DefaultNameLanguageEnum;
  /**
   *
   * @type {string}
   * @memberof TagForApiContract
   */
  description?: string;
  /**
   *
   * @type {number}
   * @memberof TagForApiContract
   */
  id?: number;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof TagForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {string}
   * @memberof TagForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringWithIdContract>}
   * @memberof TagForApiContract
   */
  names?: Array<LocalizedStringWithIdContract>;
  /**
   *
   * @type {TagBaseContract}
   * @memberof TagForApiContract
   */
  parent?: TagBaseContract;
  /**
   *
   * @type {Array<TagBaseContract>}
   * @memberof TagForApiContract
   */
  relatedTags?: Array<TagBaseContract>;
  /**
   *
   * @type {string}
   * @memberof TagForApiContract
   */
  status?: TagForApiContract.StatusEnum;
  /**
   *
   * @type {number}
   * @memberof TagForApiContract
   */
  targets?: number;
  /**
   *
   * @type {EnglishTranslatedStringContract}
   * @memberof TagForApiContract
   */
  translatedDescription?: EnglishTranslatedStringContract;
  /**
   *
   * @type {string}
   * @memberof TagForApiContract
   */
  urlSlug?: string;
  /**
   *
   * @type {number}
   * @memberof TagForApiContract
   */
  usageCount?: number;
  /**
   *
   * @type {number}
   * @memberof TagForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkForApiContract>}
   * @memberof TagForApiContract
   */
  webLinks?: Array<WebLinkForApiContract>;
}

/**
 * @export
 * @namespace TagForApiContract
 */
export namespace TagForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum DefaultNameLanguageEnum {
    Unspecified = <any>'Unspecified',
    Japanese = <any>'Japanese',
    Romaji = <any>'Romaji',
    English = <any>'English',
  }
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface TagUsageForApiContract
 */
export interface TagUsageForApiContract {
  /**
   *
   * @type {number}
   * @memberof TagUsageForApiContract
   */
  count?: number;
  /**
   *
   * @type {TagBaseContract}
   * @memberof TagUsageForApiContract
   */
  tag?: TagBaseContract;
}

/**
 *
 * @export
 * @interface UserForApiContract
 */
export interface UserForApiContract {
  /**
   *
   * @type {boolean}
   * @memberof UserForApiContract
   */
  active?: boolean;
  /**
   *
   * @type {string}
   * @memberof UserForApiContract
   */
  groupId?: UserForApiContract.GroupIdEnum;
  /**
   *
   * @type {Array<UserKnownLanguageContract>}
   * @memberof UserForApiContract
   */
  knownLanguages?: Array<UserKnownLanguageContract>;
  /**
   *
   * @type {EntryThumbForApiContract}
   * @memberof UserForApiContract
   */
  mainPicture?: EntryThumbForApiContract;
  /**
   *
   * @type {Date}
   * @memberof UserForApiContract
   */
  memberSince?: Date;
  /**
   *
   * @type {Array<OldUsernameContract>}
   * @memberof UserForApiContract
   */
  oldUsernames?: Array<OldUsernameContract>;
  /**
   *
   * @type {boolean}
   * @memberof UserForApiContract
   */
  verifiedArtist?: boolean;
  /**
   *
   * @type {number}
   * @memberof UserForApiContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof UserForApiContract
   */
  name?: string;
}

/**
 * @export
 * @namespace UserForApiContract
 */
export namespace UserForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum GroupIdEnum {
    Nothing = <any>'Nothing',
    Limited = <any>'Limited',
    Regular = <any>'Regular',
    Trusted = <any>'Trusted',
    Moderator = <any>'Moderator',
    Admin = <any>'Admin',
  }
}

/**
 *
 * @export
 * @interface UserKnownLanguageContract
 */
export interface UserKnownLanguageContract {
  /**
   *
   * @type {string}
   * @memberof UserKnownLanguageContract
   */
  cultureCode?: string;
  /**
   *
   * @type {string}
   * @memberof UserKnownLanguageContract
   */
  proficiency?: UserKnownLanguageContract.ProficiencyEnum;
}

/**
 * @export
 * @namespace UserKnownLanguageContract
 */
export namespace UserKnownLanguageContract {
  /**
   * @export
   * @enum {string}
   */
  export enum ProficiencyEnum {
    Nothing = <any>'Nothing',
    Basics = <any>'Basics',
    Intermediate = <any>'Intermediate',
    Advanced = <any>'Advanced',
    Native = <any>'Native',
  }
}

/**
 *
 * @export
 * @interface UserMessageContract
 */
export interface UserMessageContract {
  /**
   *
   * @type {string}
   * @memberof UserMessageContract
   */
  body?: string;
  /**
   *
   * @type {string}
   * @memberof UserMessageContract
   */
  createdFormatted?: string;
  /**
   *
   * @type {boolean}
   * @memberof UserMessageContract
   */
  highPriority?: boolean;
  /**
   *
   * @type {number}
   * @memberof UserMessageContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof UserMessageContract
   */
  inbox?: UserMessageContract.InboxEnum;
  /**
   *
   * @type {boolean}
   * @memberof UserMessageContract
   */
  read?: boolean;
  /**
   *
   * @type {UserForApiContract}
   * @memberof UserMessageContract
   */
  receiver?: UserForApiContract;
  /**
   *
   * @type {UserForApiContract}
   * @memberof UserMessageContract
   */
  sender?: UserForApiContract;
  /**
   *
   * @type {string}
   * @memberof UserMessageContract
   */
  subject?: string;
}

/**
 * @export
 * @namespace UserMessageContract
 */
export namespace UserMessageContract {
  /**
   * @export
   * @enum {string}
   */
  export enum InboxEnum {
    Nothing = <any>'Nothing',
    Received = <any>'Received',
    Sent = <any>'Sent',
    Notifications = <any>'Notifications',
  }
}

/**
 *
 * @export
 * @interface UserWithEmailContract
 */
export interface UserWithEmailContract {
  /**
   *
   * @type {string}
   * @memberof UserWithEmailContract
   */
  email?: string;
  /**
   *
   * @type {number}
   * @memberof UserWithEmailContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof UserWithEmailContract
   */
  name?: string;
}

/**
 *
 * @export
 * @interface VenueContract
 */
export interface VenueContract {
  /**
   *
   * @type {string}
   * @memberof VenueContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof VenueContract
   */
  address?: string;
  /**
   *
   * @type {string}
   * @memberof VenueContract
   */
  addressCountryCode?: string;
  /**
   *
   * @type {OptionalGeoPointContract}
   * @memberof VenueContract
   */
  coordinates?: OptionalGeoPointContract;
  /**
   *
   * @type {boolean}
   * @memberof VenueContract
   */
  deleted?: boolean;
  /**
   *
   * @type {string}
   * @memberof VenueContract
   */
  description?: string;
  /**
   *
   * @type {number}
   * @memberof VenueContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof VenueContract
   */
  name?: string;
  /**
   *
   * @type {string}
   * @memberof VenueContract
   */
  status?: VenueContract.StatusEnum;
  /**
   *
   * @type {number}
   * @memberof VenueContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkContract>}
   * @memberof VenueContract
   */
  webLinks?: Array<WebLinkContract>;
}

/**
 * @export
 * @namespace VenueContract
 */
export namespace VenueContract {
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface VenueForApiContract
 */
export interface VenueForApiContract {
  /**
   *
   * @type {string}
   * @memberof VenueForApiContract
   */
  additionalNames?: string;
  /**
   *
   * @type {string}
   * @memberof VenueForApiContract
   */
  address?: string;
  /**
   *
   * @type {string}
   * @memberof VenueForApiContract
   */
  addressCountryCode?: string;
  /**
   *
   * @type {OptionalGeoPointContract}
   * @memberof VenueForApiContract
   */
  coordinates?: OptionalGeoPointContract;
  /**
   *
   * @type {string}
   * @memberof VenueForApiContract
   */
  description?: string;
  /**
   *
   * @type {Array<ReleaseEventContract>}
   * @memberof VenueForApiContract
   */
  events?: Array<ReleaseEventContract>;
  /**
   *
   * @type {number}
   * @memberof VenueForApiContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof VenueForApiContract
   */
  name?: string;
  /**
   *
   * @type {Array<LocalizedStringContract>}
   * @memberof VenueForApiContract
   */
  names?: Array<LocalizedStringContract>;
  /**
   *
   * @type {string}
   * @memberof VenueForApiContract
   */
  status?: VenueForApiContract.StatusEnum;
  /**
   *
   * @type {number}
   * @memberof VenueForApiContract
   */
  version?: number;
  /**
   *
   * @type {Array<WebLinkForApiContract>}
   * @memberof VenueForApiContract
   */
  webLinks?: Array<WebLinkForApiContract>;
}

/**
 * @export
 * @namespace VenueForApiContract
 */
export namespace VenueForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum StatusEnum {
    Draft = <any>'Draft',
    Finished = <any>'Finished',
    Approved = <any>'Approved',
    Locked = <any>'Locked',
  }
}

/**
 *
 * @export
 * @interface WebLinkContract
 */
export interface WebLinkContract {
  /**
   *
   * @type {string}
   * @memberof WebLinkContract
   */
  category?: WebLinkContract.CategoryEnum;
  /**
   *
   * @type {string}
   * @memberof WebLinkContract
   */
  description?: string;
  /**
   *
   * @type {string}
   * @memberof WebLinkContract
   */
  descriptionOrUrl?: string;
  /**
   *
   * @type {number}
   * @memberof WebLinkContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof WebLinkContract
   */
  url?: string;
}

/**
 * @export
 * @namespace WebLinkContract
 */
export namespace WebLinkContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoryEnum {
    Official = <any>'Official',
    Commercial = <any>'Commercial',
    Reference = <any>'Reference',
    Other = <any>'Other',
  }
}

/**
 *
 * @export
 * @interface WebLinkForApiContract
 */
export interface WebLinkForApiContract {
  /**
   *
   * @type {string}
   * @memberof WebLinkForApiContract
   */
  category?: WebLinkForApiContract.CategoryEnum;
  /**
   *
   * @type {string}
   * @memberof WebLinkForApiContract
   */
  description?: string;
  /**
   *
   * @type {string}
   * @memberof WebLinkForApiContract
   */
  descriptionOrUrl?: string;
  /**
   *
   * @type {number}
   * @memberof WebLinkForApiContract
   */
  id?: number;
  /**
   *
   * @type {string}
   * @memberof WebLinkForApiContract
   */
  url?: string;
}

/**
 * @export
 * @namespace WebLinkForApiContract
 */
export namespace WebLinkForApiContract {
  /**
   * @export
   * @enum {string}
   */
  export enum CategoryEnum {
    Official = <any>'Official',
    Commercial = <any>'Commercial',
    Reference = <any>'Reference',
    Other = <any>'Other',
  }
}

export interface ActivityEntryApiGetListParams {
  before?: Date;
  since?: Date;
  userId?: number;
  editEvent?: 'Created' | 'Updated' | 'Deleted' | 'Restored';
  maxResults?: number;
  getTotalCount?: boolean;
  fields?: 'None' | 'ArchivedVersion' | 'Entry';
  entryFields?:
    | 'None'
    | 'AdditionalNames'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'Tags'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

/**
 * ActivityEntryApiApi - fetch parameter creator
 * @export
 */
export const ActivityEntryApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     * Entries are always returned sorted from newest to oldest.              Activity for deleted entries is not returned.
     * @summary Gets a list of recent activity entries.
     * @param {ActivityEntryApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    activityEntryApiGetList(
      params: ActivityEntryApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        before,
        since,
        userId,
        editEvent,
        maxResults,
        getTotalCount,
        fields,
        entryFields,
        lang,
      } = params;
      const localVarPath = `/api/activityEntries`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (before !== undefined) {
        localVarQueryParameter['before'] = (before as any).toISOString();
      }

      if (since !== undefined) {
        localVarQueryParameter['since'] = (since as any).toISOString();
      }

      if (userId !== undefined) {
        localVarQueryParameter['userId'] = userId;
      }

      if (editEvent !== undefined) {
        localVarQueryParameter['editEvent'] = editEvent;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (entryFields !== undefined) {
        localVarQueryParameter['entryFields'] = entryFields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * ActivityEntryApiApi - functional programming interface
 * @export
 */
export const ActivityEntryApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     * Entries are always returned sorted from newest to oldest.              Activity for deleted entries is not returned.
     * @summary Gets a list of recent activity entries.
     * @param {ActivityEntryApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    activityEntryApiGetList(
      params: ActivityEntryApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultActivityEntryForApiContract> {
      const localVarFetchArgs = ActivityEntryApiApiFetchParamCreator(
        configuration,
      ).activityEntryApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * ActivityEntryApiApi - factory interface
 * @export
 */
export const ActivityEntryApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     * Entries are always returned sorted from newest to oldest.              Activity for deleted entries is not returned.
     * @summary Gets a list of recent activity entries.
     * @param {ActivityEntryApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    activityEntryApiGetList(
      params: ActivityEntryApiGetListParams = {},
      options?: any,
    ) {
      return ActivityEntryApiApiFp(configuration).activityEntryApiGetList(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useActivityEntryApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => ActivityEntryApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface AlbumApiDeleteParams {
  id: number;
  notes?: string;
}

export interface AlbumApiDeleteCommentParams {
  commentId: number;
}

export interface AlbumApiDeleteReviewParams {
  reviewId: number;
  id: string;
}

export interface AlbumApiGetCommentsParams {
  id: number;
}

export interface AlbumApiGetListParams {
  query?: string;
  discTypes?:
    | 'Unknown'
    | 'Album'
    | 'Single'
    | 'EP'
    | 'SplitAlbum'
    | 'Compilation'
    | 'Video'
    | 'Artbook'
    | 'Game'
    | 'Fanmade'
    | 'Instrumental'
    | 'Other';
  tagName?: Array<string>;
  tagId?: Array<number>;
  childTags?: boolean;
  artistId?: Array<number>;
  artistParticipationStatus?:
    | 'Everything'
    | 'OnlyMainAlbums'
    | 'OnlyCollaborations';
  childVoicebanks?: boolean;
  includeMembers?: boolean;
  barcode?: string;
  status?: 'Draft' | 'Finished' | 'Approved' | 'Locked';
  releaseDateAfter?: Date;
  releaseDateBefore?: Date;
  advancedFilters?: Array<any>;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?:
    | 'None'
    | 'Name'
    | 'ReleaseDate'
    | 'ReleaseDateWithNulls'
    | 'AdditionDate'
    | 'RatingAverage'
    | 'RatingTotal'
    | 'NameThenReleaseDate'
    | 'CollectionCount';
  preferAccurateMatches?: boolean;
  deleted?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'Discs'
    | 'Identifiers'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'Tracks'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface AlbumApiGetNamesParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  maxResults?: number;
}

export interface AlbumApiGetNewAlbumsParams {
  languagePreference?: 'Default' | 'Japanese' | 'Romaji' | 'English';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'Discs'
    | 'Identifiers'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'Tracks'
    | 'WebLinks';
}

export interface AlbumApiGetOneParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'Discs'
    | 'Identifiers'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'Tracks'
    | 'WebLinks';
  songFields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface AlbumApiGetReviewsParams {
  id: number;
  languageCode?: string;
}

export interface AlbumApiGetTopAlbumsParams {
  ignoreIds: Array<number>;
  languagePreference?: 'Default' | 'Japanese' | 'Romaji' | 'English';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'Discs'
    | 'Identifiers'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'Tracks'
    | 'WebLinks';
}

export interface AlbumApiGetTracksParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface AlbumApiGetTracksFieldsParams {
  id: number;
  field?: Array<string>;
  discNumber?: number;
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface AlbumApiGetUserCollectionsParams {
  id: number;
  languagePreference?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface AlbumApiPostEditCommentParams {
  commentId: number;
  contract: CommentForApiContract;
}

export interface AlbumApiPostNewCommentParams {
  id: number;
  contract: CommentForApiContract;
}

export interface AlbumApiPostReviewParams {
  id: number;
  reviewContract: AlbumReviewContract;
}

/**
 * AlbumApiApi - fetch parameter creator
 * @export
 */
export const AlbumApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes an album.
     * @param {AlbumApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDelete(params: AlbumApiDeleteParams, options: any = {}): FetchArgs {
      const { id, notes } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiDelete.',
        );
      }
      const localVarPath = `/api/albums/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {AlbumApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDeleteComment(
      params: AlbumApiDeleteCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling albumApiDeleteComment.',
        );
      }
      const localVarPath = `/api/albums/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {AlbumApiDeleteReviewParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDeleteReview(
      params: AlbumApiDeleteReviewParams,
      options: any = {},
    ): FetchArgs {
      const { reviewId, id } = params;
      // verify required parameter 'reviewId' is not null or undefined
      if (reviewId === null || reviewId === undefined) {
        throw new RequiredError(
          'reviewId',
          'Required parameter reviewId was null or undefined when calling albumApiDeleteReview.',
        );
      }
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiDeleteReview.',
        );
      }
      const localVarPath = `/api/albums/{id}/reviews/{reviewId}`
        .replace(`{${'reviewId'}}`, encodeURIComponent(String(reviewId)))
        .replace(`{${'id'}}`, encodeURIComponent(String(id)));
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for an album.
     * @param {AlbumApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetComments(
      params: AlbumApiGetCommentsParams,
      options: any = {},
    ): FetchArgs {
      const { id } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiGetComments.',
        );
      }
      const localVarPath = `/api/albums/{id}/comments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a page of albums.
     * @param {AlbumApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetList(
      params: AlbumApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        discTypes,
        tagName,
        tagId,
        childTags,
        artistId,
        artistParticipationStatus,
        childVoicebanks,
        includeMembers,
        barcode,
        status,
        releaseDateAfter,
        releaseDateBefore,
        advancedFilters,
        start,
        maxResults,
        getTotalCount,
        sort,
        preferAccurateMatches,
        deleted,
        nameMatchMode,
        fields,
        lang,
      } = params;
      const localVarPath = `/api/albums`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (discTypes !== undefined) {
        localVarQueryParameter['discTypes'] = discTypes;
      }

      if (tagName) {
        localVarQueryParameter['tagName'] = tagName;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (childTags !== undefined) {
        localVarQueryParameter['childTags'] = childTags;
      }

      if (artistId) {
        localVarQueryParameter['artistId'] = artistId;
      }

      if (artistParticipationStatus !== undefined) {
        localVarQueryParameter[
          'artistParticipationStatus'
        ] = artistParticipationStatus;
      }

      if (childVoicebanks !== undefined) {
        localVarQueryParameter['childVoicebanks'] = childVoicebanks;
      }

      if (includeMembers !== undefined) {
        localVarQueryParameter['includeMembers'] = includeMembers;
      }

      if (barcode !== undefined) {
        localVarQueryParameter['barcode'] = barcode;
      }

      if (status !== undefined) {
        localVarQueryParameter['status'] = status;
      }

      if (releaseDateAfter !== undefined) {
        localVarQueryParameter[
          'releaseDateAfter'
        ] = (releaseDateAfter as any).toISOString();
      }

      if (releaseDateBefore !== undefined) {
        localVarQueryParameter[
          'releaseDateBefore'
        ] = (releaseDateBefore as any).toISOString();
      }

      if (advancedFilters) {
        localVarQueryParameter['advancedFilters'] = advancedFilters;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (preferAccurateMatches !== undefined) {
        localVarQueryParameter['preferAccurateMatches'] = preferAccurateMatches;
      }

      if (deleted !== undefined) {
        localVarQueryParameter['deleted'] = deleted;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of album names. Ideal for autocomplete boxes.
     * @param {AlbumApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetNames(
      params: AlbumApiGetNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, nameMatchMode, maxResults } = params;
      const localVarPath = `/api/albums/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of upcoming or recent albums, same as front page.
     * @param {AlbumApiGetNewAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetNewAlbums(
      params: AlbumApiGetNewAlbumsParams = {},
      options: any = {},
    ): FetchArgs {
      const { languagePreference, fields } = params;
      const localVarPath = `/api/albums/new`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (languagePreference !== undefined) {
        localVarQueryParameter['languagePreference'] = languagePreference;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets an album by Id.
     * @param {AlbumApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetOne(params: AlbumApiGetOneParams, options: any = {}): FetchArgs {
      const { id, fields, songFields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiGetOne.',
        );
      }
      const localVarPath = `/api/albums/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (songFields !== undefined) {
        localVarQueryParameter['songFields'] = songFields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {AlbumApiGetReviewsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetReviews(
      params: AlbumApiGetReviewsParams,
      options: any = {},
    ): FetchArgs {
      const { id, languageCode } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiGetReviews.',
        );
      }
      const localVarPath = `/api/albums/{id}/reviews`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (languageCode !== undefined) {
        localVarQueryParameter['languageCode'] = languageCode;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of top rated albums, same as front page.
     * @param {AlbumApiGetTopAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTopAlbums(
      params: AlbumApiGetTopAlbumsParams,
      options: any = {},
    ): FetchArgs {
      const { ignoreIds, languagePreference, fields } = params;
      // verify required parameter 'ignoreIds' is not null or undefined
      if (ignoreIds === null || ignoreIds === undefined) {
        throw new RequiredError(
          'ignoreIds',
          'Required parameter ignoreIds was null or undefined when calling albumApiGetTopAlbums.',
        );
      }
      const localVarPath = `/api/albums/top`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (languagePreference !== undefined) {
        localVarQueryParameter['languagePreference'] = languagePreference;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'Array&lt;number&gt;' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(ignoreIds || {})
        : ignoreIds || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets tracks for an album.
     * @param {AlbumApiGetTracksParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTracks(
      params: AlbumApiGetTracksParams,
      options: any = {},
    ): FetchArgs {
      const { id, fields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiGetTracks.',
        );
      }
      const localVarPath = `/api/albums/{id}/tracks`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {AlbumApiGetTracksFieldsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTracksFields(
      params: AlbumApiGetTracksFieldsParams,
      options: any = {},
    ): FetchArgs {
      const { id, field, discNumber, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiGetTracksFields.',
        );
      }
      const localVarPath = `/api/albums/{id}/tracks/fields`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (field) {
        localVarQueryParameter['field'] = field;
      }

      if (discNumber !== undefined) {
        localVarQueryParameter['discNumber'] = discNumber;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {AlbumApiGetUserCollectionsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetUserCollections(
      params: AlbumApiGetUserCollectionsParams,
      options: any = {},
    ): FetchArgs {
      const { id, languagePreference } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiGetUserCollections.',
        );
      }
      const localVarPath = `/api/albums/{id}/user-collections`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (languagePreference !== undefined) {
        localVarQueryParameter['languagePreference'] = languagePreference;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {AlbumApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostEditComment(
      params: AlbumApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId, contract } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling albumApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling albumApiPostEditComment.',
        );
      }
      const localVarPath = `/api/albums/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {AlbumApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostNewComment(
      params: AlbumApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { id, contract } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling albumApiPostNewComment.',
        );
      }
      const localVarPath = `/api/albums/{id}/comments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {AlbumApiPostReviewParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostReview(
      params: AlbumApiPostReviewParams,
      options: any = {},
    ): FetchArgs {
      const { id, reviewContract } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling albumApiPostReview.',
        );
      }
      // verify required parameter 'reviewContract' is not null or undefined
      if (reviewContract === null || reviewContract === undefined) {
        throw new RequiredError(
          'reviewContract',
          'Required parameter reviewContract was null or undefined when calling albumApiPostReview.',
        );
      }
      const localVarPath = `/api/albums/{id}/reviews`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'AlbumReviewContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(reviewContract || {})
        : reviewContract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * AlbumApiApi - functional programming interface
 * @export
 */
export const AlbumApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Deletes an album.
     * @param {AlbumApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDelete(
      params: AlbumApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {AlbumApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDeleteComment(
      params: AlbumApiDeleteCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiDeleteComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {AlbumApiDeleteReviewParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDeleteReview(
      params: AlbumApiDeleteReviewParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiDeleteReview(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for an album.
     * @param {AlbumApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetComments(
      params: AlbumApiGetCommentsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<CommentForApiContract>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetComments(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a page of albums.
     * @param {AlbumApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetList(
      params: AlbumApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultAlbumForApiContract> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of album names. Ideal for autocomplete boxes.
     * @param {AlbumApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetNames(
      params: AlbumApiGetNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of upcoming or recent albums, same as front page.
     * @param {AlbumApiGetNewAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetNewAlbums(
      params: AlbumApiGetNewAlbumsParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<AlbumForApiContract>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetNewAlbums(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets an album by Id.
     * @param {AlbumApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetOne(
      params: AlbumApiGetOneParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<AlbumForApiContract> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetOne(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {AlbumApiGetReviewsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetReviews(
      params: AlbumApiGetReviewsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<AlbumReviewContract>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetReviews(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of top rated albums, same as front page.
     * @param {AlbumApiGetTopAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTopAlbums(
      params: AlbumApiGetTopAlbumsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<AlbumForApiContract>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetTopAlbums(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets tracks for an album.
     * @param {AlbumApiGetTracksParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTracks(
      params: AlbumApiGetTracksParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<SongInAlbumForApiContract>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetTracks(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {AlbumApiGetTracksFieldsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTracksFields(
      params: AlbumApiGetTracksFieldsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<{ [key: string]: string }>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetTracksFields(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {AlbumApiGetUserCollectionsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetUserCollections(
      params: AlbumApiGetUserCollectionsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<AlbumForUserForApiContract>> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiGetUserCollections(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {AlbumApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostEditComment(
      params: AlbumApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {AlbumApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostNewComment(
      params: AlbumApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {AlbumApiPostReviewParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostReview(
      params: AlbumApiPostReviewParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<AlbumReviewContract> {
      const localVarFetchArgs = AlbumApiApiFetchParamCreator(
        configuration,
      ).albumApiPostReview(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * AlbumApiApi - factory interface
 * @export
 */
export const AlbumApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes an album.
     * @param {AlbumApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDelete(params: AlbumApiDeleteParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiDelete(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {AlbumApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDeleteComment(params: AlbumApiDeleteCommentParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiDeleteComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {AlbumApiDeleteReviewParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiDeleteReview(params: AlbumApiDeleteReviewParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiDeleteReview(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for an album.
     * @param {AlbumApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetComments(params: AlbumApiGetCommentsParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiGetComments(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a page of albums.
     * @param {AlbumApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetList(params: AlbumApiGetListParams = {}, options?: any) {
      return AlbumApiApiFp(configuration).albumApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of album names. Ideal for autocomplete boxes.
     * @param {AlbumApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetNames(params: AlbumApiGetNamesParams = {}, options?: any) {
      return AlbumApiApiFp(configuration).albumApiGetNames(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of upcoming or recent albums, same as front page.
     * @param {AlbumApiGetNewAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetNewAlbums(
      params: AlbumApiGetNewAlbumsParams = {},
      options?: any,
    ) {
      return AlbumApiApiFp(configuration).albumApiGetNewAlbums(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets an album by Id.
     * @param {AlbumApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetOne(params: AlbumApiGetOneParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiGetOne(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @param {AlbumApiGetReviewsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetReviews(params: AlbumApiGetReviewsParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiGetReviews(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of top rated albums, same as front page.
     * @param {AlbumApiGetTopAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTopAlbums(params: AlbumApiGetTopAlbumsParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiGetTopAlbums(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets tracks for an album.
     * @param {AlbumApiGetTracksParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTracks(params: AlbumApiGetTracksParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiGetTracks(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @param {AlbumApiGetTracksFieldsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetTracksFields(
      params: AlbumApiGetTracksFieldsParams,
      options?: any,
    ) {
      return AlbumApiApiFp(configuration).albumApiGetTracksFields(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {AlbumApiGetUserCollectionsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiGetUserCollections(
      params: AlbumApiGetUserCollectionsParams,
      options?: any,
    ) {
      return AlbumApiApiFp(configuration).albumApiGetUserCollections(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {AlbumApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostEditComment(
      params: AlbumApiPostEditCommentParams,
      options?: any,
    ) {
      return AlbumApiApiFp(configuration).albumApiPostEditComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {AlbumApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostNewComment(
      params: AlbumApiPostNewCommentParams,
      options?: any,
    ) {
      return AlbumApiApiFp(configuration).albumApiPostNewComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {AlbumApiPostReviewParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    albumApiPostReview(params: AlbumApiPostReviewParams, options?: any) {
      return AlbumApiApiFp(configuration).albumApiPostReview(params, options)(
        fetch,
        basePath,
      );
    },
  };
};

export const useAlbumApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => AlbumApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface ArtistApiDeleteParams {
  id: number;
  notes?: string;
}

export interface ArtistApiDeleteCommentParams {
  commentId: number;
}

export interface ArtistApiGetCommentsParams {
  id: number;
}

export interface ArtistApiGetListParams {
  query?: string;
  artistTypes?: string;
  allowBaseVoicebanks?: boolean;
  tagName?: Array<string>;
  tagId?: Array<number>;
  childTags?: boolean;
  followedByUserId?: number;
  status?: 'Draft' | 'Finished' | 'Approved' | 'Locked';
  advancedFilters?: Array<any>;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?:
    | 'None'
    | 'Name'
    | 'AdditionDate'
    | 'AdditionDateAsc'
    | 'ReleaseDate'
    | 'SongCount'
    | 'SongRating'
    | 'FollowerCount';
  preferAccurateMatches?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'ArtistLinks'
    | 'ArtistLinksReverse'
    | 'BaseVoicebank'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Tags'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface ArtistApiGetNamesParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  maxResults?: number;
}

export interface ArtistApiGetOneParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'ArtistLinks'
    | 'ArtistLinksReverse'
    | 'BaseVoicebank'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Tags'
    | 'WebLinks';
  relations?:
    | 'None'
    | 'LatestAlbums'
    | 'LatestEvents'
    | 'LatestSongs'
    | 'PopularAlbums'
    | 'PopularSongs'
    | 'All';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface ArtistApiPostEditCommentParams {
  commentId: number;
  contract: CommentForApiContract;
}

export interface ArtistApiPostNewCommentParams {
  id: number;
  contract: CommentForApiContract;
}

/**
 * ArtistApiApi - fetch parameter creator
 * @export
 */
export const ArtistApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes an artist.
     * @param {ArtistApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiDelete(
      params: ArtistApiDeleteParams,
      options: any = {},
    ): FetchArgs {
      const { id, notes } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling artistApiDelete.',
        );
      }
      const localVarPath = `/api/artists/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {ArtistApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiDeleteComment(
      params: ArtistApiDeleteCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling artistApiDeleteComment.',
        );
      }
      const localVarPath = `/api/artists/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for an artist.
     * @param {ArtistApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetComments(
      params: ArtistApiGetCommentsParams,
      options: any = {},
    ): FetchArgs {
      const { id } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling artistApiGetComments.',
        );
      }
      const localVarPath = `/api/artists/{id}/comments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Find artists.
     * @param {ArtistApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetList(
      params: ArtistApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        artistTypes,
        allowBaseVoicebanks,
        tagName,
        tagId,
        childTags,
        followedByUserId,
        status,
        advancedFilters,
        start,
        maxResults,
        getTotalCount,
        sort,
        preferAccurateMatches,
        nameMatchMode,
        fields,
        lang,
      } = params;
      const localVarPath = `/api/artists`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (artistTypes !== undefined) {
        localVarQueryParameter['artistTypes'] = artistTypes;
      }

      if (allowBaseVoicebanks !== undefined) {
        localVarQueryParameter['allowBaseVoicebanks'] = allowBaseVoicebanks;
      }

      if (tagName) {
        localVarQueryParameter['tagName'] = tagName;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (childTags !== undefined) {
        localVarQueryParameter['childTags'] = childTags;
      }

      if (followedByUserId !== undefined) {
        localVarQueryParameter['followedByUserId'] = followedByUserId;
      }

      if (status !== undefined) {
        localVarQueryParameter['status'] = status;
      }

      if (advancedFilters) {
        localVarQueryParameter['advancedFilters'] = advancedFilters;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (preferAccurateMatches !== undefined) {
        localVarQueryParameter['preferAccurateMatches'] = preferAccurateMatches;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of artist names. Ideal for autocomplete boxes.
     * @param {ArtistApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetNames(
      params: ArtistApiGetNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, nameMatchMode, maxResults } = params;
      const localVarPath = `/api/artists/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets an artist by Id.
     * @param {ArtistApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetOne(
      params: ArtistApiGetOneParams,
      options: any = {},
    ): FetchArgs {
      const { id, fields, relations, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling artistApiGetOne.',
        );
      }
      const localVarPath = `/api/artists/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (relations !== undefined) {
        localVarQueryParameter['relations'] = relations;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {ArtistApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiPostEditComment(
      params: ArtistApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId, contract } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling artistApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling artistApiPostEditComment.',
        );
      }
      const localVarPath = `/api/artists/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {ArtistApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiPostNewComment(
      params: ArtistApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { id, contract } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling artistApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling artistApiPostNewComment.',
        );
      }
      const localVarPath = `/api/artists/{id}/comments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * ArtistApiApi - functional programming interface
 * @export
 */
export const ArtistApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Deletes an artist.
     * @param {ArtistApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiDelete(
      params: ArtistApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {ArtistApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiDeleteComment(
      params: ArtistApiDeleteCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiDeleteComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for an artist.
     * @param {ArtistApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetComments(
      params: ArtistApiGetCommentsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<CommentForApiContract>> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiGetComments(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Find artists.
     * @param {ArtistApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetList(
      params: ArtistApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultArtistForApiContract> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of artist names. Ideal for autocomplete boxes.
     * @param {ArtistApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetNames(
      params: ArtistApiGetNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiGetNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets an artist by Id.
     * @param {ArtistApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetOne(
      params: ArtistApiGetOneParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<ArtistForApiContract> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiGetOne(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {ArtistApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiPostEditComment(
      params: ArtistApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {ArtistApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiPostNewComment(
      params: ArtistApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = ArtistApiApiFetchParamCreator(
        configuration,
      ).artistApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * ArtistApiApi - factory interface
 * @export
 */
export const ArtistApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes an artist.
     * @param {ArtistApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiDelete(params: ArtistApiDeleteParams, options?: any) {
      return ArtistApiApiFp(configuration).artistApiDelete(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {ArtistApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiDeleteComment(
      params: ArtistApiDeleteCommentParams,
      options?: any,
    ) {
      return ArtistApiApiFp(configuration).artistApiDeleteComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for an artist.
     * @param {ArtistApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetComments(params: ArtistApiGetCommentsParams, options?: any) {
      return ArtistApiApiFp(configuration).artistApiGetComments(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Find artists.
     * @param {ArtistApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetList(params: ArtistApiGetListParams = {}, options?: any) {
      return ArtistApiApiFp(configuration).artistApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of artist names. Ideal for autocomplete boxes.
     * @param {ArtistApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetNames(params: ArtistApiGetNamesParams = {}, options?: any) {
      return ArtistApiApiFp(configuration).artistApiGetNames(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets an artist by Id.
     * @param {ArtistApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiGetOne(params: ArtistApiGetOneParams, options?: any) {
      return ArtistApiApiFp(configuration).artistApiGetOne(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {ArtistApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiPostEditComment(
      params: ArtistApiPostEditCommentParams,
      options?: any,
    ) {
      return ArtistApiApiFp(configuration).artistApiPostEditComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {ArtistApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    artistApiPostNewComment(
      params: ArtistApiPostNewCommentParams,
      options?: any,
    ) {
      return ArtistApiApiFp(configuration).artistApiPostNewComment(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useArtistApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => ArtistApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface CommentApiDeleteCommentParams {
  entryType:
    | 'Undefined'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  commentId: number;
}

export interface CommentApiGetCommentsParams {
  entryType:
    | 'Undefined'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  entryId: number;
}

export interface CommentApiPostEditCommentParams {
  entryType:
    | 'Undefined'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  commentId: number;
  contract: CommentForApiContract;
}

export interface CommentApiPostNewCommentParams {
  entryType:
    | 'Undefined'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  contract: CommentForApiContract;
}

/**
 * CommentApiApi - fetch parameter creator
 * @export
 */
export const CommentApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {CommentApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiDeleteComment(
      params: CommentApiDeleteCommentParams,
      options: any = {},
    ): FetchArgs {
      const { entryType, commentId } = params;
      // verify required parameter 'entryType' is not null or undefined
      if (entryType === null || entryType === undefined) {
        throw new RequiredError(
          'entryType',
          'Required parameter entryType was null or undefined when calling commentApiDeleteComment.',
        );
      }
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling commentApiDeleteComment.',
        );
      }
      const localVarPath = `/api/comments/{entryType}-comments/{commentId}`
        .replace(`{${'entryType'}}`, encodeURIComponent(String(entryType)))
        .replace(`{${'commentId'}}`, encodeURIComponent(String(commentId)));
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of comments for an entry.
     * @param {CommentApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiGetComments(
      params: CommentApiGetCommentsParams,
      options: any = {},
    ): FetchArgs {
      const { entryType, entryId } = params;
      // verify required parameter 'entryType' is not null or undefined
      if (entryType === null || entryType === undefined) {
        throw new RequiredError(
          'entryType',
          'Required parameter entryType was null or undefined when calling commentApiGetComments.',
        );
      }
      // verify required parameter 'entryId' is not null or undefined
      if (entryId === null || entryId === undefined) {
        throw new RequiredError(
          'entryId',
          'Required parameter entryId was null or undefined when calling commentApiGetComments.',
        );
      }
      const localVarPath = `/api/comments/{entryType}-comments`.replace(
        `{${'entryType'}}`,
        encodeURIComponent(String(entryType)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (entryId !== undefined) {
        localVarQueryParameter['entryId'] = entryId;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {CommentApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiPostEditComment(
      params: CommentApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { entryType, commentId, contract } = params;
      // verify required parameter 'entryType' is not null or undefined
      if (entryType === null || entryType === undefined) {
        throw new RequiredError(
          'entryType',
          'Required parameter entryType was null or undefined when calling commentApiPostEditComment.',
        );
      }
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling commentApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling commentApiPostEditComment.',
        );
      }
      const localVarPath = `/api/comments/{entryType}-comments/{commentId}`
        .replace(`{${'entryType'}}`, encodeURIComponent(String(entryType)))
        .replace(`{${'commentId'}}`, encodeURIComponent(String(commentId)));
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {CommentApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiPostNewComment(
      params: CommentApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { entryType, contract } = params;
      // verify required parameter 'entryType' is not null or undefined
      if (entryType === null || entryType === undefined) {
        throw new RequiredError(
          'entryType',
          'Required parameter entryType was null or undefined when calling commentApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling commentApiPostNewComment.',
        );
      }
      const localVarPath = `/api/comments/{entryType}-comments`.replace(
        `{${'entryType'}}`,
        encodeURIComponent(String(entryType)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * CommentApiApi - functional programming interface
 * @export
 */
export const CommentApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {CommentApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiDeleteComment(
      params: CommentApiDeleteCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = CommentApiApiFetchParamCreator(
        configuration,
      ).commentApiDeleteComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of comments for an entry.
     * @param {CommentApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiGetComments(
      params: CommentApiGetCommentsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultCommentForApiContract> {
      const localVarFetchArgs = CommentApiApiFetchParamCreator(
        configuration,
      ).commentApiGetComments(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {CommentApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiPostEditComment(
      params: CommentApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = CommentApiApiFetchParamCreator(
        configuration,
      ).commentApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {CommentApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiPostNewComment(
      params: CommentApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = CommentApiApiFetchParamCreator(
        configuration,
      ).commentApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * CommentApiApi - factory interface
 * @export
 */
export const CommentApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {CommentApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiDeleteComment(
      params: CommentApiDeleteCommentParams,
      options?: any,
    ) {
      return CommentApiApiFp(configuration).commentApiDeleteComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of comments for an entry.
     * @param {CommentApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiGetComments(params: CommentApiGetCommentsParams, options?: any) {
      return CommentApiApiFp(configuration).commentApiGetComments(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {CommentApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiPostEditComment(
      params: CommentApiPostEditCommentParams,
      options?: any,
    ) {
      return CommentApiApiFp(configuration).commentApiPostEditComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {CommentApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    commentApiPostNewComment(
      params: CommentApiPostNewCommentParams,
      options?: any,
    ) {
      return CommentApiApiFp(configuration).commentApiPostNewComment(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useCommentApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => CommentApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface DiscussionApiDeleteCommentParams {
  commentId: number;
}

export interface DiscussionApiDeleteTopicParams {
  topicId: number;
}

export interface DiscussionApiGetFoldersParams {
  fields?: 'None' | 'LastTopic' | 'TopicCount';
}

export interface DiscussionApiGetTopicParams {
  topicId: number;
  fields?:
    | 'None'
    | 'Comments'
    | 'CommentCount'
    | 'Content'
    | 'LastComment'
    | 'All';
}

export interface DiscussionApiGetTopicsParams {
  folderId?: number;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?: 'None' | 'Name' | 'DateCreated' | 'LastCommentDate';
  fields?:
    | 'None'
    | 'Comments'
    | 'CommentCount'
    | 'Content'
    | 'LastComment'
    | 'All';
}

export interface DiscussionApiGetTopicsForFolderParams {
  folderId: number;
  fields?:
    | 'None'
    | 'Comments'
    | 'CommentCount'
    | 'Content'
    | 'LastComment'
    | 'All';
}

export interface DiscussionApiPostEditCommentParams {
  commentId: number;
  contract: CommentForApiContract;
}

export interface DiscussionApiPostEditTopicParams {
  topicId: number;
  contract: DiscussionTopicContract;
}

export interface DiscussionApiPostNewCommentParams {
  topicId: number;
  contract: CommentForApiContract;
}

export interface DiscussionApiPostNewFolderParams {
  contract: DiscussionFolderContract;
}

export interface DiscussionApiPostNewTopicParams {
  folderId: number;
  contract: DiscussionTopicContract;
}

/**
 * DiscussionApiApi - fetch parameter creator
 * @export
 */
export const DiscussionApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @param {DiscussionApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiDeleteComment(
      params: DiscussionApiDeleteCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling discussionApiDeleteComment.',
        );
      }
      const localVarPath = `/api/discussions/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiDeleteTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiDeleteTopic(
      params: DiscussionApiDeleteTopicParams,
      options: any = {},
    ): FetchArgs {
      const { topicId } = params;
      // verify required parameter 'topicId' is not null or undefined
      if (topicId === null || topicId === undefined) {
        throw new RequiredError(
          'topicId',
          'Required parameter topicId was null or undefined when calling discussionApiDeleteTopic.',
        );
      }
      const localVarPath = `/api/discussions/topics/{topicId}`.replace(
        `{${'topicId'}}`,
        encodeURIComponent(String(topicId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiGetFoldersParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetFolders(
      params: DiscussionApiGetFoldersParams = {},
      options: any = {},
    ): FetchArgs {
      const { fields } = params;
      const localVarPath = `/api/discussions/folders`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiGetTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopic(
      params: DiscussionApiGetTopicParams,
      options: any = {},
    ): FetchArgs {
      const { topicId, fields } = params;
      // verify required parameter 'topicId' is not null or undefined
      if (topicId === null || topicId === undefined) {
        throw new RequiredError(
          'topicId',
          'Required parameter topicId was null or undefined when calling discussionApiGetTopic.',
        );
      }
      const localVarPath = `/api/discussions/topics/{topicId}`.replace(
        `{${'topicId'}}`,
        encodeURIComponent(String(topicId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiGetTopicsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopics(
      params: DiscussionApiGetTopicsParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        folderId,
        start,
        maxResults,
        getTotalCount,
        sort,
        fields,
      } = params;
      const localVarPath = `/api/discussions/topics`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (folderId !== undefined) {
        localVarQueryParameter['folderId'] = folderId;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiGetTopicsForFolderParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopicsForFolder(
      params: DiscussionApiGetTopicsForFolderParams,
      options: any = {},
    ): FetchArgs {
      const { folderId, fields } = params;
      // verify required parameter 'folderId' is not null or undefined
      if (folderId === null || folderId === undefined) {
        throw new RequiredError(
          'folderId',
          'Required parameter folderId was null or undefined when calling discussionApiGetTopicsForFolder.',
        );
      }
      const localVarPath = `/api/discussions/folders/{folderId}/topics`.replace(
        `{${'folderId'}}`,
        encodeURIComponent(String(folderId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostEditComment(
      params: DiscussionApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId, contract } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling discussionApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling discussionApiPostEditComment.',
        );
      }
      const localVarPath = `/api/discussions/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiPostEditTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostEditTopic(
      params: DiscussionApiPostEditTopicParams,
      options: any = {},
    ): FetchArgs {
      const { topicId, contract } = params;
      // verify required parameter 'topicId' is not null or undefined
      if (topicId === null || topicId === undefined) {
        throw new RequiredError(
          'topicId',
          'Required parameter topicId was null or undefined when calling discussionApiPostEditTopic.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling discussionApiPostEditTopic.',
        );
      }
      const localVarPath = `/api/discussions/topics/{topicId}`.replace(
        `{${'topicId'}}`,
        encodeURIComponent(String(topicId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'DiscussionTopicContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewComment(
      params: DiscussionApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { topicId, contract } = params;
      // verify required parameter 'topicId' is not null or undefined
      if (topicId === null || topicId === undefined) {
        throw new RequiredError(
          'topicId',
          'Required parameter topicId was null or undefined when calling discussionApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling discussionApiPostNewComment.',
        );
      }
      const localVarPath = `/api/discussions/topics/{topicId}/comments`.replace(
        `{${'topicId'}}`,
        encodeURIComponent(String(topicId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiPostNewFolderParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewFolder(
      params: DiscussionApiPostNewFolderParams,
      options: any = {},
    ): FetchArgs {
      const { contract } = params;
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling discussionApiPostNewFolder.',
        );
      }
      const localVarPath = `/api/discussions/folders`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'DiscussionFolderContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {DiscussionApiPostNewTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewTopic(
      params: DiscussionApiPostNewTopicParams,
      options: any = {},
    ): FetchArgs {
      const { folderId, contract } = params;
      // verify required parameter 'folderId' is not null or undefined
      if (folderId === null || folderId === undefined) {
        throw new RequiredError(
          'folderId',
          'Required parameter folderId was null or undefined when calling discussionApiPostNewTopic.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling discussionApiPostNewTopic.',
        );
      }
      const localVarPath = `/api/discussions/folders/{folderId}/topics`.replace(
        `{${'folderId'}}`,
        encodeURIComponent(String(folderId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'DiscussionTopicContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * DiscussionApiApi - functional programming interface
 * @export
 */
export const DiscussionApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @param {DiscussionApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiDeleteComment(
      params: DiscussionApiDeleteCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiDeleteComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiDeleteTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiDeleteTopic(
      params: DiscussionApiDeleteTopicParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiDeleteTopic(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiGetFoldersParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetFolders(
      params: DiscussionApiGetFoldersParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<DiscussionFolderContract>> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiGetFolders(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiGetTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopic(
      params: DiscussionApiGetTopicParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<DiscussionTopicContract> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiGetTopic(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiGetTopicsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopics(
      params: DiscussionApiGetTopicsParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultDiscussionTopicContract> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiGetTopics(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiGetTopicsForFolderParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopicsForFolder(
      params: DiscussionApiGetTopicsForFolderParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<DiscussionTopicContract>> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiGetTopicsForFolder(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostEditComment(
      params: DiscussionApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiPostEditTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostEditTopic(
      params: DiscussionApiPostEditTopicParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiPostEditTopic(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewComment(
      params: DiscussionApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiPostNewFolderParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewFolder(
      params: DiscussionApiPostNewFolderParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<DiscussionFolderContract> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiPostNewFolder(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {DiscussionApiPostNewTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewTopic(
      params: DiscussionApiPostNewTopicParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<DiscussionTopicContract> {
      const localVarFetchArgs = DiscussionApiApiFetchParamCreator(
        configuration,
      ).discussionApiPostNewTopic(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * DiscussionApiApi - factory interface
 * @export
 */
export const DiscussionApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @param {DiscussionApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiDeleteComment(
      params: DiscussionApiDeleteCommentParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiDeleteComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiDeleteTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiDeleteTopic(
      params: DiscussionApiDeleteTopicParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiDeleteTopic(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiGetFoldersParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetFolders(
      params: DiscussionApiGetFoldersParams = {},
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiGetFolders(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiGetTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopic(params: DiscussionApiGetTopicParams, options?: any) {
      return DiscussionApiApiFp(configuration).discussionApiGetTopic(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiGetTopicsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopics(
      params: DiscussionApiGetTopicsParams = {},
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiGetTopics(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiGetTopicsForFolderParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiGetTopicsForFolder(
      params: DiscussionApiGetTopicsForFolderParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiGetTopicsForFolder(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostEditComment(
      params: DiscussionApiPostEditCommentParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiPostEditComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiPostEditTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostEditTopic(
      params: DiscussionApiPostEditTopicParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiPostEditTopic(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewComment(
      params: DiscussionApiPostNewCommentParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiPostNewComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiPostNewFolderParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewFolder(
      params: DiscussionApiPostNewFolderParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiPostNewFolder(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {DiscussionApiPostNewTopicParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    discussionApiPostNewTopic(
      params: DiscussionApiPostNewTopicParams,
      options?: any,
    ) {
      return DiscussionApiApiFp(configuration).discussionApiPostNewTopic(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useDiscussionApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => DiscussionApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface EntryApiGetListParams {
  query?: string;
  tagName?: Array<string>;
  tagId?: Array<number>;
  childTags?: boolean;
  entryTypes?:
    | 'Nothing'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  status?: 'Draft' | 'Finished' | 'Approved' | 'Locked';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?: 'None' | 'Name' | 'AdditionDate' | 'ActivityDate';
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'Tags'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface EntryApiGetNamesParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  maxResults?: number;
}

/**
 * EntryApiApi - fetch parameter creator
 * @export
 */
export const EntryApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Find entries.
     * @param {EntryApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryApiGetList(
      params: EntryApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        tagName,
        tagId,
        childTags,
        entryTypes,
        status,
        start,
        maxResults,
        getTotalCount,
        sort,
        nameMatchMode,
        fields,
        lang,
      } = params;
      const localVarPath = `/api/entries`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (tagName) {
        localVarQueryParameter['tagName'] = tagName;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (childTags !== undefined) {
        localVarQueryParameter['childTags'] = childTags;
      }

      if (entryTypes !== undefined) {
        localVarQueryParameter['entryTypes'] = entryTypes;
      }

      if (status !== undefined) {
        localVarQueryParameter['status'] = status;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of entry names. Ideal for autocomplete boxes.
     * @param {EntryApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryApiGetNames(
      params: EntryApiGetNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, nameMatchMode, maxResults } = params;
      const localVarPath = `/api/entries/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * EntryApiApi - functional programming interface
 * @export
 */
export const EntryApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Find entries.
     * @param {EntryApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryApiGetList(
      params: EntryApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultEntryForApiContract> {
      const localVarFetchArgs = EntryApiApiFetchParamCreator(
        configuration,
      ).entryApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of entry names. Ideal for autocomplete boxes.
     * @param {EntryApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryApiGetNames(
      params: EntryApiGetNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = EntryApiApiFetchParamCreator(
        configuration,
      ).entryApiGetNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * EntryApiApi - factory interface
 * @export
 */
export const EntryApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Find entries.
     * @param {EntryApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryApiGetList(params: EntryApiGetListParams = {}, options?: any) {
      return EntryApiApiFp(configuration).entryApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of entry names. Ideal for autocomplete boxes.
     * @param {EntryApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryApiGetNames(params: EntryApiGetNamesParams = {}, options?: any) {
      return EntryApiApiFp(configuration).entryApiGetNames(params, options)(
        fetch,
        basePath,
      );
    },
  };
};

export const useEntryApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => EntryApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface EntryTypesApiGetMappedTagParams {
  entryType:
    | 'Undefined'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  subType: string;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'AliasedTo'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Parent'
    | 'RelatedTags'
    | 'TranslatedDescription'
    | 'WebLinks';
}

/**
 * EntryTypesApiApi - fetch parameter creator
 * @export
 */
export const EntryTypesApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @param {EntryTypesApiGetMappedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryTypesApiGetMappedTag(
      params: EntryTypesApiGetMappedTagParams,
      options: any = {},
    ): FetchArgs {
      const { entryType, subType, fields } = params;
      // verify required parameter 'entryType' is not null or undefined
      if (entryType === null || entryType === undefined) {
        throw new RequiredError(
          'entryType',
          'Required parameter entryType was null or undefined when calling entryTypesApiGetMappedTag.',
        );
      }
      // verify required parameter 'subType' is not null or undefined
      if (subType === null || subType === undefined) {
        throw new RequiredError(
          'subType',
          'Required parameter subType was null or undefined when calling entryTypesApiGetMappedTag.',
        );
      }
      const localVarPath = `/api/entry-types/{entryType}/{subType}/tag`
        .replace(`{${'entryType'}}`, encodeURIComponent(String(entryType)))
        .replace(`{${'subType'}}`, encodeURIComponent(String(subType)));
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * EntryTypesApiApi - functional programming interface
 * @export
 */
export const EntryTypesApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @param {EntryTypesApiGetMappedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryTypesApiGetMappedTag(
      params: EntryTypesApiGetMappedTagParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<TagForApiContract> {
      const localVarFetchArgs = EntryTypesApiApiFetchParamCreator(
        configuration,
      ).entryTypesApiGetMappedTag(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * EntryTypesApiApi - factory interface
 * @export
 */
export const EntryTypesApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @param {EntryTypesApiGetMappedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    entryTypesApiGetMappedTag(
      params: EntryTypesApiGetMappedTagParams,
      options?: any,
    ) {
      return EntryTypesApiApiFp(configuration).entryTypesApiGetMappedTag(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useEntryTypesApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => EntryTypesApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface PVApiGetListParams {
  name?: string;
  author?: string;
  service?:
    | 'NicoNicoDouga'
    | 'Youtube'
    | 'SoundCloud'
    | 'Vimeo'
    | 'Piapro'
    | 'Bilibili'
    | 'File'
    | 'LocalFile'
    | 'Creofuga'
    | 'Bandcamp';
  maxResults?: number;
  getTotalCount?: boolean;
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

/**
 * PVApiApi - fetch parameter creator
 * @export
 */
export const PVApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Gets a list of PVs for songs.
     * @param {PVApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    pVApiGetList(
      params: PVApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const { name, author, service, maxResults, getTotalCount, lang } = params;
      const localVarPath = `/api/pvs/for-songs`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (name !== undefined) {
        localVarQueryParameter['name'] = name;
      }

      if (author !== undefined) {
        localVarQueryParameter['author'] = author;
      }

      if (service !== undefined) {
        localVarQueryParameter['service'] = service;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * PVApiApi - functional programming interface
 * @export
 */
export const PVApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Gets a list of PVs for songs.
     * @param {PVApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    pVApiGetList(
      params: PVApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultPVForSongContract> {
      const localVarFetchArgs = PVApiApiFetchParamCreator(
        configuration,
      ).pVApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * PVApiApi - factory interface
 * @export
 */
export const PVApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Gets a list of PVs for songs.
     * @param {PVApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    pVApiGetList(params: PVApiGetListParams = {}, options?: any) {
      return PVApiApiFp(configuration).pVApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
  };
};

export const usePVApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => PVApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface ReleaseEventApiDeleteParams {
  id: number;
  notes?: string;
  hardDelete?: boolean;
}

export interface ReleaseEventApiGetAlbumsParams {
  eventId: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'Discs'
    | 'Identifiers'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'Tracks'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface ReleaseEventApiGetListParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  seriesId?: number;
  afterDate?: Date;
  beforeDate?: Date;
  category?:
    | 'Unspecified'
    | 'AlbumRelease'
    | 'Anniversary'
    | 'Club'
    | 'Concert'
    | 'Contest'
    | 'Convention'
    | 'Other';
  userCollectionId?: number;
  tagId?: Array<number>;
  childTags?: boolean;
  artistId?: Array<number>;
  childVoicebanks?: boolean;
  includeMembers?: boolean;
  status?: 'Draft' | 'Finished' | 'Approved' | 'Locked';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?: 'None' | 'Name' | 'Date' | 'AdditionDate' | 'SeriesName' | 'VenueName';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Series'
    | 'SongList'
    | 'Tags'
    | 'Venue'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface ReleaseEventApiGetNamesParams {
  query?: string;
  maxResults?: number;
}

export interface ReleaseEventApiGetOneParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Series'
    | 'SongList'
    | 'Tags'
    | 'Venue'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface ReleaseEventApiGetPublishedSongsParams {
  eventId: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface ReleaseEventApiPostReportParams {
  eventId: number;
  reportType: 'InvalidInfo' | 'Duplicate' | 'Inappropriate' | 'Other';
  notes: string;
  versionNumber: number;
}

/**
 * ReleaseEventApiApi - fetch parameter creator
 * @export
 */
export const ReleaseEventApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes an event.
     * @param {ReleaseEventApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiDelete(
      params: ReleaseEventApiDeleteParams,
      options: any = {},
    ): FetchArgs {
      const { id, notes, hardDelete } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling releaseEventApiDelete.',
        );
      }
      const localVarPath = `/api/releaseEvents/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (hardDelete !== undefined) {
        localVarQueryParameter['hardDelete'] = hardDelete;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of albums for a specific event.
     * @param {ReleaseEventApiGetAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetAlbums(
      params: ReleaseEventApiGetAlbumsParams,
      options: any = {},
    ): FetchArgs {
      const { eventId, fields, lang } = params;
      // verify required parameter 'eventId' is not null or undefined
      if (eventId === null || eventId === undefined) {
        throw new RequiredError(
          'eventId',
          'Required parameter eventId was null or undefined when calling releaseEventApiGetAlbums.',
        );
      }
      const localVarPath = `/api/releaseEvents/{eventId}/albums`.replace(
        `{${'eventId'}}`,
        encodeURIComponent(String(eventId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a page of events.
     * @param {ReleaseEventApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetList(
      params: ReleaseEventApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        nameMatchMode,
        seriesId,
        afterDate,
        beforeDate,
        category,
        userCollectionId,
        tagId,
        childTags,
        artistId,
        childVoicebanks,
        includeMembers,
        status,
        start,
        maxResults,
        getTotalCount,
        sort,
        fields,
        lang,
      } = params;
      const localVarPath = `/api/releaseEvents`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (seriesId !== undefined) {
        localVarQueryParameter['seriesId'] = seriesId;
      }

      if (afterDate !== undefined) {
        localVarQueryParameter['afterDate'] = (afterDate as any).toISOString();
      }

      if (beforeDate !== undefined) {
        localVarQueryParameter[
          'beforeDate'
        ] = (beforeDate as any).toISOString();
      }

      if (category !== undefined) {
        localVarQueryParameter['category'] = category;
      }

      if (userCollectionId !== undefined) {
        localVarQueryParameter['userCollectionId'] = userCollectionId;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (childTags !== undefined) {
        localVarQueryParameter['childTags'] = childTags;
      }

      if (artistId) {
        localVarQueryParameter['artistId'] = artistId;
      }

      if (childVoicebanks !== undefined) {
        localVarQueryParameter['childVoicebanks'] = childVoicebanks;
      }

      if (includeMembers !== undefined) {
        localVarQueryParameter['includeMembers'] = includeMembers;
      }

      if (status !== undefined) {
        localVarQueryParameter['status'] = status;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Find event names by a part of name.                            Matching is done anywhere from the name.
     * @param {ReleaseEventApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetNames(
      params: ReleaseEventApiGetNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, maxResults } = params;
      const localVarPath = `/api/releaseEvents/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {ReleaseEventApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetOne(
      params: ReleaseEventApiGetOneParams,
      options: any = {},
    ): FetchArgs {
      const { id, fields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling releaseEventApiGetOne.',
        );
      }
      const localVarPath = `/api/releaseEvents/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of songs for a specific event.
     * @param {ReleaseEventApiGetPublishedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetPublishedSongs(
      params: ReleaseEventApiGetPublishedSongsParams,
      options: any = {},
    ): FetchArgs {
      const { eventId, fields, lang } = params;
      // verify required parameter 'eventId' is not null or undefined
      if (eventId === null || eventId === undefined) {
        throw new RequiredError(
          'eventId',
          'Required parameter eventId was null or undefined when calling releaseEventApiGetPublishedSongs.',
        );
      }
      const localVarPath = `/api/releaseEvents/{eventId}/published-songs`.replace(
        `{${'eventId'}}`,
        encodeURIComponent(String(eventId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Creates a new report.
     * @param {ReleaseEventApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiPostReport(
      params: ReleaseEventApiPostReportParams,
      options: any = {},
    ): FetchArgs {
      const { eventId, reportType, notes, versionNumber } = params;
      // verify required parameter 'eventId' is not null or undefined
      if (eventId === null || eventId === undefined) {
        throw new RequiredError(
          'eventId',
          'Required parameter eventId was null or undefined when calling releaseEventApiPostReport.',
        );
      }
      // verify required parameter 'reportType' is not null or undefined
      if (reportType === null || reportType === undefined) {
        throw new RequiredError(
          'reportType',
          'Required parameter reportType was null or undefined when calling releaseEventApiPostReport.',
        );
      }
      // verify required parameter 'notes' is not null or undefined
      if (notes === null || notes === undefined) {
        throw new RequiredError(
          'notes',
          'Required parameter notes was null or undefined when calling releaseEventApiPostReport.',
        );
      }
      // verify required parameter 'versionNumber' is not null or undefined
      if (versionNumber === null || versionNumber === undefined) {
        throw new RequiredError(
          'versionNumber',
          'Required parameter versionNumber was null or undefined when calling releaseEventApiPostReport.',
        );
      }
      const localVarPath = `/api/releaseEvents/{eventId}/reports`.replace(
        `{${'eventId'}}`,
        encodeURIComponent(String(eventId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (reportType !== undefined) {
        localVarQueryParameter['reportType'] = reportType;
      }

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (versionNumber !== undefined) {
        localVarQueryParameter['versionNumber'] = versionNumber;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * ReleaseEventApiApi - functional programming interface
 * @export
 */
export const ReleaseEventApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Deletes an event.
     * @param {ReleaseEventApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiDelete(
      params: ReleaseEventApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = ReleaseEventApiApiFetchParamCreator(
        configuration,
      ).releaseEventApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of albums for a specific event.
     * @param {ReleaseEventApiGetAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetAlbums(
      params: ReleaseEventApiGetAlbumsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<AlbumForApiContract>> {
      const localVarFetchArgs = ReleaseEventApiApiFetchParamCreator(
        configuration,
      ).releaseEventApiGetAlbums(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a page of events.
     * @param {ReleaseEventApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetList(
      params: ReleaseEventApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultReleaseEventForApiContract> {
      const localVarFetchArgs = ReleaseEventApiApiFetchParamCreator(
        configuration,
      ).releaseEventApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Find event names by a part of name.                            Matching is done anywhere from the name.
     * @param {ReleaseEventApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetNames(
      params: ReleaseEventApiGetNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = ReleaseEventApiApiFetchParamCreator(
        configuration,
      ).releaseEventApiGetNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {ReleaseEventApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetOne(
      params: ReleaseEventApiGetOneParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<ReleaseEventForApiContract> {
      const localVarFetchArgs = ReleaseEventApiApiFetchParamCreator(
        configuration,
      ).releaseEventApiGetOne(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of songs for a specific event.
     * @param {ReleaseEventApiGetPublishedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetPublishedSongs(
      params: ReleaseEventApiGetPublishedSongsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<SongForApiContract>> {
      const localVarFetchArgs = ReleaseEventApiApiFetchParamCreator(
        configuration,
      ).releaseEventApiGetPublishedSongs(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Creates a new report.
     * @param {ReleaseEventApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiPostReport(
      params: ReleaseEventApiPostReportParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = ReleaseEventApiApiFetchParamCreator(
        configuration,
      ).releaseEventApiPostReport(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * ReleaseEventApiApi - factory interface
 * @export
 */
export const ReleaseEventApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes an event.
     * @param {ReleaseEventApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiDelete(params: ReleaseEventApiDeleteParams, options?: any) {
      return ReleaseEventApiApiFp(configuration).releaseEventApiDelete(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of albums for a specific event.
     * @param {ReleaseEventApiGetAlbumsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetAlbums(
      params: ReleaseEventApiGetAlbumsParams,
      options?: any,
    ) {
      return ReleaseEventApiApiFp(configuration).releaseEventApiGetAlbums(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a page of events.
     * @param {ReleaseEventApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetList(
      params: ReleaseEventApiGetListParams = {},
      options?: any,
    ) {
      return ReleaseEventApiApiFp(configuration).releaseEventApiGetList(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Find event names by a part of name.                            Matching is done anywhere from the name.
     * @param {ReleaseEventApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetNames(
      params: ReleaseEventApiGetNamesParams = {},
      options?: any,
    ) {
      return ReleaseEventApiApiFp(configuration).releaseEventApiGetNames(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {ReleaseEventApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetOne(params: ReleaseEventApiGetOneParams, options?: any) {
      return ReleaseEventApiApiFp(configuration).releaseEventApiGetOne(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of songs for a specific event.
     * @param {ReleaseEventApiGetPublishedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiGetPublishedSongs(
      params: ReleaseEventApiGetPublishedSongsParams,
      options?: any,
    ) {
      return ReleaseEventApiApiFp(
        configuration,
      ).releaseEventApiGetPublishedSongs(params, options)(fetch, basePath);
    },
    /**
     *
     * @summary Creates a new report.
     * @param {ReleaseEventApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventApiPostReport(
      params: ReleaseEventApiPostReportParams,
      options?: any,
    ) {
      return ReleaseEventApiApiFp(configuration).releaseEventApiPostReport(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useReleaseEventApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => ReleaseEventApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface ReleaseEventSeriesApiDeleteParams {
  id: number;
  notes?: string;
  hardDelete?: boolean;
}

export interface ReleaseEventSeriesApiGetListParams {
  query?: string;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Description'
    | 'Events'
    | 'MainPicture'
    | 'Names'
    | 'WebLinks';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface ReleaseEventSeriesApiGetOneParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Description'
    | 'Events'
    | 'MainPicture'
    | 'Names'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

/**
 * ReleaseEventSeriesApiApi - fetch parameter creator
 * @export
 */
export const ReleaseEventSeriesApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes an event series.
     * @param {ReleaseEventSeriesApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiDelete(
      params: ReleaseEventSeriesApiDeleteParams,
      options: any = {},
    ): FetchArgs {
      const { id, notes, hardDelete } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling releaseEventSeriesApiDelete.',
        );
      }
      const localVarPath = `/api/releaseEventSeries/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (hardDelete !== undefined) {
        localVarQueryParameter['hardDelete'] = hardDelete;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a page of event series.
     * @param {ReleaseEventSeriesApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiGetList(
      params: ReleaseEventSeriesApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        fields,
        start,
        maxResults,
        getTotalCount,
        nameMatchMode,
        lang,
      } = params;
      const localVarPath = `/api/releaseEventSeries`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets single event series by ID.
     * @param {ReleaseEventSeriesApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiGetOne(
      params: ReleaseEventSeriesApiGetOneParams,
      options: any = {},
    ): FetchArgs {
      const { id, fields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling releaseEventSeriesApiGetOne.',
        );
      }
      const localVarPath = `/api/releaseEventSeries/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * ReleaseEventSeriesApiApi - functional programming interface
 * @export
 */
export const ReleaseEventSeriesApiApiFp = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes an event series.
     * @param {ReleaseEventSeriesApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiDelete(
      params: ReleaseEventSeriesApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = ReleaseEventSeriesApiApiFetchParamCreator(
        configuration,
      ).releaseEventSeriesApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a page of event series.
     * @param {ReleaseEventSeriesApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiGetList(
      params: ReleaseEventSeriesApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultReleaseEventSeriesForApiContract> {
      const localVarFetchArgs = ReleaseEventSeriesApiApiFetchParamCreator(
        configuration,
      ).releaseEventSeriesApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets single event series by ID.
     * @param {ReleaseEventSeriesApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiGetOne(
      params: ReleaseEventSeriesApiGetOneParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<ReleaseEventSeriesForApiContract> {
      const localVarFetchArgs = ReleaseEventSeriesApiApiFetchParamCreator(
        configuration,
      ).releaseEventSeriesApiGetOne(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * ReleaseEventSeriesApiApi - factory interface
 * @export
 */
export const ReleaseEventSeriesApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes an event series.
     * @param {ReleaseEventSeriesApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiDelete(
      params: ReleaseEventSeriesApiDeleteParams,
      options?: any,
    ) {
      return ReleaseEventSeriesApiApiFp(
        configuration,
      ).releaseEventSeriesApiDelete(params, options)(fetch, basePath);
    },
    /**
     *
     * @summary Gets a page of event series.
     * @param {ReleaseEventSeriesApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiGetList(
      params: ReleaseEventSeriesApiGetListParams = {},
      options?: any,
    ) {
      return ReleaseEventSeriesApiApiFp(
        configuration,
      ).releaseEventSeriesApiGetList(params, options)(fetch, basePath);
    },
    /**
     *
     * @summary Gets single event series by ID.
     * @param {ReleaseEventSeriesApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    releaseEventSeriesApiGetOne(
      params: ReleaseEventSeriesApiGetOneParams,
      options?: any,
    ) {
      return ReleaseEventSeriesApiApiFp(
        configuration,
      ).releaseEventSeriesApiGetOne(params, options)(fetch, basePath);
    },
  };
};

export const useReleaseEventSeriesApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => ReleaseEventSeriesApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface ResourcesApiGetListParams {
  cultureCode: string;
  setNames: Array<string>;
}

/**
 * ResourcesApiApi - fetch parameter creator
 * @export
 */
export const ResourcesApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Gets a number of resource sets for a specific culture.
     * @param {ResourcesApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    resourcesApiGetList(
      params: ResourcesApiGetListParams,
      options: any = {},
    ): FetchArgs {
      const { cultureCode, setNames } = params;
      // verify required parameter 'cultureCode' is not null or undefined
      if (cultureCode === null || cultureCode === undefined) {
        throw new RequiredError(
          'cultureCode',
          'Required parameter cultureCode was null or undefined when calling resourcesApiGetList.',
        );
      }
      // verify required parameter 'setNames' is not null or undefined
      if (setNames === null || setNames === undefined) {
        throw new RequiredError(
          'setNames',
          'Required parameter setNames was null or undefined when calling resourcesApiGetList.',
        );
      }
      const localVarPath = `/api/resources/{cultureCode}`.replace(
        `{${'cultureCode'}}`,
        encodeURIComponent(String(cultureCode)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (setNames) {
        localVarQueryParameter['setNames'] = setNames;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * ResourcesApiApi - functional programming interface
 * @export
 */
export const ResourcesApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Gets a number of resource sets for a specific culture.
     * @param {ResourcesApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    resourcesApiGetList(
      params: ResourcesApiGetListParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<{ [key: string]: { [key: string]: string } }> {
      const localVarFetchArgs = ResourcesApiApiFetchParamCreator(
        configuration,
      ).resourcesApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * ResourcesApiApi - factory interface
 * @export
 */
export const ResourcesApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Gets a number of resource sets for a specific culture.
     * @param {ResourcesApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    resourcesApiGetList(params: ResourcesApiGetListParams, options?: any) {
      return ResourcesApiApiFp(configuration).resourcesApiGetList(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useResourcesApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => ResourcesApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface SongApiDeleteParams {
  id: number;
  notes?: string;
}

export interface SongApiDeleteCommentParams {
  commentId: number;
}

export interface SongApiGetByIdParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongApiGetByPVParams {
  pvService:
    | 'NicoNicoDouga'
    | 'Youtube'
    | 'SoundCloud'
    | 'Vimeo'
    | 'Piapro'
    | 'Bilibili'
    | 'File'
    | 'LocalFile'
    | 'Creofuga'
    | 'Bandcamp';
  pvId: string;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongApiGetCommentsParams {
  id: number;
}

export interface SongApiGetDerivedParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongApiGetHighlightedSongsParams {
  languagePreference?: 'Default' | 'Japanese' | 'Romaji' | 'English';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
}

export interface SongApiGetListParams {
  query?: string;
  songTypes?: string;
  afterDate?: Date;
  beforeDate?: Date;
  tagName?: Array<string>;
  tagId?: Array<number>;
  childTags?: boolean;
  unifyTypesAndTags?: boolean;
  artistId?: Array<number>;
  artistParticipationStatus?:
    | 'Everything'
    | 'OnlyMainAlbums'
    | 'OnlyCollaborations';
  childVoicebanks?: boolean;
  includeMembers?: boolean;
  onlyWithPvs?: boolean;
  pvServices?:
    | 'Nothing'
    | 'NicoNicoDouga'
    | 'Youtube'
    | 'SoundCloud'
    | 'Vimeo'
    | 'Piapro'
    | 'Bilibili'
    | 'File'
    | 'LocalFile'
    | 'Creofuga'
    | 'Bandcamp';
  since?: number;
  minScore?: number;
  userCollectionId?: number;
  releaseEventId?: number;
  parentSongId?: number;
  status?: 'Draft' | 'Finished' | 'Approved' | 'Locked';
  advancedFilters?: Array<any>;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?:
    | 'None'
    | 'Name'
    | 'AdditionDate'
    | 'PublishDate'
    | 'FavoritedTimes'
    | 'RatingScore'
    | 'TagUsageCount';
  preferAccurateMatches?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongApiGetLyricsParams {
  lyricsId: number;
}

export interface SongApiGetNamesParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  maxResults?: number;
}

export interface SongApiGetRatingsParams {
  id: number;
  userFields: 'None' | 'KnownLanguages' | 'MainPicture' | 'OldUsernames';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongApiGetRelatedParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongApiGetTopSongsParams {
  durationHours?: number;
  startDate?: Date;
  filterBy?: 'CreateDate' | 'PublishDate' | 'Popularity';
  vocalist?: 'Nothing' | 'Vocaloid' | 'UTAU' | 'CeVIO';
  maxResults?: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  languagePreference?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongApiPostEditCommentParams {
  commentId: number;
  contract: CommentForApiContract;
}

export interface SongApiPostNewCommentParams {
  id: number;
  contract: CommentForApiContract;
}

export interface SongApiPostRatingParams {
  id: number;
  rating: SongRatingContract;
}

/**
 * SongApiApi - fetch parameter creator
 * @export
 */
export const SongApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes a song.
     * @param {SongApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiDelete(params: SongApiDeleteParams, options: any = {}): FetchArgs {
      const { id, notes } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiDelete.',
        );
      }
      const localVarPath = `/api/songs/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {SongApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiDeleteComment(
      params: SongApiDeleteCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling songApiDeleteComment.',
        );
      }
      const localVarPath = `/api/songs/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a song by Id.
     * @param {SongApiGetByIdParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetById(params: SongApiGetByIdParams, options: any = {}): FetchArgs {
      const { id, fields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiGetById.',
        );
      }
      const localVarPath = `/api/songs/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a song by PV.
     * @param {SongApiGetByPVParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetByPV(params: SongApiGetByPVParams, options: any = {}): FetchArgs {
      const { pvService, pvId, fields, lang } = params;
      // verify required parameter 'pvService' is not null or undefined
      if (pvService === null || pvService === undefined) {
        throw new RequiredError(
          'pvService',
          'Required parameter pvService was null or undefined when calling songApiGetByPV.',
        );
      }
      // verify required parameter 'pvId' is not null or undefined
      if (pvId === null || pvId === undefined) {
        throw new RequiredError(
          'pvId',
          'Required parameter pvId was null or undefined when calling songApiGetByPV.',
        );
      }
      const localVarPath = `/api/songs/byPv`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (pvService !== undefined) {
        localVarQueryParameter['pvService'] = pvService;
      }

      if (pvId !== undefined) {
        localVarQueryParameter['pvId'] = pvId;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for a song.
     * @param {SongApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetComments(
      params: SongApiGetCommentsParams,
      options: any = {},
    ): FetchArgs {
      const { id } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiGetComments.',
        );
      }
      const localVarPath = `/api/songs/{id}/comments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets derived (alternate versions) of a song.
     * @param {SongApiGetDerivedParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetDerived(
      params: SongApiGetDerivedParams,
      options: any = {},
    ): FetchArgs {
      const { id, fields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiGetDerived.',
        );
      }
      const localVarPath = `/api/songs/{id}/derived`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of highlighted songs, same as front page.
     * @param {SongApiGetHighlightedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetHighlightedSongs(
      params: SongApiGetHighlightedSongsParams = {},
      options: any = {},
    ): FetchArgs {
      const { languagePreference, fields } = params;
      const localVarPath = `/api/songs/highlighted`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (languagePreference !== undefined) {
        localVarQueryParameter['languagePreference'] = languagePreference;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Find songs.
     * @param {SongApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetList(
      params: SongApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        songTypes,
        afterDate,
        beforeDate,
        tagName,
        tagId,
        childTags,
        unifyTypesAndTags,
        artistId,
        artistParticipationStatus,
        childVoicebanks,
        includeMembers,
        onlyWithPvs,
        pvServices,
        since,
        minScore,
        userCollectionId,
        releaseEventId,
        parentSongId,
        status,
        advancedFilters,
        start,
        maxResults,
        getTotalCount,
        sort,
        preferAccurateMatches,
        nameMatchMode,
        fields,
        lang,
      } = params;
      const localVarPath = `/api/songs`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (songTypes !== undefined) {
        localVarQueryParameter['songTypes'] = songTypes;
      }

      if (afterDate !== undefined) {
        localVarQueryParameter['afterDate'] = (afterDate as any).toISOString();
      }

      if (beforeDate !== undefined) {
        localVarQueryParameter[
          'beforeDate'
        ] = (beforeDate as any).toISOString();
      }

      if (tagName) {
        localVarQueryParameter['tagName'] = tagName;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (childTags !== undefined) {
        localVarQueryParameter['childTags'] = childTags;
      }

      if (unifyTypesAndTags !== undefined) {
        localVarQueryParameter['unifyTypesAndTags'] = unifyTypesAndTags;
      }

      if (artistId) {
        localVarQueryParameter['artistId'] = artistId;
      }

      if (artistParticipationStatus !== undefined) {
        localVarQueryParameter[
          'artistParticipationStatus'
        ] = artistParticipationStatus;
      }

      if (childVoicebanks !== undefined) {
        localVarQueryParameter['childVoicebanks'] = childVoicebanks;
      }

      if (includeMembers !== undefined) {
        localVarQueryParameter['includeMembers'] = includeMembers;
      }

      if (onlyWithPvs !== undefined) {
        localVarQueryParameter['onlyWithPvs'] = onlyWithPvs;
      }

      if (pvServices !== undefined) {
        localVarQueryParameter['pvServices'] = pvServices;
      }

      if (since !== undefined) {
        localVarQueryParameter['since'] = since;
      }

      if (minScore !== undefined) {
        localVarQueryParameter['minScore'] = minScore;
      }

      if (userCollectionId !== undefined) {
        localVarQueryParameter['userCollectionId'] = userCollectionId;
      }

      if (releaseEventId !== undefined) {
        localVarQueryParameter['releaseEventId'] = releaseEventId;
      }

      if (parentSongId !== undefined) {
        localVarQueryParameter['parentSongId'] = parentSongId;
      }

      if (status !== undefined) {
        localVarQueryParameter['status'] = status;
      }

      if (advancedFilters) {
        localVarQueryParameter['advancedFilters'] = advancedFilters;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (preferAccurateMatches !== undefined) {
        localVarQueryParameter['preferAccurateMatches'] = preferAccurateMatches;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Output is cached. Specify song version as parameter to refresh.
     * @summary Gets lyrics by ID.
     * @param {SongApiGetLyricsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetLyrics(
      params: SongApiGetLyricsParams,
      options: any = {},
    ): FetchArgs {
      const { lyricsId } = params;
      // verify required parameter 'lyricsId' is not null or undefined
      if (lyricsId === null || lyricsId === undefined) {
        throw new RequiredError(
          'lyricsId',
          'Required parameter lyricsId was null or undefined when calling songApiGetLyrics.',
        );
      }
      const localVarPath = `/api/songs/lyrics/{lyricsId}`.replace(
        `{${'lyricsId'}}`,
        encodeURIComponent(String(lyricsId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of song names. Ideal for autocomplete boxes.
     * @param {SongApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetNames(
      params: SongApiGetNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, nameMatchMode, maxResults } = params;
      const localVarPath = `/api/songs/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * The result includes ratings and user information.              For users who have requested not to make their ratings public, the user will be empty.
     * @summary Get ratings for a song.
     * @param {SongApiGetRatingsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetRatings(
      params: SongApiGetRatingsParams,
      options: any = {},
    ): FetchArgs {
      const { id, userFields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiGetRatings.',
        );
      }
      // verify required parameter 'userFields' is not null or undefined
      if (userFields === null || userFields === undefined) {
        throw new RequiredError(
          'userFields',
          'Required parameter userFields was null or undefined when calling songApiGetRatings.',
        );
      }
      const localVarPath = `/api/songs/{id}/ratings`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (userFields !== undefined) {
        localVarQueryParameter['userFields'] = userFields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets related songs.
     * @param {SongApiGetRelatedParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetRelated(
      params: SongApiGetRelatedParams,
      options: any = {},
    ): FetchArgs {
      const { id, fields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiGetRelated.',
        );
      }
      const localVarPath = `/api/songs/{id}/related`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets top rated songs.
     * @param {SongApiGetTopSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetTopSongs(
      params: SongApiGetTopSongsParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        durationHours,
        startDate,
        filterBy,
        vocalist,
        maxResults,
        fields,
        languagePreference,
      } = params;
      const localVarPath = `/api/songs/top-rated`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (durationHours !== undefined) {
        localVarQueryParameter['durationHours'] = durationHours;
      }

      if (startDate !== undefined) {
        localVarQueryParameter['startDate'] = (startDate as any).toISOString();
      }

      if (filterBy !== undefined) {
        localVarQueryParameter['filterBy'] = filterBy;
      }

      if (vocalist !== undefined) {
        localVarQueryParameter['vocalist'] = vocalist;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (languagePreference !== undefined) {
        localVarQueryParameter['languagePreference'] = languagePreference;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {SongApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostEditComment(
      params: SongApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId, contract } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling songApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling songApiPostEditComment.',
        );
      }
      const localVarPath = `/api/songs/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {SongApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostNewComment(
      params: SongApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { id, contract } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling songApiPostNewComment.',
        );
      }
      const localVarPath = `/api/songs/{id}/comments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * If the user has already rated the song, any previous rating is replaced.              Authorization cookie must be included.              This API supports CORS.
     * @summary Add or update rating for a specific song, for the currently logged in user.
     * @param {SongApiPostRatingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostRating(
      params: SongApiPostRatingParams,
      options: any = {},
    ): FetchArgs {
      const { id, rating } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songApiPostRating.',
        );
      }
      // verify required parameter 'rating' is not null or undefined
      if (rating === null || rating === undefined) {
        throw new RequiredError(
          'rating',
          'Required parameter rating was null or undefined when calling songApiPostRating.',
        );
      }
      const localVarPath = `/api/songs/{id}/ratings`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'SongRatingContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(rating || {})
        : rating || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * SongApiApi - functional programming interface
 * @export
 */
export const SongApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Deletes a song.
     * @param {SongApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiDelete(
      params: SongApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {SongApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiDeleteComment(
      params: SongApiDeleteCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiDeleteComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a song by Id.
     * @param {SongApiGetByIdParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetById(
      params: SongApiGetByIdParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<SongForApiContract> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetById(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a song by PV.
     * @param {SongApiGetByPVParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetByPV(
      params: SongApiGetByPVParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<SongForApiContract> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetByPV(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for a song.
     * @param {SongApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetComments(
      params: SongApiGetCommentsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<CommentForApiContract>> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetComments(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets derived (alternate versions) of a song.
     * @param {SongApiGetDerivedParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetDerived(
      params: SongApiGetDerivedParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<SongForApiContract>> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetDerived(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of highlighted songs, same as front page.
     * @param {SongApiGetHighlightedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetHighlightedSongs(
      params: SongApiGetHighlightedSongsParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<SongForApiContract>> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetHighlightedSongs(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Find songs.
     * @param {SongApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetList(
      params: SongApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultSongForApiContract> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Output is cached. Specify song version as parameter to refresh.
     * @summary Gets lyrics by ID.
     * @param {SongApiGetLyricsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetLyrics(
      params: SongApiGetLyricsParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<LyricsForSongContract> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetLyrics(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of song names. Ideal for autocomplete boxes.
     * @param {SongApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetNames(
      params: SongApiGetNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * The result includes ratings and user information.              For users who have requested not to make their ratings public, the user will be empty.
     * @summary Get ratings for a song.
     * @param {SongApiGetRatingsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetRatings(
      params: SongApiGetRatingsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<RatedSongForUserForApiContract>> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetRatings(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets related songs.
     * @param {SongApiGetRelatedParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetRelated(
      params: SongApiGetRelatedParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<RelatedSongsContract> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetRelated(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets top rated songs.
     * @param {SongApiGetTopSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetTopSongs(
      params: SongApiGetTopSongsParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<SongForApiContract>> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiGetTopSongs(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {SongApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostEditComment(
      params: SongApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {SongApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostNewComment(
      params: SongApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * If the user has already rated the song, any previous rating is replaced.              Authorization cookie must be included.              This API supports CORS.
     * @summary Add or update rating for a specific song, for the currently logged in user.
     * @param {SongApiPostRatingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostRating(
      params: SongApiPostRatingParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = SongApiApiFetchParamCreator(
        configuration,
      ).songApiPostRating(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * SongApiApi - factory interface
 * @export
 */
export const SongApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes a song.
     * @param {SongApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiDelete(params: SongApiDeleteParams, options?: any) {
      return SongApiApiFp(configuration).songApiDelete(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {SongApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiDeleteComment(params: SongApiDeleteCommentParams, options?: any) {
      return SongApiApiFp(configuration).songApiDeleteComment(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a song by Id.
     * @param {SongApiGetByIdParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetById(params: SongApiGetByIdParams, options?: any) {
      return SongApiApiFp(configuration).songApiGetById(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a song by PV.
     * @param {SongApiGetByPVParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetByPV(params: SongApiGetByPVParams, options?: any) {
      return SongApiApiFp(configuration).songApiGetByPV(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets a list of comments for a song.
     * @param {SongApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetComments(params: SongApiGetCommentsParams, options?: any) {
      return SongApiApiFp(configuration).songApiGetComments(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Pagination and sorting might be added later.
     * @summary Gets derived (alternate versions) of a song.
     * @param {SongApiGetDerivedParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetDerived(params: SongApiGetDerivedParams, options?: any) {
      return SongApiApiFp(configuration).songApiGetDerived(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Output is cached for 1 hour.
     * @summary Gets list of highlighted songs, same as front page.
     * @param {SongApiGetHighlightedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetHighlightedSongs(
      params: SongApiGetHighlightedSongsParams = {},
      options?: any,
    ) {
      return SongApiApiFp(configuration).songApiGetHighlightedSongs(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Find songs.
     * @param {SongApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetList(params: SongApiGetListParams = {}, options?: any) {
      return SongApiApiFp(configuration).songApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Output is cached. Specify song version as parameter to refresh.
     * @summary Gets lyrics by ID.
     * @param {SongApiGetLyricsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetLyrics(params: SongApiGetLyricsParams, options?: any) {
      return SongApiApiFp(configuration).songApiGetLyrics(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of song names. Ideal for autocomplete boxes.
     * @param {SongApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetNames(params: SongApiGetNamesParams = {}, options?: any) {
      return SongApiApiFp(configuration).songApiGetNames(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * The result includes ratings and user information.              For users who have requested not to make their ratings public, the user will be empty.
     * @summary Get ratings for a song.
     * @param {SongApiGetRatingsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetRatings(params: SongApiGetRatingsParams, options?: any) {
      return SongApiApiFp(configuration).songApiGetRatings(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets related songs.
     * @param {SongApiGetRelatedParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetRelated(params: SongApiGetRelatedParams, options?: any) {
      return SongApiApiFp(configuration).songApiGetRelated(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets top rated songs.
     * @param {SongApiGetTopSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiGetTopSongs(params: SongApiGetTopSongsParams = {}, options?: any) {
      return SongApiApiFp(configuration).songApiGetTopSongs(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {SongApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostEditComment(
      params: SongApiPostEditCommentParams,
      options?: any,
    ) {
      return SongApiApiFp(configuration).songApiPostEditComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {SongApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostNewComment(params: SongApiPostNewCommentParams, options?: any) {
      return SongApiApiFp(configuration).songApiPostNewComment(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * If the user has already rated the song, any previous rating is replaced.              Authorization cookie must be included.              This API supports CORS.
     * @summary Add or update rating for a specific song, for the currently logged in user.
     * @param {SongApiPostRatingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songApiPostRating(params: SongApiPostRatingParams, options?: any) {
      return SongApiApiFp(configuration).songApiPostRating(params, options)(
        fetch,
        basePath,
      );
    },
  };
};

export const useSongApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => SongApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface SongListApiDeleteParams {
  id: number;
  notes?: string;
  hardDelete?: boolean;
}

export interface SongListApiDeleteCommentParams {
  commentId: number;
}

export interface SongListApiGetCommentsParams {
  listId: number;
}

export interface SongListApiGetFeaturedListNamesParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  featuredCategory?:
    | 'Nothing'
    | 'Concerts'
    | 'VocaloidRanking'
    | 'Pools'
    | 'Other';
  maxResults?: number;
}

export interface SongListApiGetFeaturedListsParams {
  query?: string;
  tagId?: Array<number>;
  childTags?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  featuredCategory?:
    | 'Nothing'
    | 'Concerts'
    | 'VocaloidRanking'
    | 'Pools'
    | 'Other';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?: 'None' | 'Name' | 'Date' | 'CreateDate';
  fields?: 'None' | 'Description' | 'Events' | 'MainPicture' | 'Tags';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongListApiGetSongsParams {
  listId: number;
  query?: string;
  songTypes?: string;
  pvServices?:
    | 'Nothing'
    | 'NicoNicoDouga'
    | 'Youtube'
    | 'SoundCloud'
    | 'Vimeo'
    | 'Piapro'
    | 'Bilibili'
    | 'File'
    | 'LocalFile'
    | 'Creofuga'
    | 'Bandcamp';
  tagId?: Array<number>;
  artistId?: Array<number>;
  childVoicebanks?: boolean;
  advancedFilters?: Array<any>;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?:
    | 'None'
    | 'Name'
    | 'AdditionDate'
    | 'PublishDate'
    | 'FavoritedTimes'
    | 'RatingScore'
    | 'TagUsageCount';
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface SongListApiPostParams {
  list: SongListForEditContract;
}

export interface SongListApiPostEditCommentParams {
  commentId: number;
  contract: CommentForApiContract;
}

export interface SongListApiPostNewCommentParams {
  listId: number;
  contract: CommentForApiContract;
}

/**
 * SongListApiApi - fetch parameter creator
 * @export
 */
export const SongListApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes a song list.
     * @param {SongListApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiDelete(
      params: SongListApiDeleteParams,
      options: any = {},
    ): FetchArgs {
      const { id, notes, hardDelete } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling songListApiDelete.',
        );
      }
      const localVarPath = `/api/songLists/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (hardDelete !== undefined) {
        localVarQueryParameter['hardDelete'] = hardDelete;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {SongListApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiDeleteComment(
      params: SongListApiDeleteCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling songListApiDeleteComment.',
        );
      }
      const localVarPath = `/api/songLists/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of comments for a song list.
     * @param {SongListApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetComments(
      params: SongListApiGetCommentsParams,
      options: any = {},
    ): FetchArgs {
      const { listId } = params;
      // verify required parameter 'listId' is not null or undefined
      if (listId === null || listId === undefined) {
        throw new RequiredError(
          'listId',
          'Required parameter listId was null or undefined when calling songListApiGetComments.',
        );
      }
      const localVarPath = `/api/songLists/{listId}/comments`.replace(
        `{${'listId'}}`,
        encodeURIComponent(String(listId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of featuedd list names. Ideal for autocomplete boxes.
     * @param {SongListApiGetFeaturedListNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetFeaturedListNames(
      params: SongListApiGetFeaturedListNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, nameMatchMode, featuredCategory, maxResults } = params;
      const localVarPath = `/api/songLists/featured/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (featuredCategory !== undefined) {
        localVarQueryParameter['featuredCategory'] = featuredCategory;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of featured song lists.
     * @param {SongListApiGetFeaturedListsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetFeaturedLists(
      params: SongListApiGetFeaturedListsParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        tagId,
        childTags,
        nameMatchMode,
        featuredCategory,
        start,
        maxResults,
        getTotalCount,
        sort,
        fields,
        lang,
      } = params;
      const localVarPath = `/api/songLists/featured`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (childTags !== undefined) {
        localVarQueryParameter['childTags'] = childTags;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (featuredCategory !== undefined) {
        localVarQueryParameter['featuredCategory'] = featuredCategory;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of songs in a song list.
     * @param {SongListApiGetSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetSongs(
      params: SongListApiGetSongsParams,
      options: any = {},
    ): FetchArgs {
      const {
        listId,
        query,
        songTypes,
        pvServices,
        tagId,
        artistId,
        childVoicebanks,
        advancedFilters,
        start,
        maxResults,
        getTotalCount,
        sort,
        nameMatchMode,
        fields,
        lang,
      } = params;
      // verify required parameter 'listId' is not null or undefined
      if (listId === null || listId === undefined) {
        throw new RequiredError(
          'listId',
          'Required parameter listId was null or undefined when calling songListApiGetSongs.',
        );
      }
      const localVarPath = `/api/songLists/{listId}/songs`.replace(
        `{${'listId'}}`,
        encodeURIComponent(String(listId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (songTypes !== undefined) {
        localVarQueryParameter['songTypes'] = songTypes;
      }

      if (pvServices !== undefined) {
        localVarQueryParameter['pvServices'] = pvServices;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (artistId) {
        localVarQueryParameter['artistId'] = artistId;
      }

      if (childVoicebanks !== undefined) {
        localVarQueryParameter['childVoicebanks'] = childVoicebanks;
      }

      if (advancedFilters) {
        localVarQueryParameter['advancedFilters'] = advancedFilters;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Creates a song list.
     * @param {SongListApiPostParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPost(
      params: SongListApiPostParams,
      options: any = {},
    ): FetchArgs {
      const { list } = params;
      // verify required parameter 'list' is not null or undefined
      if (list === null || list === undefined) {
        throw new RequiredError(
          'list',
          'Required parameter list was null or undefined when calling songListApiPost.',
        );
      }
      const localVarPath = `/api/songLists`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'SongListForEditContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(list || {})
        : list || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {SongListApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPostEditComment(
      params: SongListApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId, contract } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling songListApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling songListApiPostEditComment.',
        );
      }
      const localVarPath = `/api/songLists/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {SongListApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPostNewComment(
      params: SongListApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { listId, contract } = params;
      // verify required parameter 'listId' is not null or undefined
      if (listId === null || listId === undefined) {
        throw new RequiredError(
          'listId',
          'Required parameter listId was null or undefined when calling songListApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling songListApiPostNewComment.',
        );
      }
      const localVarPath = `/api/songLists/{listId}/comments`.replace(
        `{${'listId'}}`,
        encodeURIComponent(String(listId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * SongListApiApi - functional programming interface
 * @export
 */
export const SongListApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Deletes a song list.
     * @param {SongListApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiDelete(
      params: SongListApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {SongListApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiDeleteComment(
      params: SongListApiDeleteCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiDeleteComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of comments for a song list.
     * @param {SongListApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetComments(
      params: SongListApiGetCommentsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultCommentForApiContract> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiGetComments(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of featuedd list names. Ideal for autocomplete boxes.
     * @param {SongListApiGetFeaturedListNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetFeaturedListNames(
      params: SongListApiGetFeaturedListNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiGetFeaturedListNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of featured song lists.
     * @param {SongListApiGetFeaturedListsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetFeaturedLists(
      params: SongListApiGetFeaturedListsParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultSongListForApiContract> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiGetFeaturedLists(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of songs in a song list.
     * @param {SongListApiGetSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetSongs(
      params: SongListApiGetSongsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultSongInListForApiContract> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiGetSongs(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Creates a song list.
     * @param {SongListApiPostParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPost(
      params: SongListApiPostParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<number> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiPost(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {SongListApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPostEditComment(
      params: SongListApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {SongListApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPostNewComment(
      params: SongListApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = SongListApiApiFetchParamCreator(
        configuration,
      ).songListApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * SongListApiApi - factory interface
 * @export
 */
export const SongListApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes a song list.
     * @param {SongListApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiDelete(params: SongListApiDeleteParams, options?: any) {
      return SongListApiApiFp(configuration).songListApiDelete(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {SongListApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiDeleteComment(
      params: SongListApiDeleteCommentParams,
      options?: any,
    ) {
      return SongListApiApiFp(configuration).songListApiDeleteComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of comments for a song list.
     * @param {SongListApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetComments(
      params: SongListApiGetCommentsParams,
      options?: any,
    ) {
      return SongListApiApiFp(configuration).songListApiGetComments(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of featuedd list names. Ideal for autocomplete boxes.
     * @param {SongListApiGetFeaturedListNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetFeaturedListNames(
      params: SongListApiGetFeaturedListNamesParams = {},
      options?: any,
    ) {
      return SongListApiApiFp(configuration).songListApiGetFeaturedListNames(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of featured song lists.
     * @param {SongListApiGetFeaturedListsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetFeaturedLists(
      params: SongListApiGetFeaturedListsParams = {},
      options?: any,
    ) {
      return SongListApiApiFp(configuration).songListApiGetFeaturedLists(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of songs in a song list.
     * @param {SongListApiGetSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiGetSongs(params: SongListApiGetSongsParams, options?: any) {
      return SongListApiApiFp(configuration).songListApiGetSongs(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Creates a song list.
     * @param {SongListApiPostParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPost(params: SongListApiPostParams, options?: any) {
      return SongListApiApiFp(configuration).songListApiPost(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {SongListApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPostEditComment(
      params: SongListApiPostEditCommentParams,
      options?: any,
    ) {
      return SongListApiApiFp(configuration).songListApiPostEditComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {SongListApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    songListApiPostNewComment(
      params: SongListApiPostNewCommentParams,
      options?: any,
    ) {
      return SongListApiApiFp(configuration).songListApiPostNewComment(
        params,
        options,
      )(fetch, basePath);
    },
  };
};

export const useSongListApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => SongListApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface TagApiDeleteParams {
  id: number;
  notes?: string;
  hardDelete?: boolean;
}

export interface TagApiDeleteCommentParams {
  commentId: number;
}

export interface TagApiGetByIdParams {
  id: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'AliasedTo'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Parent'
    | 'RelatedTags'
    | 'TranslatedDescription'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface TagApiGetByNameParams {
  name: string;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'AliasedTo'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Parent'
    | 'RelatedTags'
    | 'TranslatedDescription'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface TagApiGetCategoryNamesListParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
}

export interface TagApiGetChildTagsParams {
  tagId: number;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'AliasedTo'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Parent'
    | 'RelatedTags'
    | 'TranslatedDescription'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface TagApiGetCommentsParams {
  tagId: number;
}

export interface TagApiGetListParams {
  query?: string;
  allowChildren?: boolean;
  categoryName?: string;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  sort?: 'Nothing' | 'Name' | 'AdditionDate' | 'UsageCount';
  preferAccurateMatches?: boolean;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'AliasedTo'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Parent'
    | 'RelatedTags'
    | 'TranslatedDescription'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
  target?:
    | 'Nothing'
    | 'Album'
    | 'Artist'
    | 'AlbumArtist'
    | 'Event'
    | 'Song'
    | 'AlbumSong'
    | 'ArtistSong'
    | 'All';
}

export interface TagApiGetNamesParams {
  query?: string;
  allowAliases?: boolean;
  maxResults?: number;
}

export interface TagApiGetTopTagsParams {
  categoryName?: string;
  entryType?:
    | 'Undefined'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  maxResults?: number;
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface TagApiPostEditCommentParams {
  commentId: number;
  contract: CommentForApiContract;
}

export interface TagApiPostNewCommentParams {
  tagId: number;
  contract: CommentForApiContract;
}

export interface TagApiPostNewTagParams {
  name: string;
}

export interface TagApiPostReportParams {
  tagId: number;
  reportType: 'InvalidInfo' | 'Duplicate' | 'Inappropriate' | 'Other';
  notes: string;
  versionNumber: number;
}

/**
 * TagApiApi - fetch parameter creator
 * @export
 */
export const TagApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes a tag.
     * @param {TagApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiDelete(params: TagApiDeleteParams, options: any = {}): FetchArgs {
      const { id, notes, hardDelete } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling tagApiDelete.',
        );
      }
      const localVarPath = `/api/tags/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (hardDelete !== undefined) {
        localVarQueryParameter['hardDelete'] = hardDelete;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Deletes a comment.              Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @param {TagApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiDeleteComment(
      params: TagApiDeleteCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling tagApiDeleteComment.',
        );
      }
      const localVarPath = `/api/tags/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a tag by ID.
     * @param {TagApiGetByIdParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetById(params: TagApiGetByIdParams, options: any = {}): FetchArgs {
      const { id, fields, lang } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling tagApiGetById.',
        );
      }
      const localVarPath = `/api/tags/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary DEPRECATED. Gets a tag by name.
     * @param {TagApiGetByNameParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetByName(
      params: TagApiGetByNameParams,
      options: any = {},
    ): FetchArgs {
      const { name, fields, lang } = params;
      // verify required parameter 'name' is not null or undefined
      if (name === null || name === undefined) {
        throw new RequiredError(
          'name',
          'Required parameter name was null or undefined when calling tagApiGetByName.',
        );
      }
      const localVarPath = `/api/tags/byName/{name}`.replace(
        `{${'name'}}`,
        encodeURIComponent(String(name)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of tag category names.
     * @param {TagApiGetCategoryNamesListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetCategoryNamesList(
      params: TagApiGetCategoryNamesListParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, nameMatchMode } = params;
      const localVarPath = `/api/tags/categoryNames`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of child tags for a tag.              Only direct children will be included.
     * @param {TagApiGetChildTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetChildTags(
      params: TagApiGetChildTagsParams,
      options: any = {},
    ): FetchArgs {
      const { tagId, fields, lang } = params;
      // verify required parameter 'tagId' is not null or undefined
      if (tagId === null || tagId === undefined) {
        throw new RequiredError(
          'tagId',
          'Required parameter tagId was null or undefined when calling tagApiGetChildTags.',
        );
      }
      const localVarPath = `/api/tags/{tagId}/children`.replace(
        `{${'tagId'}}`,
        encodeURIComponent(String(tagId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of comments for a tag.              Note: pagination and sorting might be added later.
     * @param {TagApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetComments(
      params: TagApiGetCommentsParams,
      options: any = {},
    ): FetchArgs {
      const { tagId } = params;
      // verify required parameter 'tagId' is not null or undefined
      if (tagId === null || tagId === undefined) {
        throw new RequiredError(
          'tagId',
          'Required parameter tagId was null or undefined when calling tagApiGetComments.',
        );
      }
      const localVarPath = `/api/tags/{tagId}/comments`.replace(
        `{${'tagId'}}`,
        encodeURIComponent(String(tagId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Find tags.
     * @param {TagApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetList(
      params: TagApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        allowChildren,
        categoryName,
        start,
        maxResults,
        getTotalCount,
        nameMatchMode,
        sort,
        preferAccurateMatches,
        fields,
        lang,
        target,
      } = params;
      const localVarPath = `/api/tags`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (allowChildren !== undefined) {
        localVarQueryParameter['allowChildren'] = allowChildren;
      }

      if (categoryName !== undefined) {
        localVarQueryParameter['categoryName'] = categoryName;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (preferAccurateMatches !== undefined) {
        localVarQueryParameter['preferAccurateMatches'] = preferAccurateMatches;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      if (target !== undefined) {
        localVarQueryParameter['target'] = target;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Find tag names by a part of name.                            Matching is done anywhere from the name.
     * @param {TagApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetNames(
      params: TagApiGetNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, allowAliases, maxResults } = params;
      const localVarPath = `/api/tags/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (allowAliases !== undefined) {
        localVarQueryParameter['allowAliases'] = allowAliases;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets the most common tags in a category.
     * @param {TagApiGetTopTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetTopTags(
      params: TagApiGetTopTagsParams = {},
      options: any = {},
    ): FetchArgs {
      const { categoryName, entryType, maxResults, lang } = params;
      const localVarPath = `/api/tags/top`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (categoryName !== undefined) {
        localVarQueryParameter['categoryName'] = categoryName;
      }

      if (entryType !== undefined) {
        localVarQueryParameter['entryType'] = entryType;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Updates a comment.              Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @param {TagApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostEditComment(
      params: TagApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId, contract } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling tagApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling tagApiPostEditComment.',
        );
      }
      const localVarPath = `/api/tags/comments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {TagApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostNewComment(
      params: TagApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { tagId, contract } = params;
      // verify required parameter 'tagId' is not null or undefined
      if (tagId === null || tagId === undefined) {
        throw new RequiredError(
          'tagId',
          'Required parameter tagId was null or undefined when calling tagApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling tagApiPostNewComment.',
        );
      }
      const localVarPath = `/api/tags/{tagId}/comments`.replace(
        `{${'tagId'}}`,
        encodeURIComponent(String(tagId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Creates a new tag.
     * @param {TagApiPostNewTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostNewTag(
      params: TagApiPostNewTagParams,
      options: any = {},
    ): FetchArgs {
      const { name } = params;
      // verify required parameter 'name' is not null or undefined
      if (name === null || name === undefined) {
        throw new RequiredError(
          'name',
          'Required parameter name was null or undefined when calling tagApiPostNewTag.',
        );
      }
      const localVarPath = `/api/tags`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (name !== undefined) {
        localVarQueryParameter['name'] = name;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Creates a new report.
     * @param {TagApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostReport(
      params: TagApiPostReportParams,
      options: any = {},
    ): FetchArgs {
      const { tagId, reportType, notes, versionNumber } = params;
      // verify required parameter 'tagId' is not null or undefined
      if (tagId === null || tagId === undefined) {
        throw new RequiredError(
          'tagId',
          'Required parameter tagId was null or undefined when calling tagApiPostReport.',
        );
      }
      // verify required parameter 'reportType' is not null or undefined
      if (reportType === null || reportType === undefined) {
        throw new RequiredError(
          'reportType',
          'Required parameter reportType was null or undefined when calling tagApiPostReport.',
        );
      }
      // verify required parameter 'notes' is not null or undefined
      if (notes === null || notes === undefined) {
        throw new RequiredError(
          'notes',
          'Required parameter notes was null or undefined when calling tagApiPostReport.',
        );
      }
      // verify required parameter 'versionNumber' is not null or undefined
      if (versionNumber === null || versionNumber === undefined) {
        throw new RequiredError(
          'versionNumber',
          'Required parameter versionNumber was null or undefined when calling tagApiPostReport.',
        );
      }
      const localVarPath = `/api/tags/{tagId}/reports`.replace(
        `{${'tagId'}}`,
        encodeURIComponent(String(tagId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (reportType !== undefined) {
        localVarQueryParameter['reportType'] = reportType;
      }

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (versionNumber !== undefined) {
        localVarQueryParameter['versionNumber'] = versionNumber;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * TagApiApi - functional programming interface
 * @export
 */
export const TagApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Deletes a tag.
     * @param {TagApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiDelete(
      params: TagApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Deletes a comment.              Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @param {TagApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiDeleteComment(
      params: TagApiDeleteCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiDeleteComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a tag by ID.
     * @param {TagApiGetByIdParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetById(
      params: TagApiGetByIdParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<TagForApiContract> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetById(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary DEPRECATED. Gets a tag by name.
     * @param {TagApiGetByNameParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetByName(
      params: TagApiGetByNameParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<TagForApiContract> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetByName(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of tag category names.
     * @param {TagApiGetCategoryNamesListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetCategoryNamesList(
      params: TagApiGetCategoryNamesListParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetCategoryNamesList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of child tags for a tag.              Only direct children will be included.
     * @param {TagApiGetChildTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetChildTags(
      params: TagApiGetChildTagsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<TagForApiContract>> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetChildTags(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of comments for a tag.              Note: pagination and sorting might be added later.
     * @param {TagApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetComments(
      params: TagApiGetCommentsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultCommentForApiContract> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetComments(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Find tags.
     * @param {TagApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetList(
      params: TagApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultTagForApiContract> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Find tag names by a part of name.                            Matching is done anywhere from the name.
     * @param {TagApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetNames(
      params: TagApiGetNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets the most common tags in a category.
     * @param {TagApiGetTopTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetTopTags(
      params: TagApiGetTopTagsParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<TagBaseContract>> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiGetTopTags(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Updates a comment.              Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @param {TagApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostEditComment(
      params: TagApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {TagApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostNewComment(
      params: TagApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Creates a new tag.
     * @param {TagApiPostNewTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostNewTag(
      params: TagApiPostNewTagParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<TagBaseContract> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiPostNewTag(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Creates a new report.
     * @param {TagApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostReport(
      params: TagApiPostReportParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = TagApiApiFetchParamCreator(
        configuration,
      ).tagApiPostReport(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * TagApiApi - factory interface
 * @export
 */
export const TagApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes a tag.
     * @param {TagApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiDelete(params: TagApiDeleteParams, options?: any) {
      return TagApiApiFp(configuration).tagApiDelete(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Deletes a comment.              Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @param {TagApiDeleteCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiDeleteComment(params: TagApiDeleteCommentParams, options?: any) {
      return TagApiApiFp(configuration).tagApiDeleteComment(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a tag by ID.
     * @param {TagApiGetByIdParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetById(params: TagApiGetByIdParams, options?: any) {
      return TagApiApiFp(configuration).tagApiGetById(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary DEPRECATED. Gets a tag by name.
     * @param {TagApiGetByNameParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetByName(params: TagApiGetByNameParams, options?: any) {
      return TagApiApiFp(configuration).tagApiGetByName(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of tag category names.
     * @param {TagApiGetCategoryNamesListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetCategoryNamesList(
      params: TagApiGetCategoryNamesListParams = {},
      options?: any,
    ) {
      return TagApiApiFp(configuration).tagApiGetCategoryNamesList(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of child tags for a tag.              Only direct children will be included.
     * @param {TagApiGetChildTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetChildTags(params: TagApiGetChildTagsParams, options?: any) {
      return TagApiApiFp(configuration).tagApiGetChildTags(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of comments for a tag.              Note: pagination and sorting might be added later.
     * @param {TagApiGetCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetComments(params: TagApiGetCommentsParams, options?: any) {
      return TagApiApiFp(configuration).tagApiGetComments(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Find tags.
     * @param {TagApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetList(params: TagApiGetListParams = {}, options?: any) {
      return TagApiApiFp(configuration).tagApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Find tag names by a part of name.                            Matching is done anywhere from the name.
     * @param {TagApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetNames(params: TagApiGetNamesParams = {}, options?: any) {
      return TagApiApiFp(configuration).tagApiGetNames(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets the most common tags in a category.
     * @param {TagApiGetTopTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiGetTopTags(params: TagApiGetTopTagsParams = {}, options?: any) {
      return TagApiApiFp(configuration).tagApiGetTopTags(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Updates a comment.              Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @param {TagApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostEditComment(params: TagApiPostEditCommentParams, options?: any) {
      return TagApiApiFp(configuration).tagApiPostEditComment(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {TagApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostNewComment(params: TagApiPostNewCommentParams, options?: any) {
      return TagApiApiFp(configuration).tagApiPostNewComment(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Creates a new tag.
     * @param {TagApiPostNewTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostNewTag(params: TagApiPostNewTagParams, options?: any) {
      return TagApiApiFp(configuration).tagApiPostNewTag(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Creates a new report.
     * @param {TagApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    tagApiPostReport(params: TagApiPostReportParams, options?: any) {
      return TagApiApiFp(configuration).tagApiPostReport(params, options)(
        fetch,
        basePath,
      );
    },
  };
};

export const useTagApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => TagApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface UserApiDeleteFollowedTagParams {
  tagId: number;
}

export interface UserApiDeleteMessagesParams {
  id: number;
  messageId: Array<number>;
}

export interface UserApiDeleteProfileCommentParams {
  commentId: number;
}

export interface UserApiGetAlbumCollectionParams {
  id: number;
  query?: string;
  tagId?: number;
  tag?: string;
  artistId?: number;
  purchaseStatuses?: 'Nothing' | 'Wishlisted' | 'Ordered' | 'Owned' | 'All';
  releaseEventId?: number;
  albumTypes?:
    | 'Unknown'
    | 'Album'
    | 'Single'
    | 'EP'
    | 'SplitAlbum'
    | 'Compilation'
    | 'Video'
    | 'Artbook'
    | 'Game'
    | 'Fanmade'
    | 'Instrumental'
    | 'Other';
  advancedFilters?: Array<any>;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?:
    | 'None'
    | 'Name'
    | 'ReleaseDate'
    | 'ReleaseDateWithNulls'
    | 'AdditionDate'
    | 'RatingAverage'
    | 'RatingTotal'
    | 'NameThenReleaseDate'
    | 'CollectionCount';
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Artists'
    | 'Description'
    | 'Discs'
    | 'Identifiers'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'Tracks'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface UserApiGetCurrentParams {
  fields?: 'None' | 'KnownLanguages' | 'MainPicture' | 'OldUsernames';
}

export interface UserApiGetEventsParams {
  id: number;
  relationshipType: 'Interested' | 'Attending';
}

export interface UserApiGetFollowedArtistsParams {
  id: number;
  query?: string;
  tagId?: Array<number>;
  artistType?:
    | 'Unknown'
    | 'Circle'
    | 'Label'
    | 'Producer'
    | 'Animator'
    | 'Illustrator'
    | 'Lyricist'
    | 'Vocaloid'
    | 'UTAU'
    | 'CeVIO'
    | 'OtherVoiceSynthesizer'
    | 'OtherVocalist'
    | 'OtherGroup'
    | 'OtherIndividual'
    | 'Utaite'
    | 'Band'
    | 'Vocalist'
    | 'Character';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?:
    | 'None'
    | 'Name'
    | 'AdditionDate'
    | 'AdditionDateAsc'
    | 'ReleaseDate'
    | 'SongCount'
    | 'SongRating'
    | 'FollowerCount';
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'ArtistLinks'
    | 'ArtistLinksReverse'
    | 'BaseVoicebank'
    | 'Description'
    | 'MainPicture'
    | 'Names'
    | 'Tags'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface UserApiGetListParams {
  query?: string;
  groups?:
    | 'Nothing'
    | 'Limited'
    | 'Regular'
    | 'Trusted'
    | 'Moderator'
    | 'Admin';
  joinDateAfter?: Date;
  joinDateBefore?: Date;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?: 'RegisterDate' | 'Name' | 'Group';
  includeDisabled?: boolean;
  onlyVerified?: boolean;
  knowsLanguage?: string;
  fields?: 'None' | 'KnownLanguages' | 'MainPicture' | 'OldUsernames';
}

export interface UserApiGetMessageParams {
  messageId: number;
}

export interface UserApiGetMessagesParams {
  id: number;
  inbox?: 'Nothing' | 'Received' | 'Sent' | 'Notifications';
  unread?: boolean;
  anotherUserId?: number;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
}

export interface UserApiGetNamesParams {
  query?: string;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  maxResults?: number;
  includeDisabled?: boolean;
}

export interface UserApiGetOneParams {
  id: number;
  fields?: 'None' | 'KnownLanguages' | 'MainPicture' | 'OldUsernames';
}

export interface UserApiGetProfileCommentsParams {
  id: number;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
}

export interface UserApiGetRatedSongsParams {
  id: number;
  query?: string;
  tagName?: string;
  tagId?: Array<number>;
  artistId?: Array<number>;
  childVoicebanks?: boolean;
  artistGrouping?: 'And' | 'Or';
  rating?: 'Nothing' | 'Dislike' | 'Like' | 'Favorite';
  songListId?: number;
  groupByRating?: boolean;
  pvServices?:
    | 'Nothing'
    | 'NicoNicoDouga'
    | 'Youtube'
    | 'SoundCloud'
    | 'Vimeo'
    | 'Piapro'
    | 'Bilibili'
    | 'File'
    | 'LocalFile'
    | 'Creofuga'
    | 'Bandcamp';
  advancedFilters?: Array<any>;
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?:
    | 'None'
    | 'Name'
    | 'AdditionDate'
    | 'PublishDate'
    | 'FavoritedTimes'
    | 'RatingScore'
    | 'RatingDate';
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Albums'
    | 'Artists'
    | 'Lyrics'
    | 'MainPicture'
    | 'Names'
    | 'PVs'
    | 'ReleaseEvent'
    | 'Tags'
    | 'ThumbUrl'
    | 'WebLinks';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
}

export interface UserApiGetSongListsParams {
  id: number;
  query?: string;
  tagId?: Array<number>;
  childTags?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  sort?: 'None' | 'Name' | 'Date' | 'CreateDate';
  fields?: 'None' | 'Description' | 'Events' | 'MainPicture' | 'Tags';
}

export interface UserApiGetSongRatingParams {
  id: number;
  songId: number;
}

export interface UserApiGetSongRatingForCurrentParams {
  songId: number;
}

export interface UserApiPostAlbumStatusParams {
  albumId: number;
  collectionStatus: 'Nothing' | 'Wishlisted' | 'Ordered' | 'Owned';
  mediaType: 'PhysicalDisc' | 'DigitalDownload' | 'Other';
  rating: number;
}

export interface UserApiPostEditCommentParams {
  commentId: number;
  contract: CommentForApiContract;
}

export interface UserApiPostFollowedTagParams {
  tagId: number;
}

export interface UserApiPostNewCommentParams {
  id: number;
  contract: CommentForApiContract;
}

export interface UserApiPostNewMessageParams {
  id: number;
  contract: UserMessageContract;
}

export interface UserApiPostRefreshEntryEditParams {
  entryType:
    | 'Undefined'
    | 'Album'
    | 'Artist'
    | 'DiscussionTopic'
    | 'PV'
    | 'ReleaseEvent'
    | 'ReleaseEventSeries'
    | 'Song'
    | 'SongList'
    | 'Tag'
    | 'User'
    | 'Venue';
  entryId: number;
}

export interface UserApiPostReportParams {
  id: number;
  model: CreateReportModel;
}

export interface UserApiPostSettingParams {
  id: number;
  settingName: string;
  settingValue: string;
}

export interface UserApiPostSongTagsParams {
  songId: number;
  tags: Array<TagBaseContract>;
}

/**
 * UserApiApi - fetch parameter creator
 * @export
 */
export const UserApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @param {UserApiDeleteFollowedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteFollowedTag(
      params: UserApiDeleteFollowedTagParams,
      options: any = {},
    ): FetchArgs {
      const { tagId } = params;
      // verify required parameter 'tagId' is not null or undefined
      if (tagId === null || tagId === undefined) {
        throw new RequiredError(
          'tagId',
          'Required parameter tagId was null or undefined when calling userApiDeleteFollowedTag.',
        );
      }
      const localVarPath = `/api/users/current/followedTags/{tagId}`.replace(
        `{${'tagId'}}`,
        encodeURIComponent(String(tagId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Deletes a list of user messages.
     * @param {UserApiDeleteMessagesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteMessages(
      params: UserApiDeleteMessagesParams,
      options: any = {},
    ): FetchArgs {
      const { id, messageId } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiDeleteMessages.',
        );
      }
      // verify required parameter 'messageId' is not null or undefined
      if (messageId === null || messageId === undefined) {
        throw new RequiredError(
          'messageId',
          'Required parameter messageId was null or undefined when calling userApiDeleteMessages.',
        );
      }
      const localVarPath = `/api/users/{id}/messages`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (messageId) {
        localVarQueryParameter['messageId'] = messageId;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {UserApiDeleteProfileCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteProfileComment(
      params: UserApiDeleteProfileCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling userApiDeleteProfileComment.',
        );
      }
      const localVarPath = `/api/users/profileComments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * This includes albums that have been rated by the user as well as albums that the user has bought or wishlisted.              Note that the user might have set his album ownership status and media type as private, in which case those properties are not included.
     * @summary Gets a list of albums in a user's collection.
     * @param {UserApiGetAlbumCollectionParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetAlbumCollection(
      params: UserApiGetAlbumCollectionParams,
      options: any = {},
    ): FetchArgs {
      const {
        id,
        query,
        tagId,
        tag,
        artistId,
        purchaseStatuses,
        releaseEventId,
        albumTypes,
        advancedFilters,
        start,
        maxResults,
        getTotalCount,
        sort,
        nameMatchMode,
        fields,
        lang,
      } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetAlbumCollection.',
        );
      }
      const localVarPath = `/api/users/{id}/albums`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (tagId !== undefined) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (tag !== undefined) {
        localVarQueryParameter['tag'] = tag;
      }

      if (artistId !== undefined) {
        localVarQueryParameter['artistId'] = artistId;
      }

      if (purchaseStatuses !== undefined) {
        localVarQueryParameter['purchaseStatuses'] = purchaseStatuses;
      }

      if (releaseEventId !== undefined) {
        localVarQueryParameter['releaseEventId'] = releaseEventId;
      }

      if (albumTypes !== undefined) {
        localVarQueryParameter['albumTypes'] = albumTypes;
      }

      if (advancedFilters) {
        localVarQueryParameter['advancedFilters'] = advancedFilters;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Requires login information.              This API supports CORS, and is restricted to specific origins.
     * @summary Gets information about the currently logged in user.
     * @param {UserApiGetCurrentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetCurrent(
      params: UserApiGetCurrentParams = {},
      options: any = {},
    ): FetchArgs {
      const { fields } = params;
      const localVarPath = `/api/users/current`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of events a user has subscribed to.
     * @param {UserApiGetEventsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetEvents(
      params: UserApiGetEventsParams,
      options: any = {},
    ): FetchArgs {
      const { id, relationshipType } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetEvents.',
        );
      }
      // verify required parameter 'relationshipType' is not null or undefined
      if (relationshipType === null || relationshipType === undefined) {
        throw new RequiredError(
          'relationshipType',
          'Required parameter relationshipType was null or undefined when calling userApiGetEvents.',
        );
      }
      const localVarPath = `/api/users/{id}/events`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (relationshipType !== undefined) {
        localVarQueryParameter['relationshipType'] = relationshipType;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of artists followed by a user.
     * @param {UserApiGetFollowedArtistsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetFollowedArtists(
      params: UserApiGetFollowedArtistsParams,
      options: any = {},
    ): FetchArgs {
      const {
        id,
        query,
        tagId,
        artistType,
        start,
        maxResults,
        getTotalCount,
        sort,
        nameMatchMode,
        fields,
        lang,
      } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetFollowedArtists.',
        );
      }
      const localVarPath = `/api/users/{id}/followedArtists`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (artistType !== undefined) {
        localVarQueryParameter['artistType'] = artistType;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of users.
     * @param {UserApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetList(
      params: UserApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        groups,
        joinDateAfter,
        joinDateBefore,
        nameMatchMode,
        start,
        maxResults,
        getTotalCount,
        sort,
        includeDisabled,
        onlyVerified,
        knowsLanguage,
        fields,
      } = params;
      const localVarPath = `/api/users`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (groups !== undefined) {
        localVarQueryParameter['groups'] = groups;
      }

      if (joinDateAfter !== undefined) {
        localVarQueryParameter[
          'joinDateAfter'
        ] = (joinDateAfter as any).toISOString();
      }

      if (joinDateBefore !== undefined) {
        localVarQueryParameter[
          'joinDateBefore'
        ] = (joinDateBefore as any).toISOString();
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (includeDisabled !== undefined) {
        localVarQueryParameter['includeDisabled'] = includeDisabled;
      }

      if (onlyVerified !== undefined) {
        localVarQueryParameter['onlyVerified'] = onlyVerified;
      }

      if (knowsLanguage !== undefined) {
        localVarQueryParameter['knowsLanguage'] = knowsLanguage;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * The message will be marked as read.              User can only load messages from their own inbox.
     * @summary Gets a user message.
     * @param {UserApiGetMessageParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetMessage(
      params: UserApiGetMessageParams,
      options: any = {},
    ): FetchArgs {
      const { messageId } = params;
      // verify required parameter 'messageId' is not null or undefined
      if (messageId === null || messageId === undefined) {
        throw new RequiredError(
          'messageId',
          'Required parameter messageId was null or undefined when calling userApiGetMessage.',
        );
      }
      const localVarPath = `/api/users/messages/{messageId}`.replace(
        `{${'messageId'}}`,
        encodeURIComponent(String(messageId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of messages.
     * @param {UserApiGetMessagesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetMessages(
      params: UserApiGetMessagesParams,
      options: any = {},
    ): FetchArgs {
      const {
        id,
        inbox,
        unread,
        anotherUserId,
        start,
        maxResults,
        getTotalCount,
      } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetMessages.',
        );
      }
      const localVarPath = `/api/users/{id}/messages`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (inbox !== undefined) {
        localVarQueryParameter['inbox'] = inbox;
      }

      if (unread !== undefined) {
        localVarQueryParameter['unread'] = unread;
      }

      if (anotherUserId !== undefined) {
        localVarQueryParameter['anotherUserId'] = anotherUserId;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of user names. Ideal for autocomplete boxes.
     * @param {UserApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetNames(
      params: UserApiGetNamesParams = {},
      options: any = {},
    ): FetchArgs {
      const { query, nameMatchMode, maxResults, includeDisabled } = params;
      const localVarPath = `/api/users/names`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (includeDisabled !== undefined) {
        localVarQueryParameter['includeDisabled'] = includeDisabled;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets user by ID.
     * @param {UserApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetOne(params: UserApiGetOneParams, options: any = {}): FetchArgs {
      const { id, fields } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetOne.',
        );
      }
      const localVarPath = `/api/users/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of comments posted on user's profile.
     * @param {UserApiGetProfileCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetProfileComments(
      params: UserApiGetProfileCommentsParams,
      options: any = {},
    ): FetchArgs {
      const { id, start, maxResults, getTotalCount } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetProfileComments.',
        );
      }
      const localVarPath = `/api/users/{id}/profileComments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a list of songs rated by a user.
     * @param {UserApiGetRatedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetRatedSongs(
      params: UserApiGetRatedSongsParams,
      options: any = {},
    ): FetchArgs {
      const {
        id,
        query,
        tagName,
        tagId,
        artistId,
        childVoicebanks,
        artistGrouping,
        rating,
        songListId,
        groupByRating,
        pvServices,
        advancedFilters,
        start,
        maxResults,
        getTotalCount,
        sort,
        nameMatchMode,
        fields,
        lang,
      } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetRatedSongs.',
        );
      }
      const localVarPath = `/api/users/{id}/ratedSongs`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (tagName !== undefined) {
        localVarQueryParameter['tagName'] = tagName;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (artistId) {
        localVarQueryParameter['artistId'] = artistId;
      }

      if (childVoicebanks !== undefined) {
        localVarQueryParameter['childVoicebanks'] = childVoicebanks;
      }

      if (artistGrouping !== undefined) {
        localVarQueryParameter['artistGrouping'] = artistGrouping;
      }

      if (rating !== undefined) {
        localVarQueryParameter['rating'] = rating;
      }

      if (songListId !== undefined) {
        localVarQueryParameter['songListId'] = songListId;
      }

      if (groupByRating !== undefined) {
        localVarQueryParameter['groupByRating'] = groupByRating;
      }

      if (pvServices !== undefined) {
        localVarQueryParameter['pvServices'] = pvServices;
      }

      if (advancedFilters) {
        localVarQueryParameter['advancedFilters'] = advancedFilters;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {UserApiGetSongListsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongLists(
      params: UserApiGetSongListsParams,
      options: any = {},
    ): FetchArgs {
      const {
        id,
        query,
        tagId,
        childTags,
        nameMatchMode,
        start,
        maxResults,
        getTotalCount,
        sort,
        fields,
      } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetSongLists.',
        );
      }
      const localVarPath = `/api/users/{id}/songLists`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (tagId) {
        localVarQueryParameter['tagId'] = tagId;
      }

      if (childTags !== undefined) {
        localVarQueryParameter['childTags'] = childTags;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (sort !== undefined) {
        localVarQueryParameter['sort'] = sort;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a specific user's rating for a song.
     * @param {UserApiGetSongRatingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongRating(
      params: UserApiGetSongRatingParams,
      options: any = {},
    ): FetchArgs {
      const { id, songId } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiGetSongRating.',
        );
      }
      // verify required parameter 'songId' is not null or undefined
      if (songId === null || songId === undefined) {
        throw new RequiredError(
          'songId',
          'Required parameter songId was null or undefined when calling userApiGetSongRating.',
        );
      }
      const localVarPath = `/api/users/{id}/ratedSongs/{songId}`
        .replace(`{${'id'}}`, encodeURIComponent(String(id)))
        .replace(`{${'songId'}}`, encodeURIComponent(String(songId)));
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Requires authentication.
     * @summary Gets currently logged in user's rating for a song.
     * @param {UserApiGetSongRatingForCurrentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongRatingForCurrent(
      params: UserApiGetSongRatingForCurrentParams,
      options: any = {},
    ): FetchArgs {
      const { songId } = params;
      // verify required parameter 'songId' is not null or undefined
      if (songId === null || songId === undefined) {
        throw new RequiredError(
          'songId',
          'Required parameter songId was null or undefined when calling userApiGetSongRatingForCurrent.',
        );
      }
      const localVarPath = `/api/users/current/ratedSongs/{songId}`.replace(
        `{${'songId'}}`,
        encodeURIComponent(String(songId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * If the user has already rated the album, any previous rating is replaced.              Authorization cookie must be included.
     * @summary Add or update collection status, media type and rating for a specific album, for the currently logged in user.
     * @param {UserApiPostAlbumStatusParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostAlbumStatus(
      params: UserApiPostAlbumStatusParams,
      options: any = {},
    ): FetchArgs {
      const { albumId, collectionStatus, mediaType, rating } = params;
      // verify required parameter 'albumId' is not null or undefined
      if (albumId === null || albumId === undefined) {
        throw new RequiredError(
          'albumId',
          'Required parameter albumId was null or undefined when calling userApiPostAlbumStatus.',
        );
      }
      // verify required parameter 'collectionStatus' is not null or undefined
      if (collectionStatus === null || collectionStatus === undefined) {
        throw new RequiredError(
          'collectionStatus',
          'Required parameter collectionStatus was null or undefined when calling userApiPostAlbumStatus.',
        );
      }
      // verify required parameter 'mediaType' is not null or undefined
      if (mediaType === null || mediaType === undefined) {
        throw new RequiredError(
          'mediaType',
          'Required parameter mediaType was null or undefined when calling userApiPostAlbumStatus.',
        );
      }
      // verify required parameter 'rating' is not null or undefined
      if (rating === null || rating === undefined) {
        throw new RequiredError(
          'rating',
          'Required parameter rating was null or undefined when calling userApiPostAlbumStatus.',
        );
      }
      const localVarPath = `/api/users/current/albums/{albumId}`.replace(
        `{${'albumId'}}`,
        encodeURIComponent(String(albumId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (collectionStatus !== undefined) {
        localVarQueryParameter['collectionStatus'] = collectionStatus;
      }

      if (mediaType !== undefined) {
        localVarQueryParameter['mediaType'] = mediaType;
      }

      if (rating !== undefined) {
        localVarQueryParameter['rating'] = rating;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {UserApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostEditComment(
      params: UserApiPostEditCommentParams,
      options: any = {},
    ): FetchArgs {
      const { commentId, contract } = params;
      // verify required parameter 'commentId' is not null or undefined
      if (commentId === null || commentId === undefined) {
        throw new RequiredError(
          'commentId',
          'Required parameter commentId was null or undefined when calling userApiPostEditComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling userApiPostEditComment.',
        );
      }
      const localVarPath = `/api/users/profileComments/{commentId}`.replace(
        `{${'commentId'}}`,
        encodeURIComponent(String(commentId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {UserApiPostFollowedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostFollowedTag(
      params: UserApiPostFollowedTagParams,
      options: any = {},
    ): FetchArgs {
      const { tagId } = params;
      // verify required parameter 'tagId' is not null or undefined
      if (tagId === null || tagId === undefined) {
        throw new RequiredError(
          'tagId',
          'Required parameter tagId was null or undefined when calling userApiPostFollowedTag.',
        );
      }
      const localVarPath = `/api/users/current/followedTags/{tagId}`.replace(
        `{${'tagId'}}`,
        encodeURIComponent(String(tagId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {UserApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostNewComment(
      params: UserApiPostNewCommentParams,
      options: any = {},
    ): FetchArgs {
      const { id, contract } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiPostNewComment.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling userApiPostNewComment.',
        );
      }
      const localVarPath = `/api/users/{id}/profileComments`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CommentForApiContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Creates a new message.
     * @param {UserApiPostNewMessageParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostNewMessage(
      params: UserApiPostNewMessageParams,
      options: any = {},
    ): FetchArgs {
      const { id, contract } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiPostNewMessage.',
        );
      }
      // verify required parameter 'contract' is not null or undefined
      if (contract === null || contract === undefined) {
        throw new RequiredError(
          'contract',
          'Required parameter contract was null or undefined when calling userApiPostNewMessage.',
        );
      }
      const localVarPath = `/api/users/{id}/messages`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'UserMessageContract' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(contract || {})
        : contract || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Refresh entry edit status, indicating that the current user is still editing that entry.
     * @param {UserApiPostRefreshEntryEditParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostRefreshEntryEdit(
      params: UserApiPostRefreshEntryEditParams,
      options: any = {},
    ): FetchArgs {
      const { entryType, entryId } = params;
      // verify required parameter 'entryType' is not null or undefined
      if (entryType === null || entryType === undefined) {
        throw new RequiredError(
          'entryType',
          'Required parameter entryType was null or undefined when calling userApiPostRefreshEntryEdit.',
        );
      }
      // verify required parameter 'entryId' is not null or undefined
      if (entryId === null || entryId === undefined) {
        throw new RequiredError(
          'entryId',
          'Required parameter entryId was null or undefined when calling userApiPostRefreshEntryEdit.',
        );
      }
      const localVarPath = `/api/users/current/refreshEntryEdit`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (entryType !== undefined) {
        localVarQueryParameter['entryType'] = entryType;
      }

      if (entryId !== undefined) {
        localVarQueryParameter['entryId'] = entryId;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @param {UserApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostReport(
      params: UserApiPostReportParams,
      options: any = {},
    ): FetchArgs {
      const { id, model } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiPostReport.',
        );
      }
      // verify required parameter 'model' is not null or undefined
      if (model === null || model === undefined) {
        throw new RequiredError(
          'model',
          'Required parameter model was null or undefined when calling userApiPostReport.',
        );
      }
      const localVarPath = `/api/users/{id}/reports`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'CreateReportModel' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(model || {})
        : model || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Updates user setting.
     * @param {UserApiPostSettingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostSetting(
      params: UserApiPostSettingParams,
      options: any = {},
    ): FetchArgs {
      const { id, settingName, settingValue } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling userApiPostSetting.',
        );
      }
      // verify required parameter 'settingName' is not null or undefined
      if (settingName === null || settingName === undefined) {
        throw new RequiredError(
          'settingName',
          'Required parameter settingName was null or undefined when calling userApiPostSetting.',
        );
      }
      // verify required parameter 'settingValue' is not null or undefined
      if (settingValue === null || settingValue === undefined) {
        throw new RequiredError(
          'settingValue',
          'Required parameter settingValue was null or undefined when calling userApiPostSetting.',
        );
      }
      const localVarPath = `/api/users/{id}/settings/{settingName}`
        .replace(`{${'id'}}`, encodeURIComponent(String(id)))
        .replace(`{${'settingName'}}`, encodeURIComponent(String(settingName)));
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'string' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(settingValue || {})
        : settingValue || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     * This can only be used to add tags - existing tags will not be removed.               Nothing will be done for tags that are already applied by the current user for the song.              Authorization cookie is required.
     * @summary Appends tags for a song, by the currently logged in user.
     * @param {UserApiPostSongTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostSongTags(
      params: UserApiPostSongTagsParams,
      options: any = {},
    ): FetchArgs {
      const { songId, tags } = params;
      // verify required parameter 'songId' is not null or undefined
      if (songId === null || songId === undefined) {
        throw new RequiredError(
          'songId',
          'Required parameter songId was null or undefined when calling userApiPostSongTags.',
        );
      }
      // verify required parameter 'tags' is not null or undefined
      if (tags === null || tags === undefined) {
        throw new RequiredError(
          'tags',
          'Required parameter tags was null or undefined when calling userApiPostSongTags.',
        );
      }
      const localVarPath = `/api/users/current/songTags/{songId}`.replace(
        `{${'songId'}}`,
        encodeURIComponent(String(songId)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );
      const needsSerialization =
        <any>'Array&lt;TagBaseContract&gt;' !== 'string' ||
        localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.body = needsSerialization
        ? JSON.stringify(tags || {})
        : tags || '';

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * UserApiApi - functional programming interface
 * @export
 */
export const UserApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @param {UserApiDeleteFollowedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteFollowedTag(
      params: UserApiDeleteFollowedTagParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiDeleteFollowedTag(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Deletes a list of user messages.
     * @param {UserApiDeleteMessagesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteMessages(
      params: UserApiDeleteMessagesParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiDeleteMessages(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {UserApiDeleteProfileCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteProfileComment(
      params: UserApiDeleteProfileCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiDeleteProfileComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * This includes albums that have been rated by the user as well as albums that the user has bought or wishlisted.              Note that the user might have set his album ownership status and media type as private, in which case those properties are not included.
     * @summary Gets a list of albums in a user's collection.
     * @param {UserApiGetAlbumCollectionParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetAlbumCollection(
      params: UserApiGetAlbumCollectionParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultAlbumForUserForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetAlbumCollection(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Requires login information.              This API supports CORS, and is restricted to specific origins.
     * @summary Gets information about the currently logged in user.
     * @param {UserApiGetCurrentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetCurrent(
      params: UserApiGetCurrentParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<UserForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetCurrent(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of events a user has subscribed to.
     * @param {UserApiGetEventsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetEvents(
      params: UserApiGetEventsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<Array<ReleaseEventForApiContract>> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetEvents(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of artists followed by a user.
     * @param {UserApiGetFollowedArtistsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetFollowedArtists(
      params: UserApiGetFollowedArtistsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultArtistForUserForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetFollowedArtists(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of users.
     * @param {UserApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetList(
      params: UserApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultUserForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * The message will be marked as read.              User can only load messages from their own inbox.
     * @summary Gets a user message.
     * @param {UserApiGetMessageParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetMessage(
      params: UserApiGetMessageParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<UserMessageContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetMessage(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of messages.
     * @param {UserApiGetMessagesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetMessages(
      params: UserApiGetMessagesParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultUserMessageContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetMessages(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of user names. Ideal for autocomplete boxes.
     * @param {UserApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetNames(
      params: UserApiGetNamesParams = {},
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetNames(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets user by ID.
     * @param {UserApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetOne(
      params: UserApiGetOneParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<UserForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetOne(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of comments posted on user's profile.
     * @param {UserApiGetProfileCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetProfileComments(
      params: UserApiGetProfileCommentsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultCommentForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetProfileComments(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a list of songs rated by a user.
     * @param {UserApiGetRatedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetRatedSongs(
      params: UserApiGetRatedSongsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultRatedSongForUserForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetRatedSongs(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {UserApiGetSongListsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongLists(
      params: UserApiGetSongListsParams,
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultSongListForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetSongLists(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a specific user's rating for a song.
     * @param {UserApiGetSongRatingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongRating(
      params: UserApiGetSongRatingParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<string> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetSongRating(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Requires authentication.
     * @summary Gets currently logged in user's rating for a song.
     * @param {UserApiGetSongRatingForCurrentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongRatingForCurrent(
      params: UserApiGetSongRatingForCurrentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<string> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiGetSongRatingForCurrent(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * If the user has already rated the album, any previous rating is replaced.              Authorization cookie must be included.
     * @summary Add or update collection status, media type and rating for a specific album, for the currently logged in user.
     * @param {UserApiPostAlbumStatusParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostAlbumStatus(
      params: UserApiPostAlbumStatusParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<string> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostAlbumStatus(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {UserApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostEditComment(
      params: UserApiPostEditCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostEditComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {UserApiPostFollowedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostFollowedTag(
      params: UserApiPostFollowedTagParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostFollowedTag(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {UserApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostNewComment(
      params: UserApiPostNewCommentParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<CommentForApiContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostNewComment(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Creates a new message.
     * @param {UserApiPostNewMessageParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostNewMessage(
      params: UserApiPostNewMessageParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<UserMessageContract> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostNewMessage(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Refresh entry edit status, indicating that the current user is still editing that entry.
     * @param {UserApiPostRefreshEntryEditParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostRefreshEntryEdit(
      params: UserApiPostRefreshEntryEditParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostRefreshEntryEdit(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @param {UserApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostReport(
      params: UserApiPostReportParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<boolean> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostReport(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Updates user setting.
     * @param {UserApiPostSettingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostSetting(
      params: UserApiPostSettingParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostSetting(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     * This can only be used to add tags - existing tags will not be removed.               Nothing will be done for tags that are already applied by the current user for the song.              Authorization cookie is required.
     * @summary Appends tags for a song, by the currently logged in user.
     * @param {UserApiPostSongTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostSongTags(
      params: UserApiPostSongTagsParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = UserApiApiFetchParamCreator(
        configuration,
      ).userApiPostSongTags(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * UserApiApi - factory interface
 * @export
 */
export const UserApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @param {UserApiDeleteFollowedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteFollowedTag(
      params: UserApiDeleteFollowedTagParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiDeleteFollowedTag(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Deletes a list of user messages.
     * @param {UserApiDeleteMessagesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteMessages(params: UserApiDeleteMessagesParams, options?: any) {
      return UserApiApiFp(configuration).userApiDeleteMessages(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Normal users can delete their own comments, moderators can delete all comments.              Requires login.
     * @summary Deletes a comment.
     * @param {UserApiDeleteProfileCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiDeleteProfileComment(
      params: UserApiDeleteProfileCommentParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiDeleteProfileComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     * This includes albums that have been rated by the user as well as albums that the user has bought or wishlisted.              Note that the user might have set his album ownership status and media type as private, in which case those properties are not included.
     * @summary Gets a list of albums in a user's collection.
     * @param {UserApiGetAlbumCollectionParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetAlbumCollection(
      params: UserApiGetAlbumCollectionParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiGetAlbumCollection(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     * Requires login information.              This API supports CORS, and is restricted to specific origins.
     * @summary Gets information about the currently logged in user.
     * @param {UserApiGetCurrentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetCurrent(params: UserApiGetCurrentParams = {}, options?: any) {
      return UserApiApiFp(configuration).userApiGetCurrent(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of events a user has subscribed to.
     * @param {UserApiGetEventsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetEvents(params: UserApiGetEventsParams, options?: any) {
      return UserApiApiFp(configuration).userApiGetEvents(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of artists followed by a user.
     * @param {UserApiGetFollowedArtistsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetFollowedArtists(
      params: UserApiGetFollowedArtistsParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiGetFollowedArtists(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of users.
     * @param {UserApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetList(params: UserApiGetListParams = {}, options?: any) {
      return UserApiApiFp(configuration).userApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * The message will be marked as read.              User can only load messages from their own inbox.
     * @summary Gets a user message.
     * @param {UserApiGetMessageParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetMessage(params: UserApiGetMessageParams, options?: any) {
      return UserApiApiFp(configuration).userApiGetMessage(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of messages.
     * @param {UserApiGetMessagesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetMessages(params: UserApiGetMessagesParams, options?: any) {
      return UserApiApiFp(configuration).userApiGetMessages(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of user names. Ideal for autocomplete boxes.
     * @param {UserApiGetNamesParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetNames(params: UserApiGetNamesParams = {}, options?: any) {
      return UserApiApiFp(configuration).userApiGetNames(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets user by ID.
     * @param {UserApiGetOneParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetOne(params: UserApiGetOneParams, options?: any) {
      return UserApiApiFp(configuration).userApiGetOne(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a list of comments posted on user's profile.
     * @param {UserApiGetProfileCommentsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetProfileComments(
      params: UserApiGetProfileCommentsParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiGetProfileComments(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Gets a list of songs rated by a user.
     * @param {UserApiGetRatedSongsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetRatedSongs(params: UserApiGetRatedSongsParams, options?: any) {
      return UserApiApiFp(configuration).userApiGetRatedSongs(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @param {UserApiGetSongListsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongLists(params: UserApiGetSongListsParams, options?: any) {
      return UserApiApiFp(configuration).userApiGetSongLists(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a specific user's rating for a song.
     * @param {UserApiGetSongRatingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongRating(params: UserApiGetSongRatingParams, options?: any) {
      return UserApiApiFp(configuration).userApiGetSongRating(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * Requires authentication.
     * @summary Gets currently logged in user's rating for a song.
     * @param {UserApiGetSongRatingForCurrentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiGetSongRatingForCurrent(
      params: UserApiGetSongRatingForCurrentParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiGetSongRatingForCurrent(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     * If the user has already rated the album, any previous rating is replaced.              Authorization cookie must be included.
     * @summary Add or update collection status, media type and rating for a specific album, for the currently logged in user.
     * @param {UserApiPostAlbumStatusParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostAlbumStatus(
      params: UserApiPostAlbumStatusParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiPostAlbumStatus(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     * Normal users can edit their own comments, moderators can edit all comments.              Requires login.
     * @summary Updates a comment.
     * @param {UserApiPostEditCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostEditComment(
      params: UserApiPostEditCommentParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiPostEditComment(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {UserApiPostFollowedTagParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostFollowedTag(
      params: UserApiPostFollowedTagParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiPostFollowedTag(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @summary Posts a new comment.
     * @param {UserApiPostNewCommentParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostNewComment(params: UserApiPostNewCommentParams, options?: any) {
      return UserApiApiFp(configuration).userApiPostNewComment(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Creates a new message.
     * @param {UserApiPostNewMessageParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostNewMessage(params: UserApiPostNewMessageParams, options?: any) {
      return UserApiApiFp(configuration).userApiPostNewMessage(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Refresh entry edit status, indicating that the current user is still editing that entry.
     * @param {UserApiPostRefreshEntryEditParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostRefreshEntryEdit(
      params: UserApiPostRefreshEntryEditParams,
      options?: any,
    ) {
      return UserApiApiFp(configuration).userApiPostRefreshEntryEdit(
        params,
        options,
      )(fetch, basePath);
    },
    /**
     *
     * @param {UserApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostReport(params: UserApiPostReportParams, options?: any) {
      return UserApiApiFp(configuration).userApiPostReport(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Updates user setting.
     * @param {UserApiPostSettingParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostSetting(params: UserApiPostSettingParams, options?: any) {
      return UserApiApiFp(configuration).userApiPostSetting(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     * This can only be used to add tags - existing tags will not be removed.               Nothing will be done for tags that are already applied by the current user for the song.              Authorization cookie is required.
     * @summary Appends tags for a song, by the currently logged in user.
     * @param {UserApiPostSongTagsParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    userApiPostSongTags(params: UserApiPostSongTagsParams, options?: any) {
      return UserApiApiFp(configuration).userApiPostSongTags(params, options)(
        fetch,
        basePath,
      );
    },
  };
};

export const useUserApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => UserApiApiFactory(configuration, fetch, basePath),
    [],
  );
};

export interface VenueApiDeleteParams {
  id: number;
  notes?: string;
  hardDelete?: boolean;
}

export interface VenueApiGetListParams {
  query?: string;
  fields?:
    | 'None'
    | 'AdditionalNames'
    | 'Description'
    | 'Events'
    | 'Names'
    | 'WebLinks';
  start?: number;
  maxResults?: number;
  getTotalCount?: boolean;
  nameMatchMode?: 'Auto' | 'Partial' | 'StartsWith' | 'Exact' | 'Words';
  lang?: 'Default' | 'Japanese' | 'Romaji' | 'English';
  sortRule?: 'None' | 'Name' | 'Distance';
  latitude?: number;
  longitude?: number;
  radius?: number;
  distanceUnit?: 'Kilometers' | 'Miles';
}

export interface VenueApiPostReportParams {
  id: number;
  reportType: 'InvalidInfo' | 'Duplicate' | 'Inappropriate' | 'Other';
  notes: string;
  versionNumber: number;
}

/**
 * VenueApiApi - fetch parameter creator
 * @export
 */
export const VenueApiApiFetchParamCreator = function (
  configuration?: Configuration,
) {
  return {
    /**
     *
     * @summary Deletes a venue.
     * @param {VenueApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiDelete(params: VenueApiDeleteParams, options: any = {}): FetchArgs {
      const { id, notes, hardDelete } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling venueApiDelete.',
        );
      }
      const localVarPath = `/api/venues/{id}`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign(
        { method: 'DELETE' },
        options,
      );
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (hardDelete !== undefined) {
        localVarQueryParameter['hardDelete'] = hardDelete;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Gets a page of event venue.
     * @param {VenueApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiGetList(
      params: VenueApiGetListParams = {},
      options: any = {},
    ): FetchArgs {
      const {
        query,
        fields,
        start,
        maxResults,
        getTotalCount,
        nameMatchMode,
        lang,
        sortRule,
        latitude,
        longitude,
        radius,
        distanceUnit,
      } = params;
      const localVarPath = `/api/venues`;
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (query !== undefined) {
        localVarQueryParameter['query'] = query;
      }

      if (fields !== undefined) {
        localVarQueryParameter['fields'] = fields;
      }

      if (start !== undefined) {
        localVarQueryParameter['start'] = start;
      }

      if (maxResults !== undefined) {
        localVarQueryParameter['maxResults'] = maxResults;
      }

      if (getTotalCount !== undefined) {
        localVarQueryParameter['getTotalCount'] = getTotalCount;
      }

      if (nameMatchMode !== undefined) {
        localVarQueryParameter['nameMatchMode'] = nameMatchMode;
      }

      if (lang !== undefined) {
        localVarQueryParameter['lang'] = lang;
      }

      if (sortRule !== undefined) {
        localVarQueryParameter['sortRule'] = sortRule;
      }

      if (latitude !== undefined) {
        localVarQueryParameter['latitude'] = latitude;
      }

      if (longitude !== undefined) {
        localVarQueryParameter['longitude'] = longitude;
      }

      if (radius !== undefined) {
        localVarQueryParameter['radius'] = radius;
      }

      if (distanceUnit !== undefined) {
        localVarQueryParameter['distanceUnit'] = distanceUnit;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
    /**
     *
     * @summary Creates a new report.
     * @param {VenueApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiPostReport(
      params: VenueApiPostReportParams,
      options: any = {},
    ): FetchArgs {
      const { id, reportType, notes, versionNumber } = params;
      // verify required parameter 'id' is not null or undefined
      if (id === null || id === undefined) {
        throw new RequiredError(
          'id',
          'Required parameter id was null or undefined when calling venueApiPostReport.',
        );
      }
      // verify required parameter 'reportType' is not null or undefined
      if (reportType === null || reportType === undefined) {
        throw new RequiredError(
          'reportType',
          'Required parameter reportType was null or undefined when calling venueApiPostReport.',
        );
      }
      // verify required parameter 'notes' is not null or undefined
      if (notes === null || notes === undefined) {
        throw new RequiredError(
          'notes',
          'Required parameter notes was null or undefined when calling venueApiPostReport.',
        );
      }
      // verify required parameter 'versionNumber' is not null or undefined
      if (versionNumber === null || versionNumber === undefined) {
        throw new RequiredError(
          'versionNumber',
          'Required parameter versionNumber was null or undefined when calling venueApiPostReport.',
        );
      }
      const localVarPath = `/api/venues/{id}/reports`.replace(
        `{${'id'}}`,
        encodeURIComponent(String(id)),
      );
      const localVarUrlObj = url.parse(localVarPath, true);
      const localVarRequestOptions = Object.assign({ method: 'POST' }, options);
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;

      if (reportType !== undefined) {
        localVarQueryParameter['reportType'] = reportType;
      }

      if (notes !== undefined) {
        localVarQueryParameter['notes'] = notes;
      }

      if (versionNumber !== undefined) {
        localVarQueryParameter['versionNumber'] = versionNumber;
      }

      localVarUrlObj.query = Object.assign(
        {},
        localVarUrlObj.query,
        localVarQueryParameter,
        options.query,
      );
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = Object.assign(
        {},
        localVarHeaderParameter,
        options.headers,
      );

      return {
        url: url.format(localVarUrlObj),
        options: localVarRequestOptions,
      };
    },
  };
};

/**
 * VenueApiApi - functional programming interface
 * @export
 */
export const VenueApiApiFp = function (configuration?: Configuration) {
  return {
    /**
     *
     * @summary Deletes a venue.
     * @param {VenueApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiDelete(
      params: VenueApiDeleteParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = VenueApiApiFetchParamCreator(
        configuration,
      ).venueApiDelete(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Gets a page of event venue.
     * @param {VenueApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiGetList(
      params: VenueApiGetListParams = {},
      options?: any,
    ): (
      fetch?: FetchAPI,
      basePath?: string,
    ) => Promise<PartialFindResultVenueForApiContract> {
      const localVarFetchArgs = VenueApiApiFetchParamCreator(
        configuration,
      ).venueApiGetList(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            throw response;
          }
        });
      };
    },
    /**
     *
     * @summary Creates a new report.
     * @param {VenueApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiPostReport(
      params: VenueApiPostReportParams,
      options?: any,
    ): (fetch?: FetchAPI, basePath?: string) => Promise<Response> {
      const localVarFetchArgs = VenueApiApiFetchParamCreator(
        configuration,
      ).venueApiPostReport(params, options);
      return (
        fetch: FetchAPI = isomorphicFetch,
        basePath: string = BASE_PATH,
      ) => {
        return fetch(
          basePath + localVarFetchArgs.url,
          localVarFetchArgs.options,
        ).then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response;
          } else {
            throw response;
          }
        });
      };
    },
  };
};

/**
 * VenueApiApi - factory interface
 * @export
 */
export const VenueApiApiFactory = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return {
    /**
     *
     * @summary Deletes a venue.
     * @param {VenueApiDeleteParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiDelete(params: VenueApiDeleteParams, options?: any) {
      return VenueApiApiFp(configuration).venueApiDelete(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Gets a page of event venue.
     * @param {VenueApiGetListParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiGetList(params: VenueApiGetListParams = {}, options?: any) {
      return VenueApiApiFp(configuration).venueApiGetList(params, options)(
        fetch,
        basePath,
      );
    },
    /**
     *
     * @summary Creates a new report.
     * @param {VenueApiPostReportParams} params Operation parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    venueApiPostReport(params: VenueApiPostReportParams, options?: any) {
      return VenueApiApiFp(configuration).venueApiPostReport(params, options)(
        fetch,
        basePath,
      );
    },
  };
};

export const useVenueApiApi = function (
  configuration?: Configuration,
  fetch?: FetchAPI,
  basePath?: string,
) {
  return React.useMemo(
    () => VenueApiApiFactory(configuration, fetch, basePath),
    [],
  );
};
