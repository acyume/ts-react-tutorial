# TS & React Tutorial

[![Pipeline Status](https://gitlab.com/acyume/ts-react-tutorial/badges/master/pipeline.svg)](https://gitlab.com/acyume/ts-react-tutorial/-/pipelines)
[![Coverage Status](https://gitlab.com/acyume/ts-react-tutorial/badges/master/coverage.svg)](https://gitlab.com/acyume/ts-react-tutorial/-/pipelines)

## 开发环境准备

- 建议采用 VSCode 进行开发，对于 TypeScript 很友好。

  - 请在 VSCode 中安装插件 ESLint 和 Prettier。

- 安装 [Node.js](https://nodejs.org/en/)。建议下载 LTS 版本，当前最新版本存在兼容性问题。

- 安装 [Yarn](https://classic.yarnpkg.com/en/docs/install#mac-stable)。

- 更换 npm 源以提高安装依赖的速度。

  ```bash
  yarn config set registry https://registry.npm.taobao.org
  yarn config set sass_binary_site https://npm.taobao.org/mirrors/node-sass/
  ```

- 克隆本仓库到本地，并进入项目主目录。

- 安装依赖包。

  ```bash
  yarn
  ```

  运行完成后可以看到本地多了一个名为 `node_modules` 的文件夹，这里面就是项目运行所需要的依赖包们。

## 基本操作

- 启动开发环境：

  ```bash
  yarn dev
  ```

  命令行里可以看到本地开发服务器的地址，复制到浏览器地址栏中打开即可。

  每次进行修改以后不需要重新运行该命令，我们所使用的 Stack 本质上是基于 Webpack HMR 来进行开发时的热更新的。

- 构建生产环境代码 & 打包前端资源：

  ```bash
  yarn build
  ```

  构建完成后，可以在本地启动生产环境模式的服务器：

  ```bash
  yarn start
  ```

  如果要构建用于交付的 Docker 镜像：(该操作需要安装 Docker)

  ```bash
  docker build . -t [image_name]
  ```

## 开发约定

我们采用 Lint + Test 来保证代码质量。

- Lint 用于检查代码风格，避免写出一些容易出 BUG 的代码。

  - [Prettier](https://prettier.io/)

    目前在前端项目中广泛使用的用于规范代码风格的工具。

    本质上是一个 opinionated code formatter。

    支持少许配置，在每次文件保存时自动将代码格式化成预期风格。

    我用了半年，感觉很科学，写代码很爽，就是乱写也能帮我自动调成美观的风格。

    **请确保 VSCode Prettier 插件能够正常工作！**

    配置文件位于 `.prettierrc`，建议不要改动。

  - [ESLint](https://eslint.org/)

    传统的用于检查代码风格的工具。

    和 Prettier 不同的是，它会基于 AST 检查是否出现了一些 Bad Practice，并向用户指出。此时，我们最好思考一下是否有更好的写法。

    支持丰富的配置，目前的配置我已经用了比较久，是在官方推荐配置的基础上禁用了一些不太合理，影响开发效率的规则。

    **请确保 VSCode ESLint 插件能够正常工作！**

    配置文件位于 `.eslintrc.js`，如果要改动的话建议先讨论一下必要性。

- Test 用于测试各模块和整体功能是否符合预期。

  - [Jest](https://jestjs.io/) (for Unit Test)

    JavaScript 最好用的单元测试框架。

    单元测试用于保证各模块的代码实现符合预期。

    React 作为一个 all-in-js 的前端框架，测试起来更为简单。我们采用 [@testing-library/react](https://github.com/testing-library/react-testing-library) 来辅助测试。

    在单元测试部分，我开启了测试覆盖率 (coverage) 收集和统计功能，这项指标可反映我们写的单元测试是否足够全面。

    运行测试：`yarn test:unit`。

  - [Cypress](https://www.cypress.io/) (for E2E Test)

    End-to-end (E2E) 测试框架。

    有了 Unit Test，为什么还要 E2E Test？

    因为只保证了局部功能正常，不能说明整体体验符合预期。

    E2E Test 将打开浏览器进行体验的流程进行自动化，将整个服务作为黑盒，通过真实的浏览器进行交互和操作。

    Cypress 会在测试全过程自动录屏和截屏，定位问题更方便。

    **在测试之前，需要先启动本地开发服务器。**

    运行测试： `yarn test:e2e`。

## 工作流程

- `master` 为保护分支，即不可以直接 Push (这一点在软工项目中会强制保证)。

- 为了增加一个功能 (feat) 或者修复一个 BUG (fix) 或者重构 (refactor) 等等，需要新建工作分支。在该工作分支将任务完成后，向主分支提交 Pull Request (PR)，在 CI 上通过所有自动化测试以及其他成员 Review 后方可并入主分支。合并方式建议采用变基 (Rebase) 合并，让 Commit Log 更可读。

- Branch Name 和 Commit Log 需要采用统一的格式，以快速识别工作的类型。

  - 主要参照这个规范: https://www.conventionalcommits.org/en/v1.0.0-beta.2
  - 我们的项目中 Commit Type 的列表以及相应的解释如下：

    - feat：新增功能
    - fix：修复 BUG
    - docs：修改文档
    - refactor：代码重构，未新增任何功能和修复任何 BUG
    - chore：改变构建流程，新增依赖库、工具等（例如 webpack 修改）
    - style：仅仅修改了空格、缩进等，不改变代码逻辑
    - perf/improvement：改善性能和表现的修改
    - test：测试用例的修改
    - ci：自动化流程配置修改

  - type 以及 description 均以小写字母开头，例如

    - feat: add user login page (Correct)
    - feat: Add user login page (Incorrect)
    - Feat: add user login page (Incorrect)

  - 分支命名相对不那么严格，将目前要进行的工作加以概括即可。例如

    - ci-fix
    - register-logic-patch-1
    - feature/user-detail-page

    主要原因是分支合并后就没了，但是 Commits 还是在的 (我推荐用 Rebase 而不是 Merge，不 Squash)，所以 Commit 最好规范一些。

  - 不要作大量修改后一并提交，这时候往往就不知道如何描述自己所做的修改了，然后就开始乱写 Commit Log。最好的方式是每完成一小部分修改（逻辑上相对完整的部分）就提交一次，这样既容易用一句话描述，也方便追溯自己的工作，还能增强成就感。

* 在本地开发时建议每开发完一部分就运行已有的单元测试，以及添加相应功能的新单元测试。防止出现 CI 上大量测试无法通过 / 测试覆盖率过低等尴尬局面。
