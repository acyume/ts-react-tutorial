import React from 'react';
import SongList from './SongList';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { render } from '@testing-library/react';

Object.defineProperty(window, 'matchMedia', {
  value: () => {
    return {
      matches: false,
      addListener: () => {},
      removeListener: () => {},
    };
  },
});

Object.defineProperty(window, 'getComputedStyle', {
  value: () => {
    return {
      getPropertyValue: () => {},
    };
  },
});

const server = setupServer(
  rest.get('https://vocadb.net/api/songs', (req, res, ctx) => {
    return res(
      ctx.json({
        items: [{}],
      }),
    );
  }),
);

beforeAll(() => server.listen());
afterEach(() => {
  server.resetHandlers();
});
afterAll(() => server.close());

test('SongList', async () => {
  const { findByText } = render(<SongList />);

  expect(await findByText(/VocaDB/)).toBeTruthy();
});
